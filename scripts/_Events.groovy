eventCreateWarStart = { warName, stagingDir ->
    println "Creating build.properties"
    new File(stagingDir, "WEB-INF/classes/build.properties").text = "build.time=${new Date().format("yyyyMMdd.HHmm")}"
}
