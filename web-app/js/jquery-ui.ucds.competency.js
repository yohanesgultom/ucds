/**
 * Initialize method question form
 * @param el
 */
function initMethodQuestionForm(el, url, url2) {

    // create popup
    var popup = $('<div>Loading..</div>');
    $(popup)
        .dialog({
            autoOpen: false,
            modal: true,
            width: 500,
            title: 'Training Method Questions',
            buttons: {
                Ok: function() {
                    showOverlay();
                    var checkedQuestions = [];
                    $('input[type=checkbox]:checked', $(this)).each(function(index,cb) {
                        checkedQuestions.push(cb.value);
                    });
                    $.getJSON(url2, {questionIds:checkedQuestions.join(', ')}, function(data) {
                        if (data) {
                            $('#methods').val(data.methods.join(', '));
                            $(popup).dialog('close');
                        } else {
                            alert('Failed to get methods. Please try again');
                        }
                        hideOverlay();
                    });
                },
                Cancel: function() {
                    $(this).dialog('close');
                }
            }
        })
        .parent().css('font-size', '90%');

    // populate data from db
    $.getJSON(
        url, {},
        function (data) {
            var table = $('<table width="100%">');
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    $(table).append('<tr><td>' + data[i].value + '</td><td><input type="checkbox" value="' + data[i].id + '"/></td></tr>');
                }
            }
            $(popup).empty().append(table);
        }
    );

    // bind event
    $(el).attr('readonly','true').on('click', function() {
        $('input[type=checkbox]', $(popup)).attr('checked', false);
        $(popup).dialog('open');
    });

}