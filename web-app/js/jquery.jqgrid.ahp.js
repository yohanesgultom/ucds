/**
 * JqGrid Analytic Hierarchy Process (AHP) Helper class
 * Dependency :
 * + Sylvester.ahp.js AHP helper for Sylvester js
 * + JqGrid https://github.com/tonytomov/jqGrid
 * @author: yohanes.gultom@gmail.com
 * @licence: GPL
 */


function JqGridAHP() {}

/**
 * Init button
 * @param button
 * @param dialog
 * @param grid
 */
JqGridAHP.initButton = function(colName, button, dialog, grid, colNameMapper) {
    button
        .button({icons: {primary: 'ui-icon-calculator'}})
        .off()
        .on('click', function() {
            JqGridAHP.openDialog(colName, dialog, grid, colNameMapper);
        });
}

/**
 * Open calculate weight dialog
 * @param dialog
 * @param grid
 */
JqGridAHP.openDialog = function(colName, dialog, grid, colNameMapper) {
    var rows = grid.jqGrid('getDataIDs');
    var vals = [];
    for (var i=0;i<rows.length;i++){
        var row = grid.jqGrid('getRowData', rows[i]);
        if (row[colName]){
            var val = (colNameMapper) ? colNameMapper[row[colName]] : row[colName];
            vals.push({id: rows[i], val: val});
        }
    }
    var comparison = AHP.generateComparisonList(vals);
    dialog.empty();
    if (comparison) {
        var table = $('<table>').css('margin','10px');
        for (var i=0;i<comparison.length;i++) {
            var comp = comparison[i];
            var row = $('<tr>').attr('data-x', comp.x).attr('data-y', comp.y).attr('data-idx', comp.idx).attr('data-idy', comp.idy).attr('data-displayx', comp.displayx).attr('data-displayy', comp.displayy)
                .append($('<td>').text(comp.displayx))
                .append($('<td>').append($('<input>').attr('type', 'number').attr('min', '1').attr('max', '9').addClass('valuex').attr('maxlength','1').css('width', '25px')))
                .append($('<td>').text('vs'))
                .append($('<td>').append($('<input>').attr('type', 'number').attr('min', '1').attr('max', '9').addClass('valuey').attr('maxlength','1').css('width', '25px')))
                .append($('<td>').text(comp.displayy));
            table.append(row);
        }
        dialog.append(table);
    }
    dialog.dialog('open');
}

/**
 * Init calculate weight dialog
 * @param dialog
 * @param grid
 */
JqGridAHP.initDialog = function(colName, dialog, grid, multiplier) {
    multiplier = (multiplier) ? multiplier : 1;
    dialog.dialog({
        autoOpen: false,
        modal: true,
        width: 'auto',
        title: 'Calculate Weight',
        buttons: {
            "Calculate": function() {
                $('input', dialog).removeClass('ui-state-error');
                var rows = $('table tr', dialog);
                var comparison = [];
                var valid = true;
                var values = [];
                $.each(rows, function(i, row) {
                    var valueX = $('input.valuex', row).val();
                    var valueY = $('input.valuey', row).val();
                    if (!valueX) $('input.valuex', row).addClass('ui-state-error');
                    if (!valueY) $('input.valuey', row).addClass('ui-state-error');
                    var comp = {x:$(row).attr('data-x'), y:$(row).attr('data-y'), valuex:valueX, valuey:valueY};
                    comparison.push(comp);
                    valid = valid && comp.x && comp.y && comp.valuex && comp.valuey;
                    values[$(row).attr('data-x')] = {id: $(row).attr('data-idx'), val: $(row).attr('data-displayx')};
                    values[$(row).attr('data-y')] = {id: $(row).attr('data-idy'), val: $(row).attr('data-displayy')};
                });
                if (valid) {
                    var matrix = AHP.buildMatrix(comparison);
                    var ranked = AHP.calculateWeight(matrix, values);
                    $.each(ranked, function(i, rank) {
                        var weight = rank.weight * multiplier;
                        grid
                            .jqGrid('setCell', rank.id, colName, weight)
                            .jqGrid('editRow', rank.id)
                            .jqGrid('saveRow', rank.id);
                    });
                    dialog.dialog('close');
                }
            }
        }
    }).parent().css({'font-size' : '90%'});
}