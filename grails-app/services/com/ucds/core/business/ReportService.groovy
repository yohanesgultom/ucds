package com.ucds.core.business

import grails.plugin.cache.Cacheable
import org.hibernate.FlushMode
import org.hibernate.annotations.FetchMode

class ReportService {

    @Cacheable(value='QualityDiagram')
    def getQualityDiagram(Map params) {
        def res = []
        // TODO fix filter personCompetencies.period = :period as it's sometimes not working
        def selectClause = "select distinct person from Person as person left join fetch person.personCompetencies as personCompetency left join fetch personCompetency.competency as competency join fetch person.personPosition as personPosition"
        def whereClause = " where (personCompetency = null or personCompetency.period = :period) and (person.active = 1 or person.lastUpdated >= :period)"
        def orderClause = " order by person.position asc, person.firstName asc"
        def whereArray = []
        ((Map)params.filters)?.each {
            whereArray << "${it.key} = '${it.value}'"
        }
        whereClause += (whereArray.size() > 0) ? " and " + whereArray.join(" and ") : ""
        def query = selectClause + whereClause + orderClause
        def people = Person.executeQuery(query, [period: params.period]);
        def types = Competency.Types.values().toArray()
        def countFarBelow = [0,0,0]
        def countBelow = [0,0,0]
        def countSatisfy = [0,0,0]
        def countAbove = [0,0,0]
        def countFarAbove = [0,0,0]
        def countNotAvailable = [0,0,0]
        for(Person person : people) {
            for(int i = 0; i < types.size(); i++) {
                def quality = this.getPersonCompetencyQuality(person, types[i], params.period)
                if (PersonCompetency.Result.FAR_BELOW == quality) {
                    countFarBelow[i]++
                } else if (PersonCompetency.Result.BELOW == quality) {
                    countBelow[i]++
                } else if (PersonCompetency.Result.SATISFY == quality) {
                    countSatisfy[i]++
                } else if (PersonCompetency.Result.ABOVE == quality) {
                    countAbove[i]++
                } else if (PersonCompetency.Result.FAR_ABOVE == quality) {
                    countFarAbove[i]++
                } else  {
                    countNotAvailable[i]++
                }
            }
        }
        for(int i = 0; i < types.size(); i++) res << [countFarBelow[i],countBelow[i],countSatisfy[i],countAbove[i],countFarAbove[i],countNotAvailable[i]]
        return res
    }

    private def getPersonCompetencyQuality(Person person, String type, Date period) {
        def res = PersonCompetency.Result.NOT_AVAILABLE
        if (person && person.personCompetencies) {
            def total = null
            for (PersonCompetency personCompetency : person.personCompetencies) {
                // TODO find to filter period properly in query
                if (period.equals(personCompetency.period) && type.equalsIgnoreCase(personCompetency.competency.type) && personCompetency.gap != null) {
                    total = (total) ? total + personCompetency.gap : personCompetency.gap
                }
            }

            int top = person.personCompetencies.size()
            int bottom = person.personCompetencies.size() * (-1)
            if (total > top) {
                res = PersonCompetency.Result.FAR_ABOVE
            } else if (total > 0) {
                res = PersonCompetency.Result.ABOVE
            } else if (total == 0) {
                res = PersonCompetency.Result.SATISFY
            } else if (total >= bottom) {
                res = PersonCompetency.Result.BELOW
            } else if (total) {
                res = PersonCompetency.Result.FAR_BELOW
            }
        }
        return res
    }

    @Cacheable(value='CompetencyDiagram')
    def getCompetencyDiagram(Map params) {
        // TODO fix filter personCompetencies.period = :period as it's sometimes not working
        def selectClause = "select distinct person from Person as person join fetch person.personCompetencies as personCompetency join fetch personCompetency.competency as competency join fetch person.personPosition as personPosition"
        def whereClause = " where (personCompetency = null or personCompetency.period = :period) and (person.active = 1 or person.lastUpdated >= :period)"
        def orderClause = " order by person.position asc, person.firstName asc"
        def whereArray = []
        ((Map)params.filters)?.each {
            whereArray << "${it.key} = '${it.value}'"
        }
        whereClause += (whereArray.size() > 0) ? " and " + whereArray.join(" and ") : ""
        def query = selectClause + whereClause + orderClause
        def people = Person.executeQuery(query, [period: params.period]);
        def competencies = Competency.all;
        def competencyNames = []
        def scores = []
        // init
        for (int j = 0; j < competencies.size(); j++) {
            competencyNames << ((Competency) competencies[j]).name
            scores << 0
        }
        for(Person person : people) {
            if (person.personCompetencies) {
                for (PersonCompetency personCompetency : person.personCompetencies) {
                    // TODO find to filter period properly in query
                    if (params.period.equals(personCompetency.period)
                            && personCompetency.gap && personCompetency.gap < 0) {
                        def i = 0
                        def found = false
                        while (i < competencies.size() && !found) {
                            if (personCompetency.competency.id == ((Competency)competencies[i]).id) {
                                scores[i] += personCompetency.gap
                                found = true
                            }
                            i++;
                        }
                    }
                }
            }
        }
        return [competencies: competencyNames, scores: scores]
    }

    @Cacheable(value='CompetencyMatrixData')
    def getCompetencyMatrixData(Map params) {
        def res = [:]
        // TODO fix filter personCompetencies.period = :period as it's sometimes not working
        def selectClause = "select distinct person from Person as person left join fetch person.personCompetencies as personCompetencies join fetch person.personPosition as personPosition"
        def whereClause = " where (personCompetencies is null or personCompetencies.period = :period) and (person.active = 1 or person.lastUpdated >= :period)"
        def orderClause = " order by person.position asc, person.firstName asc"
        def whereArray = []
        ((Map)params.filters)?.each {
            whereArray << "${it.key} = '${it.value}'"
        }
        whereClause += (whereArray.size() > 0) ? " and " + whereArray.join(" and ") : ""
        def query = selectClause + whereClause + orderClause
        def people = Person.executeQuery(query, [period: params.period]);
        def competencies = Competency.findAll("from Competency as competency left join fetch competency.competencyLevels order by competency.id asc");
        if (competencies && people) {
            // matrix header
            def header = [:]
            for (Competency com : competencies) {
                int level = (com.competencyLevels) ? com.competencyLevels.size() : 1
                header[com.id] = [competencyName: com.name, competencyLevel: level, competencyType: com.type]
            }
            res["header"] = header

            // matrix rows
            def rows = []
            for (Person person : people) {
                def name = (person.lastName) ? person.firstName + " " + person.lastName : person.firstName
                def competencyScore = [:]
                if (person.personCompetencies) {
                    for (PersonCompetency pCom : person.personCompetencies) {
                        // TODO find to filter period properly in query
                        if (params.period.equals(pCom.period)) {
                            competencyScore[pCom.competency.id] = [actual: pCom.actualScore, expected: pCom.expectedScore, gap: pCom.gap]
                        }
                    }
                }
                rows << [personName: name, personPosition: person.position, competencyScore: competencyScore]
            }
            res["rows"] = rows
        }
        return res
    }

    def getAnalyticalMatrixData(Map params) {
        return getCompetencyMatrixData(params)
    }

    private void createGraph(StrategyObjective node, Map graph, int level, Date period) {
        // take care nodes
        def res = Kpi.executeQuery("select avg(k.score) from Kpi k where k.keySuccessFactor.strategyObjective = :strategyObjective and k.keySuccessFactor.parent is null and k.period = :period", [strategyObjective: node, period: period])
        double actual = (res && res.get(0)) ? res.get(0) as Double : 0
        double target = 100
        def nodeMap = [id: node.id, label: node.name, target: target, actual: actual, warning: this.isWarning(actual, target)]
        if (((ArrayList)graph["nodes"]).size() <= level) {
            // if there is no node for this level
            graph["nodes"] << [nodeMap]
        } else {
            // if there is already at least a node for this level
            boolean exist = false
            // todo optimize
            ((ArrayList)graph["nodes"]).get(level).each {
                if (((Map)it).id == nodeMap.id) {
                    exist = true
                }
            }
            if (!exist) ((ArrayList)graph["nodes"]).get(level) << nodeMap
        }

        // take care edges
        node.dependencies?.each { dependency ->
            graph["edges"] << [source: dependency.strategyObjective.id, target: node.id]
            createGraph(dependency.strategyObjective, graph, level+1, period)
        }
    }

    @Cacheable(value='StrategyObjectiveGraph')
    def getStrategyObjectiveGraph(Map params) {
        def graph = [nodes: [], edges: []]
        def root = StrategyObjective.find("from StrategyObjective as s where s.inactive = false and s.affectees is empty")
        if (root) {
            createGraph(root, graph, 0, params.period)
            // get actual value for the root from the direct children
            double actual = 0
            ((ArrayList)graph.nodes?.get(1))?.each {
                actual += it.actual
            }
            actual = actual / ((ArrayList)graph.nodes?.get(1)).size()
            ((ArrayList)graph.nodes?.get(0))?.get(0).actual = actual
            ((ArrayList)graph.nodes?.get(0))?.get(0).warning = this.isWarning(actual, 100)
        }
        return graph
    }

    private boolean isWarning(double actual, double target) {
        // todo put the rule here
        return (actual / target <= 0.7)
    }

    @Cacheable(value='UnderTargetKeySuccessFactors')
    def getUnderTargetKeySuccessFactors(Map params) {
        def res = []
        StrategyObjective strategyObjective = StrategyObjective.find("from StrategyObjective so join fetch so.keySuccessFactors ksf left join fetch ksf.kpis kpi left join fetch kpi.person left join fetch ksf.responsiblePositions where so.id = :soid and kpi.period = :period order by ksf.parent.id", [soid: params.long('soid'), period: params.period])
        strategyObjective?.keySuccessFactors?.each { ksf ->
            def map = [ksf:[:], responsibles: []]
            ksf.kpis?.each { kpi ->
                if (this.isWarning(kpi.actual, kpi.target)) {
                    map.ksf.id = ksf.id
                    map.ksf.title = ksf.title
                    ksf.responsiblePositions?.each { pos ->
                        def fullname = (kpi.person.lastName) ? kpi.person.firstName + " " + kpi.person.lastName : kpi.person.firstName
                        map.responsibles << [id: pos.id, name: fullname, position: pos.name]
                    }
                }
            }
            res << map
        }
        return res
    }
}
