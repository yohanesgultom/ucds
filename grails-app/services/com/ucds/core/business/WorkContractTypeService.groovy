package com.ucds.core.business

class WorkContractTypeService {

    def gridUtilService

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(WorkContractType, params)
            def rows = []
            ((List<WorkContractType>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.name,
                                it.description,
                                it.averageDailyWorkHours,
                                it.annualLeaveDays
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            log.error(e.message, e)
            throw e
        }

        return ret
    }

    def add(params) throws Exception {
        def obj = new WorkContractType(params)
        obj.save(failOnError: true)
    }

    def edit(params) {
        def obj = WorkContractType.findById(params.id)
        if (obj) {
            obj.properties = params
            obj.save(failOnError: true)
        } else {
            throw new Exception("WorkContractType not found")
        }
    }

    def delete(params) {
        def obj = WorkContractType.findById(params.id)
        if (obj) {
            obj.delete()
        } else {
            throw new Exception("WorkContractType not found")
        }
    }
}
