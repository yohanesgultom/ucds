package com.ucds.core.business

import grails.converters.JSON

class CompetencyService {

    def gridUtilService

    def getList(def params){
        def ret = null

        try {
            def res = gridUtilService.getDomainList(Competency, params)
            def rows = []
            def counter = 0
            ((List<Competency>)(res?.rows))?.each{
                rows << [
                        id: it.id,
                        cell:[
                            it.name,
                            it.keyIndicator
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            log.error(e.message, e)
        }

        return ret
    }

    def getRequiredCompetencyList(def params) {
        def ret = null

        try {
            def res = this.getRequiredCompetencies(params.personId, params)
            def rows = []
            def counter = 0
            ((List<Competency>)(res?.rows))?.each{
                rows << [
                        id: it.id,
                        cell:[
                                it.name,
                                it.keyIndicator
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    private def getRequiredCompetencies(String personId, def params){
        def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
        def order = params.sord && !"".equals(params.sord) ? params.sord : null
        def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        def offset = ((params.page ? params.int('page') : 1) - 1) * max

        String query = """
            select c from Competency c join c.peopleRequiring p where p.id = ${personId}
        """

        String countQuery = """
            select count(*) from Competency c join c.peopleRequiring p where p.id = ${personId}
        """

        def rows = []
        int totalRecords = 0
        int totalPage = 0

        try {
            rows = Competency.executeQuery(query, [max:max,offset:offset])
            totalRecords = Competency.executeQuery(countQuery)?.get(0)
        } catch (e) {
            e.printStackTrace()
        }

        totalPage = Math.ceil(  totalRecords / max)

        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }

    def add(params) throws Exception{
        def competency = new Competency()
        
        competency.keyIndicator = params.keyIndicator
        competency.name = params.name
        competency.type = params.type

        competency.save()
        if (competency.hasErrors()) {
            throw new Exception("${competency.errors}")
        }
    }

    def edit(params){
        def competency = Competency.findById(params.id)
        if (competency) {
            
            competency.keyIndicator = params.keyIndicator
            competency.name = params.name

            competency.save()
            if (competency.hasErrors()) {
                throw new Exception("${competency.errors}")
            }
        }else{
            throw new Exception("Competency not found")
        }
    }

    def delete(params){
        def competency = Competency.findById(params.id)
        if (competency) {
            competency.delete()
        }else{
            throw new Exception("Competency not found")
        }
    }

}