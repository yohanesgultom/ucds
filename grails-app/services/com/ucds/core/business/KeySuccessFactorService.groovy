package com.ucds.core.business

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject


class KeySuccessFactorService {

    def gridUtilService

    def getList(Map params) {
        def ret
        def cards = BalanceScoreCard.findAllByInactive(false, [sort: "name", order: "asc"])
        def categories = KeySuccessIndicatorCategory.findAllByInactive(false, [sort: "name", order: "asc"])
        try {
            params.search
            def res = gridUtilService.getDomainList(KeySuccessFactor, params)
            def rows = []
            ((List<KeySuccessFactor>) (res?.rows))?.each {
                def cell = [it.title]
                cell << it.kri
                cell << it.useKri
                JSONObject balanceScoreCard = JSON.parse(it.balanceScoreCard)
                JSONObject keySuccessIndicatorCategory = JSON.parse(it.keySuccessIndicatorCategory)
                JSONObject selectedKpi = JSON.parse(it.selectedKpi)
                for (BalanceScoreCard card : cards) {
                    cell << balanceScoreCard.get(card.id.toString())
                }
                for (KeySuccessIndicatorCategory cat : categories) {
                    cell << keySuccessIndicatorCategory.get(cat.id.toString())
                }
                cell << it.percentage
                cell << selectedKpi?.keySet()?.toArray()?.join(",")
                rows << [
                        id: it.id,
                        cell: cell
                ]
            }
            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]
        } catch (e) {
            e.printStackTrace()
            throw e
        }
        return ret
    }

    def add(Map<String, String> params) throws Exception {
        def balanceCards = [:]
        def kpis = [:]
        for (Map.Entry<String, String> map : params.entrySet()) {
            if (map.key.contains("card_")) {
                def key = map.key.replace("card_", "")
                balanceCards[key] = map.value
            } else if (map.key.contains("cat_")) {
                def key = map.key.replace("cat_", "")
                kpis[key] = map.value
            }
        }
        def person = Person.findById(params.personId)
        def percentage = (params.percentage) ? Double.parseDouble(params.percentage) : null
        def keySuccessFactor = new KeySuccessFactor(title: params.title, kri: params.kri, useKri: params.useKri, person: person, balanceScoreCard: (balanceCards as JSON).toString(), keySuccessIndicatorCategory: (kpis as JSON).toString(), selectedKpi: params.selectedKpi, percentage: percentage).save()

        if (keySuccessFactor?.hasErrors()) {
            throw new Exception(keySuccessFactor.errors?.allErrors?.toString())
        }
    }

    def edit(Map<String, String> params) throws Exception {
        def balanceCards = [:]
        def kpis = [:]
        for (Map.Entry<String, String> map : params.entrySet()) {
            if (map.key.contains("card_")) {
                def key = map.key.replace("card_", "")
                balanceCards[key] = map.value
            } else if (map.key.contains("cat_")) {
                def key = map.key.replace("cat_", "")
                kpis[key] = map.value
            }
        }

        def keySuccessFactor = KeySuccessFactor.findById(params.id)
        keySuccessFactor.title = params.title
        keySuccessFactor.kri = params.kri
        keySuccessFactor.useKri = params.useKri
        keySuccessFactor.balanceScoreCard = (balanceCards as JSON).toString()
        keySuccessFactor.keySuccessIndicatorCategory = (kpis as JSON).toString()
        keySuccessFactor.selectedKpi = params.selectedKpi
        keySuccessFactor.percentage = (params.percentage) ? Double.parseDouble(params.percentage) : null
        keySuccessFactor.save()

        if (keySuccessFactor?.hasErrors()) {
            throw new Exception(keySuccessFactor.errors?.allErrors?.toString())
        }
    }

    def delete(Map<String, String> params) throws Exception {
        def keySuccessFactor = KeySuccessFactor.findById(params.id)
        keySuccessFactor?.delete()
    }

}