package com.ucds.core.business

import grails.plugin.cache.CacheEvict
import org.springframework.context.i18n.LocaleContextHolder

class PersonCompetencyService {
    def gridUtilService
    def appUtilService
    def grailsApplication

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(PersonCompetency, params)
            def rows = []
            def counter = 0
            ((List<PersonCompetency>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.person?.firstName,
                                it.expectedScore,
                                it.actualScore,
                                it.approval,
                                it.evaluator,
                                it.status
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    @CacheEvict(value = ['CompetencyMatrixData', 'CompetencyDiagram', 'QualityDiagram'], allEntries = true)
    def edit(params) {
        def personCompetency = PersonCompetency.findById(params.id)
        if (personCompetency) {
            personCompetency.expectedScore = (params.expectedScore) ? Integer.parseInt(params.expectedScore) : null
            personCompetency.actualScore = (params.actualScore) ? Integer.parseInt(params.actualScore) : null
            personCompetency.gap = (personCompetency.actualScore && personCompetency.expectedScore) ? personCompetency.actualScore - personCompetency.expectedScore : null
            personCompetency.approval = params.approval
            personCompetency.evaluator = params.evaluator
            personCompetency.status = 0

            personCompetency.save()
            if (personCompetency.hasErrors()) {
                throw new Exception("${personCompetency.errors}")
            }

            def scoreGap = (personCompetency.actualScore && personCompetency.expectedScore) ? personCompetency.actualScore - personCompetency.expectedScore : null;
            def desc = appUtilService.getGapDesc(scoreGap)
            return [gap: scoreGap, desc: desc]
        } else {
            throw new Exception("PersonCompetency not found")
        }
    }

    def getEvaluationList(def params) {
        def ret = null

        try {
            def res = this.getCompetencyList(params)
            def rows = []
            ((List<PersonCompetency>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.competency?.name,
                                it.competency?.keyIndicator,
                                it.expectedScore,
                                it.actualScore,
                                it.gap,
                                appUtilService.getGapDesc(it.gap),
                                it.competencyId
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    /**
     * Get list of personCompetencies and create according to rules if not yet exist
     * @param params
     * @return
     */
    def getCompetencyList(def params) {
        def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
        def order = params.sord && !"".equals(params.sord) ? params.sord : null
        def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        def offset = ((params.page ? params.int('page') : 1) - 1) * max

        def rows = []
        int totalRecords = 0
        int totalPage = 0

        if (params.personId && params.type) {
            def personId = Long.parseLong(params.personId)
            def period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
            def person = Person.findById(params.personId)
            def query = "from PersonCompetency where person.id = :personId and period = :period and competency.type = :type order by id asc";
            def personCompetencies = PersonCompetency.findAll(query, [personId: personId, period: period, type: params.type], [max: max, offset: offset])

            def requiredCompetencies
            if (Competency.Types.Core.equalsIgnoreCase(params.type) || Competency.Types.Managerial.equalsIgnoreCase(params.type)) {
                requiredCompetencies = Competency.findAllByType(params.type)
            } else if (Competency.Types.Technical.equalsIgnoreCase(params.type)) {
                requiredCompetencies = person.requiredCompetencies
            }

            // check if there is any requiredCompetency that doesn't have personCompetency
            if (requiredCompetencies && requiredCompetencies.size() != personCompetencies.size()) {
                for (Competency reqCompetency : requiredCompetencies) {
                    if (!this.findCompetency(reqCompetency, personCompetencies)) {
                        def personCompetency = new PersonCompetency()
                        personCompetency.person = person
                        personCompetency.competency = reqCompetency
                        personCompetency.status = 0
                        personCompetency.period = period
                        personCompetency.save()
                    }
                }
                // re-query to fetch newly added data
                personCompetencies = PersonCompetency.findAll(query, [personId: personId, period: period, type: params.type], [max: max, offset: offset])
            }

            rows.addAll(personCompetencies)
            totalRecords = rows.size()
        }
        totalPage = Math.ceil(totalRecords / max)
        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }

    private boolean findCompetency(Competency competency, List<PersonCompetency> personCompetencies) {
        boolean found = false
        if (competency && personCompetencies) {
            for (PersonCompetency personCompetency : personCompetencies) {
                if (personCompetency.competency.id.equals(competency.id)) {
                    found = true;
                    break;
                }
            }
        }
        return found
    }

    def getPersonCompetencyForTraining(def params) {
        def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
        def order = params.sord && !"".equals(params.sord) ? params.sord : null
        def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        def offset = ((params.page ? params.int('page') : 1) - 1) * max

        def rows = []
        int totalRecords = 0
        int totalPage = 0

        if (params.period && params.type) {
            def period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
            def personCompetencies = PersonCompetency.findAll("from PersonCompetency pc join fetch pc.person p join fetch pc.competency c where pc.gap < 0 and pc.period = :period and c.type = :type order by c.type asc", [period: period, type: params.type])
            rows.addAll(personCompetencies)
            totalRecords = rows.size()
        }

        totalPage = Math.ceil(totalRecords / max)
        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }
}