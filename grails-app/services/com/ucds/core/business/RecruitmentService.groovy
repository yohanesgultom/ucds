package com.ucds.core.business

class RecruitmentService {

    def gridUtilService
    def appUtilService
    def personService

    def getCandidateList(Map params) {
        def ret = null
        try {
            def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
            def order = params.sord && !"".equals(params.sord) ? params.sord : null
            def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
            def offset = ((params.page ? params.int('page') : 1) - 1) * max

            def selectClause = "select new map(pc.id as id, pc.name as name, pc.gender as gender, pc.dateOfBirth as dateOfBirth, pc.level as level, pc.personPosition.id as personPosition, pc.section as section, ap.status as status)"
            def countClause = "select count(*)"
            def fromClause = "from PersonCandidate pc, Approval ap where pc.id = ap.refId and ap.refCategory = ?"
            def whereClauseItems = []
            def filterData = [Approval.Category.Recruitment]

            // handle single field search
            if (params.searchString && params.searchField && params.searchOper) {
                def groupOp = "and"
                gridUtilService.buildWhereClause(params.searchField, params.searchOper, params.searchString, whereClauseItems, filterData)
                fromClause += (whereClauseItems.size() > 0) ? " ${groupOp} ${whereClauseItems.join(" ${groupOp} ")}" : ""
            }

            def selectQuery = (sort && order) ? "${selectClause} ${fromClause} order by ${sort} ${order}" : "${selectClause} ${fromClause}"
            def result = PersonCandidate.executeQuery(selectQuery,filterData, [max:max,offset:offset])
            def totalRecords = Competency.executeQuery("${countClause} ${fromClause}", filterData, [max:max,offset:offset])?.get(0)
            def totalPage = Math.ceil(totalRecords/max)

            def rows = []
            ((ArrayList<Map>)result)?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.name,
                                it.gender,
                                it.dateOfBirth,
                                null,
                                it.level,
                                it.personPosition,
                                it.section,
                                it.status
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]

        } catch (e) {
            log.error(e.message, e)
        }
        return ret
    }

    def getCandidateList(Map params, String approvalStatus) {
        def ret = null
        try {
            def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
            def order = params.sord && !"".equals(params.sord) ? params.sord : null
            def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
            def offset = ((params.page ? params.int('page') : 1) - 1) * max

            def selectClause = "select new map(pc.id as id, pc.name as name, pc.gender as gender, pc.dateOfBirth as dateOfBirth, pc.level as level, pc.personPosition.id as personPosition, pc.section as section, pc.totalGap as totalGap, ap.status as status, ap.requester as requester, ap.dateCreated as dateCreated)"
            def countClause = "select count(*)"
            def fromClause = "from PersonCandidate pc, Approval ap where pc.id = ap.refId and ap.refCategory = ? and ap.status = ?"
            def whereClauseItems = []
            def filterData = [Approval.Category.Recruitment,approvalStatus]

            // handle single field search
            if (params.searchString && params.searchField && params.searchOper) {
                def groupOp = "and"
                gridUtilService.buildWhereClause(params.searchField, params.searchOper, params.searchString, whereClauseItems, filterData)
                fromClause += (whereClauseItems.size() > 0) ? " ${groupOp} ${whereClauseItems.join(" ${groupOp} ")}" : ""
            }

            def selectQuery = (sort && order) ? "${selectClause} ${fromClause} order by ${sort} ${order}" : "${selectClause} ${fromClause}"
            def result = PersonCandidate.executeQuery(selectQuery,filterData, [max:max,offset:offset])
            def totalRecords = Competency.executeQuery("${countClause} ${fromClause}", filterData, [max:max,offset:offset])?.get(0)
            def totalPage = Math.ceil(totalRecords/max)

            def rows = []
            ((List<Map>) result)?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.name,
                                it.gender,
                                it.dateOfBirth,
                                it.level,
                                it.personPosition,
                                it.section,
                                it.requester,
                                it.totalGap
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]

        } catch (e) {
            log.error(e.message, e)
        }
        return ret
    }

    def getRequiredCompetencyList(Map params) {
        def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
        def order = params.sord && !"".equals(params.sord) ? params.sord : null
        def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        def offset = ((params.page ? params.int('page') : 1) - 1) * max

        String from = "from Competency c join c.candidatesRequiring cr where cr.id = ${params.candidateId}"
        String query = "select c ${from}"
        query += (sort && order) ? " order by c.${sort} ${order}" : ""
        String countQuery = "select count(*) ${from}"

        def result = []
        int totalRecords = 0

        try {
            result = Competency.executeQuery(query, [max:max,offset:offset])
            totalRecords = Competency.executeQuery(countQuery)?.get(0)
        } catch (e) {
            log.error(e.message, e)
        }

        def totalPage = Math.ceil(totalRecords / max)

        def rows = []
        ((List<Competency>)(result))?.each{
            rows << [
                    id: it.id,
                    cell:[
                            it.name,
                            it.keyIndicator
                    ]
            ]
        }

        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }

    def getEvaluationList(def params) {
        def ret = null
        try {
            def res = this.getCompetencyList(params)
            def rows = []
            ((List<PersonCandidateCompetency>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.competency?.name,
                                it.competency?.keyIndicator,
                                it.expectedScore,
                                it.actualScore,
                                it.gap,
                                appUtilService.getGapDesc(it.gap),
                                it.competencyId
                        ]
                ]
            }
            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]
        } catch (e) {
            e.printStackTrace()
            throw e
        }
        return ret
    }

    /**
     * Get list of personCompetencies and create according to rules if not yet exist
     * @param params
     * @return
     */
    def getCompetencyList(def params) {
        def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
        def order = params.sord && !"".equals(params.sord) ? params.sord : null
        def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        def offset = ((params.page ? params.int('page') : 1) - 1) * max

        def rows = []
        int totalRecords = 0
        int totalPage = 0

        if (params.candidateId && params.type) {
            def candidateId = Long.parseLong(params.candidateId)
            def candidate = PersonCandidate.findById(params.candidateId)
            def query = "from PersonCandidateCompetency where candidate.id = :candidateId and competency.type = :type order by id asc";
            def candidateCompetencies = PersonCandidateCompetency.findAll(query, [candidateId:candidateId,type:params.type], [max: max, offset: offset])
            def requiredCompetencies
            if (Competency.Types.Core.equalsIgnoreCase(params.type) || Competency.Types.Managerial.equalsIgnoreCase(params.type)) {
                requiredCompetencies = Competency.findAllByType(params.type)
            } else if (Competency.Types.Technical.equalsIgnoreCase(params.type)) {
                requiredCompetencies = candidate.requiredCompetencies
            }

            // check if there is any requiredCompetency that doesn't have personCandidateCompetency
            if (requiredCompetencies && requiredCompetencies.size() != candidateCompetencies.size()) {
                for (Competency reqCompetency : requiredCompetencies) {
                    if (!this.findCompetency(reqCompetency, candidateCompetencies)) {
                        def candidateCompetency = new PersonCandidateCompetency()
                        candidateCompetency.candidate = candidate
                        candidateCompetency.competency = reqCompetency
                        candidateCompetency.status = 0
                        candidateCompetency.save()
                    }
                }
                // re-query to fetch newly added data
                candidateCompetencies = PersonCandidateCompetency.findAll(query, [candidateId:candidateId,type:params.type], [max:max,offset:offset])
            }

            rows.addAll(candidateCompetencies)
            totalRecords = rows.size()
        }
        totalPage = Math.ceil(totalRecords / max)
        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }

    private boolean findCompetency(Competency competency, List<PersonCandidateCompetency> personCompetencies) {
        boolean found = false
        if (competency && personCompetencies) {
            for (PersonCandidateCompetency personCompetency : personCompetencies) {
                if (personCompetency.competency.id.equals(competency.id)) {
                    found = true;
                    break;
                }
            }
        }
        return found
    }

    def candidateAdd(Map params) {
        PersonCandidate.withTransaction {
            def personCandidate = new PersonCandidate()
            personCandidate.name = params.name
            personCandidate.gender = params.gender
            personCandidate.dateOfBirth = Date.parse("dd/MM/yyyy", params.dateOfBirth)
            personCandidate.level = params.level
            personCandidate.section = params.section
            personCandidate.personPosition = (params.replacementfor && !"null".equalsIgnoreCase(params.replacementfor)) ? null : PersonPosition.findById(params.position)
            personCandidate.save(failOnError: true)

            def approval = new Approval()
            approval.refId = personCandidate.id
            approval.refCategory = Approval.Category.Recruitment
            approval.status = PersonCandidate.Status.New
            approval.requester = params.requester
            approval.save(failOnError: true)

            def personId = params.replacementfor.split(":")[0]
            def person = Person.findById(personId)
            if (person) this.copyPersonDetails(personCandidate,person)
        }
    }

    private void copyPersonDetails(PersonCandidate candidate, Person person) {
        // copy details
        candidate.personPosition = person.personPosition
        candidate.section = person.section
        candidate.level = person.level
        // copy required competencies
        person.requiredCompetencies?.each {
            candidate.addToRequiredCompetencies(it)
        }
        // copy person competencies
        List result = PersonCompetency.executeQuery("select max(period) from PersonCompetency where person = ?",[person])
        def period = (result && result.size() > 0) ? result[0] : null
        if (period) {
            def personCompetencies = PersonCompetency.findAllByPersonAndPeriod(person,period);
            personCompetencies?.each {
                candidate.addToCandidateCompetencies(new PersonCandidateCompetency(competency: it.competency, expectedScore: it.expectedScore))
            }
        }
        candidate.save()
    }

    def candidateEdit(Map params) {
        def personCandidate = PersonCandidate.findById(params.id)
        personCandidate.name = params.name
        personCandidate.gender = params.gender
        personCandidate.dateOfBirth = Date.parse("dd/MM/yyyy", params.dateOfBirth)
        personCandidate.level = params.level
        personCandidate.section = params.section
        personCandidate.personPosition = PersonPosition.findById(params.position)
        personCandidate.save(failOnError: true)
    }

    def candidateDelete(Map params) {
        PersonCandidate.withTransaction {
            def personCandidate = PersonCandidate.findById(params.id)
            def approval = Approval.findByRefIdAndRefCategory(personCandidate.id, Approval.Category.Recruitment)
            approval.delete()
            personCandidate.delete()
        }
    }

    def candidateApprove(Map params) {
        //updateApproval(params.ids, PersonCandidate.Status.Approved)
        params.ids?.split(",")?.each { id ->
            PersonCandidate.withTransaction {
                def candidate = PersonCandidate.findById(id)
                // create new person from candidate
                def person = new Person()
                person.dateOfBirth = candidate.dateOfBirth
                person.firstName = candidate.name
                person.gender = candidate.gender
                person.section = candidate.section
                person.level = candidate.level
                person.personPosition = candidate.personPosition
                person.save(failOnError: true)
                // add required competencies
                candidate.requiredCompetencies?.each { competency ->
                    person.addToRequiredCompetencies(competency)
                }
                person.save(failOnError: true)
                // delete candidate
                candidate.personPosition = null
                candidate.delete();
                // note: personCompetency can't be added because we don't know what period it should be created for
            }
        }
    }

    def candidateReject(Map params) {
        updateApproval(params.ids, PersonCandidate.Status.Rejected)
    }

    private updateApproval(String ids, String status) {
        def idList = []
        ids?.split(",")?.each {
            idList << Long.parseLong(it)
        }
        Approval.executeUpdate("update Approval set status = :status where refId in (:ids)",[status:status,ids:idList])
    }

    def addRequiredCompetency(def params) {
        def ret = null
        PersonCandidate person = PersonCandidate.findById(params.candidateId)
        Competency competency = Competency.findById(params.competencyId)
        if (person) {
            if (competency) {
                person.addToRequiredCompetencies(competency)
                person.save()
            } else {
                throw new Exception("Invalid competency")
            }
        } else {
            throw new Exception("Invalid candidate")
        }
        return ret
    }

    def removeRequiredCompetency(def params) {
        def ret = null
        PersonCandidate person = PersonCandidate.findById(params.candidateId)
        Competency competency = Competency.findById(params.competencyId)
        if (person) {
            if (competency) {
                person.removeFromRequiredCompetencies(competency)
                person.save()
            } else {
                throw new Exception("Invalid competency")
            }
        } else {
            throw new Exception("Invalid canidate")
        }
        return ret
    }

    def evaluate(params) {
        def candidateCompetency = PersonCandidateCompetency.findById(params.id)
        if (candidateCompetency) {
            candidateCompetency.expectedScore = (params.expectedScore) ? Integer.parseInt(params.expectedScore) : null
            candidateCompetency.actualScore = (params.actualScore) ? Integer.parseInt(params.actualScore) : null
            candidateCompetency.gap = (candidateCompetency.actualScore && candidateCompetency.expectedScore) ? candidateCompetency.actualScore - candidateCompetency.expectedScore : null
            candidateCompetency.approval = params.approval
            candidateCompetency.evaluator = params.evaluator
            candidateCompetency.status = 0

            candidateCompetency.save()
            if (candidateCompetency.hasErrors()) {
                throw new Exception("${candidateCompetency.errors}")
            }

            def scoreGap = (candidateCompetency.actualScore && candidateCompetency.expectedScore) ? candidateCompetency.actualScore - candidateCompetency.expectedScore : null;
            def desc = appUtilService.getGapDesc(scoreGap)
            return [gap: scoreGap, desc: desc]
        } else {
            throw new Exception("Candidate Competency not found")
        }
    }

    def markAsEvaluated(String candidateId, String approver) {
        def candidate = PersonCandidate.find("from PersonCandidate pc join fetch pc.candidateCompetencies cc join fetch cc.competency where pc.id = :id", [id:Long.parseLong(candidateId)])

        // count total gap/get unevaluated competencies
        def unevaluatedCompetencies = []
        int totalGap = 0
        candidate.candidateCompetencies.each {
            if (it.gap != null) {
                totalGap += it.gap
            } else {
                unevaluatedCompetencies << it.competency.name
            }
        }

        // proceed if no unevaluated compentecies
        if (unevaluatedCompetencies.empty) {
            PersonCandidate.withTransaction {
                candidate.totalGap = totalGap
                candidate.save(failOnError: true)
                def approval = Approval.findByRefId(candidate.id)
                approval.status = PersonCandidate.Status.Evaluated
                approval.approver = approver
                approval.save(failOnError: true)
            }
        } else {
            throw new Exception("Please evaluate: " + unevaluatedCompetencies.join("\r\n"))
        }
    }

    def getCandidateCompetencyMatrixData(Map params) {
        def res = [:]
        // TODO fix filter personCompetencies.period = :period as it's sometimes not working
        def selectClause = "select distinct candidate from Approval a,PersonCandidate as candidate left join fetch candidate.candidateCompetencies as candidateCompetencies join fetch candidate.personPosition as personPosition"
        def whereClause = " where a.refId = candidate.id and a.status = '${PersonCandidate.Status.Evaluated}'"
        def orderClause = " order by candidate.personPosition.name asc, candidate.name asc"
        def whereArray = []
        ((Map)params.filters)?.each {
            whereArray << "${it.key} = '${it.value}'"
        }
        whereClause += (whereArray.size() > 0) ? " and " + whereArray.join(" and ") : ""
        def query = selectClause + whereClause + orderClause
        def candidates = PersonCandidate.executeQuery(query);
        def competencies = Competency.findAll("from Competency as competency left join fetch competency.competencyLevels order by competency.id asc");
        if (competencies && candidates) {
            // matrix header
            def header = [:]
            for (Competency com : competencies) {
                int level = (com.competencyLevels) ? com.competencyLevels.size() : 1
                header[com.id] = [competencyName: com.name, competencyLevel: level, competencyType: com.type]
            }
            res["header"] = header

            // matrix rows
            def rows = []
            for (PersonCandidate candidate : candidates) {
                def name = candidate.name
                def competencyScore = [:]
                if (candidate.candidateCompetencies) {
                    for (PersonCandidateCompetency pCom : candidate.candidateCompetencies) {
                        competencyScore[pCom.competency.id] = [actual: pCom.actualScore, expected: pCom.expectedScore, gap: pCom.gap]
                    }
                }
                rows << [id: candidate.id, personName: name, personPosition: candidate.personPosition?.name, competencyScore: competencyScore]
            }
            res["rows"] = rows
        }
        return res
    }

}
