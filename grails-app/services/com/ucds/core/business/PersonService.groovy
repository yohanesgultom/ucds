package com.ucds.core.business

import grails.converters.JSON
import grails.plugin.cache.CacheEvict
import grails.plugin.cache.Cacheable


class PersonService {

    def gridUtilService

    def getList(def params) {
        def ret = null
        try {
            // filter active
            this.overrideParams(params)
            def res = gridUtilService.getDomainList(Person, params)
            def rows = []
            ((List<Person>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.firstName,
                                it.lastName,
                                it.gender,
                                it.dateOfBirth,
                                it.level,
                                it.personPosition?.id,
                                it.section
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            throw e
        }

        return ret
    }

    private void overrideParams(params) {
        params._search = "true"
        if (params.filters) {
            def filters = JSON.parse(params.filters)
            filters.rules << [field: "active", op: "eq", data: true]
        } else {
            params.searchField = "active"
            params.searchOper = "eq"
            params.searchString = "true"
        }
    }

    @CacheEvict(value = ['EvaluationList'], allEntries = true)
    def add(params) throws Exception {
        def person = new Person()
        def position = PersonPosition.findById(params.position)
        person.dateOfBirth = Date.parse("dd/MM/yyyy", params.dateOfBirth)
        person.firstName = params.firstName
        person.gender = params.gender
        person.lastName = params.lastName
        person.section = params.section
        person.level = params.level
        //person.position = position.name
        person.personPosition = position
        person.save()

        if (person.hasErrors()) {
            throw new Exception("${person.errors}")
        }
    }

    @CacheEvict(value = ['EvaluationList'], allEntries = true)
    def edit(params) {
        def person = Person.findById(params.id)
        def position = PersonPosition.findById(params.position)
        if (person) {
            person.dateOfBirth = Date.parse("dd/MM/yyyy", params.dateOfBirth)
            person.firstName = params.firstName
            person.gender = params.gender
            person.lastName = params.lastName
            person.section = params.section
            person.level = params.level
            //person.position = params.position
            person.personPosition = position
            person.save(failOnError: true)
        } else {
            throw new Exception("Person not found")
        }
    }

    @CacheEvict(value = ['EvaluationList'], allEntries = true)
    def delete(params) {
        def person = Person.findById(params.id)
        if (person) {
            person.active = false
            person.save(failOnError: true)
        } else {
            throw new Exception("Person is not found")
        }
    }

    @Cacheable(value='EvaluationList')
    def getEvaluationList(def params) {
        def ret = null

        try {
            this.overrideParams(params)
            def res = gridUtilService.getDomainList(Person, params)
            def rows = []
            ((List<Person>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.firstName,
                                it.lastName,
                                it.level,
                                it.personPosition?.id,
                                it.section
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def addRequiredCompetency(def params) {
        def ret = null
        Person person = Person.findById(params.personId)
        Competency competency = Competency.findById(params.competencyId)
        if (person) {
            if (competency) {
                person.addToRequiredCompetencies(competency)
                person.save()
            } else {
                throw new Exception("Invalid competency")
            }
        } else {
            throw new Exception("Invalid person")
        }
        return ret
    }

    def removeRequiredCompetency(def params) {
        def ret = null
        Person person = Person.findById(params.personId)
        Competency competency = Competency.findById(params.competencyId)
        if (person) {
            if (competency) {
                person.removeFromRequiredCompetencies(competency)
                person.save()
            } else {
                throw new Exception("Invalid competency")
            }
        } else {
            throw new Exception("Invalid person")
        }
        return ret
    }

    def getPositionList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(PersonPosition, params)
            def rows = []
            def counter = 0
            ((List<PersonPosition>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.name
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def addPosition(params) {
        def p = new PersonPosition()
        p.name = params.name
        p.save()
        if (p.hasErrors()) {
            throw new Exception("${p.errors}")
        }
    }

    def editPosition(params) {
        def p = PersonPosition.findById(params.id)
        if (p) {
            p.name = params.name
            p.save()
            if (p.hasErrors()) {
                throw new Exception("${p.errors}")
            }
        } else {
            throw new Exception("Person position not found")
        }
    }

    def deletePosition(params) {
        def p = PersonPosition.findById(params.id)
        if (p) {
            p.delete()
        } else {
            throw new Exception("Person position is not found")
        }
    }
}