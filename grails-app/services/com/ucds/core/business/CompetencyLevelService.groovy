package com.ucds.core.business

class CompetencyLevelService {

    def gridUtilService

    def getList(def params){
        def ret = null

        try {
            def res = gridUtilService.getDomainList(CompetencyLevel, params)
            def rows = []
            ((List<CompetencyLevel>)(res?.rows))?.each{
                rows << [
                        id: it.id,
                        cell:[
                                it.level,
                                it.name,
                                it.keyIndicator,
                                it.trainingMethods*.name.join(', ')
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception{
        def competency = Competency.findById(params.competency_id)
        def methods = new HashSet<TrainingMethod>()
        params.methods?.split(', ').collect({
            def method = TrainingMethod.findByName(it)
            if (method) {
                methods.add(method)
            }
        })
        def competencyLevel = new CompetencyLevel(level: params.level, keyIndicator: params.keyIndicator, name: params.name, competency: competency, trainingMethods: methods).save()
        if (competencyLevel.hasErrors()) {
            throw new Exception("${competencyLevel.errors}")
        }
    }

    def edit(params){
        def competencyLevel = CompetencyLevel.findById(params.id)
        if (competencyLevel) {

            def methods = new HashSet<TrainingMethod>()
            params.methods?.split(', ').collect({
                def method = TrainingMethod.findByName(it)
                if (method) {
                    methods.add(method)
                }
            })

            competencyLevel.keyIndicator = params.keyIndicator
            competencyLevel.name = params.name
            competencyLevel.level = (params.level) ? Integer.parseInt(params.level) : 0
            competencyLevel.trainingMethods = methods

            competencyLevel.save()
            if (competencyLevel.hasErrors()) {
                throw new Exception("${competencyLevel.errors}")
            }
        }else{
            throw new Exception("Competency Level is not exist")
        }
    }

    def delete(params){
        def competencyLevel = CompetencyLevel.findById(params.id)
        if (competencyLevel) {
            competencyLevel.delete()
        }else{
            throw new Exception("Competency Level is not exist")
        }
    }
}
