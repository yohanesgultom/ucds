package com.ucds.core.business

class TrainingProgramService {

    def gridUtilService
    def appUtilService
    def grailsApplication

    def getList(def params) {
        def ret
        try {
            def period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
            def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
            def order = params.sord && !"".equals(params.sord) ? params.sord : null
            def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
            def offset = ((params.page ? params.int('page') : 1) - 1) * max

            def personCompetencies = PersonCompetency.findAll("from PersonCompetency pc join fetch pc.competency c join fetch c.competencyLevels cl join fetch pc.person p where pc.gap < 0 and cl.level = pc.expectedScore and pc.period = :period order by c.id asc", [period: period], [max: max, offset: offset])

            def rows = []
            ((List<PersonCompetency>) (personCompetencies))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.person.firstName,
                                it.person.lastName,
                                it.person.position,
                                it.competency.name,
                                it.actualScore,
                                it.expectedScore,
                                it.competency.competencyLevels?.first()?.keyIndicator
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: personCompetencies.size(), page: params.page, totalPage: Math.ceil(personCompetencies.size() / max)]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def getDetailsList(def params) {
        def ret = null

        try {
            def res = this.getTrainingProgramList(params)
            def rows = []
            ((List<TrainingProgram>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.competencyLevel,
                                it.trainingMethod?.name,
                                it.trainingMethod?.description,
                                it.trainerOrPIC,
                                it.startDate,
                                it.endDate,
                                it.cost,
                                it.note
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def getTrainingProgramList(def params) {
        def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
        def order = params.sord && !"".equals(params.sord) ? params.sord : null
        def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        def offset = ((params.page ? params.int('page') : 1) - 1) * max

        def rows = []
        def query = "from TrainingProgram tp where tp.personCompetency.id = :id order by tp.competencyLevel"
        def trainingPrograms = TrainingProgram.findAll(query, [id: params.long('personCompetencyId')], [max: max, offset: offset])
        // when empty recreate all training programs
        // this gives "reset" feature in case of one unintentionally deleting training program(s)
        if (!trainingPrograms || trainingPrograms.size() <= 0) {
            this.createTrainingPrograms(PersonCompetency.findById(params.personCompetencyId))
            // re-query to fetch newly created records
            trainingPrograms = TrainingProgram.findAll(query, [id: params.long('personCompetencyId')], [max: max, offset: offset])
        }
        rows.addAll(trainingPrograms)
        def totalRecords = rows.size()
        rows.sort { it.competencyLevel }
        def totalPage = Math.ceil(totalRecords / max)
        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }

    /**
     * Create training programs for each gapped level and method
     * @param personCompetency
     * @return
     */
    private void createTrainingPrograms(PersonCompetency personCompetency) {
        def trainingPrograms = []
        if (personCompetency) {
            def actualScore = personCompetency.actualScore
            def expectedScore = personCompetency.expectedScore
            def trainingPlan = TrainingPlan.findByPeriod(personCompetency.period)
            personCompetency.competency.competencyLevels.each({
                if (it.level > actualScore && it.level <= expectedScore) {
                    it.trainingMethods.eachWithIndex { method, i ->
                        def trainingProgram = new TrainingProgram(personCompetency: personCompetency, trainingMethod: method, competencyLevel: it.level).save()
                        trainingPlan.addToTrainingPrograms(trainingProgram)
                    }
                }
            })
            personCompetency.trainingPrograms = trainingPrograms
            personCompetency.save()
            trainingPlan.save()
        }
    }

    def edit(params) {
        def res
        def trainingProgram = TrainingProgram.findById(params.id)
        if (trainingProgram) {
            trainingProgram.trainerOrPIC = params.trainerOrPic
            trainingProgram.startDate = appUtilService.tryParseDate(params.startDate)
            trainingProgram.endDate = appUtilService.tryParseDate(params.endDate)
            trainingProgram.note = params.note
            try {
                params.cost = Long.parseLong(params.cost)
            } catch (Exception e) {
                params.cost = null
            }
            trainingProgram.cost = params.cost
            trainingProgram.save()
        } else {
            throw new Exception("Record is not found")
        }
        return res
    }

    def delete(params) {
        def res
        def trainingProgram = TrainingProgram.findById(params.id)
        if (trainingProgram) {
            trainingProgram.delete()
        } else {
            throw new Exception("Record is not found")
        }
        return res
    }

    def getTotalCostAndTime(Date period) {
        def res = [:]
        def result = ((ArrayList) TrainingProgram.executeQuery("select tpl.id, sum(tpg.cost) from TrainingProgram tpg join tpg.trainingPlan tpl where tpl.period = :period group by tpl.id", [period: period]))
        res.totalCost = (result && result.size() > 0 && result[0] && ((ArrayList)result[0]).size() > 0) ? result[0][1] as Double : 0

        result = TrainingProgram.executeQuery("select tpl.id, max(tpg.endDate), min(tpg.startDate) from TrainingProgram tpg join tpg.trainingPlan tpl where tpl.period = :period group by tpl.id", [period: period])
        def resDate = (result && result.size() > 0) ? result[0] as ArrayList : []
        res.totalTime = (resDate.size() >= 3) ? resDate.get(1) - resDate.get(2) : 0
        return res
    }
}