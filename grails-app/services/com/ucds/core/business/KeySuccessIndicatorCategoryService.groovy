package com.ucds.core.business


class KeySuccessIndicatorCategoryService {

    def gridUtilService

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(KeySuccessIndicatorCategory, params)
            def rows = []
            def counter = 0
            ((List<KeySuccessIndicatorCategory>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.name,
                                it.inactive
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception {
        def KPICategory = new KeySuccessIndicatorCategory()

        KPICategory.inactive = Boolean.parseBoolean(params.inactive)
        KPICategory.name = params.name

        KPICategory.save()
        if (KPICategory.hasErrors()) {
            throw new Exception("${KPICategory.errors}")
        }
    }

    def edit(params) {
        def KPICategory = KeySuccessIndicatorCategory.findById(params.id)
        if (KPICategory) {

            KPICategory.inactive = Boolean.parseBoolean(params.inactive)
            KPICategory.name = params.name

            KPICategory.save()
            if (KPICategory.hasErrors()) {
                throw new Exception("${KPICategory.errors}")
            }
        } else {
            throw new Exception("KeySuccessIndicatorCategory not found")
        }
    }

    def delete(params) {
        def KPICategory = KeySuccessIndicatorCategory.findById(params.id)
        if (KPICategory) {
            KPICategory.delete()
        } else {
            throw new Exception("KeySuccessIndicatorCategory not found")
        }
    }

}