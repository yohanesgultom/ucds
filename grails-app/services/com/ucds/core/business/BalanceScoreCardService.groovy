package com.ucds.core.business


class BalanceScoreCardService {

    def gridUtilService

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(BalanceScoreCard, params)
            def rows = []
            def counter = 0
            ((List<BalanceScoreCard>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.name,
                                it.inactive
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception {
        def balanceScoreCard = new BalanceScoreCard()

        balanceScoreCard.inactive = Boolean.parseBoolean(params.inactive)
        balanceScoreCard.name = params.name

        balanceScoreCard.save()
        if (balanceScoreCard.hasErrors()) {
            throw new Exception("${balanceScoreCard.errors}")
        }
    }

    def edit(params) {
        def balanceScoreCard = BalanceScoreCard.findById(params.id)
        if (balanceScoreCard) {

            balanceScoreCard.inactive = Boolean.parseBoolean(params.inactive)
            balanceScoreCard.name = params.name

            balanceScoreCard.save()
            if (balanceScoreCard.hasErrors()) {
                throw new Exception("${balanceScoreCard.errors}")
            }
        } else {
            throw new Exception("BalanceScoreCard not found")
        }
    }

    def delete(params) {
        def balanceScoreCard = BalanceScoreCard.findById(params.id)
        if (balanceScoreCard) {
            balanceScoreCard.delete()
        } else {
            throw new Exception("BalanceScoreCard not found")
        }
    }

}