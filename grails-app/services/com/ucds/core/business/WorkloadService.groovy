package com.ucds.core.business

class WorkloadService {

    def gridUtilService
    def appUtilService

    def add(Map params) throws Exception {
        def newId = null
        Person person = Person.findById(params.personId)
        if (person) {
            def period = new Date()
            Workload workload = new Workload()
            workload.position = person.personPosition?.name
            workload.section = person.section
            workload.period = period
            workload.person = person
            workload.summary = params.summary
            workload.manpowerAvailable = (params.manpowerAvailable) ? Float.parseFloat(params.manpowerAvailable) : 1f
            def workContractType = WorkContractType.findById(params.workContractType)
            workload.dailyWorkingTime = this.getWorkloadUnitInMinutes(WorkloadItem.UNIT.DAILY, period[Calendar.YEAR], workContractType)
            workload.weeklyWorkingTime = this.getWorkloadUnitInMinutes(WorkloadItem.UNIT.WEEKLY, period[Calendar.YEAR], workContractType)
            workload.monthlyWorkingTime = this.getWorkloadUnitInMinutes(WorkloadItem.UNIT.MONTHLY, period[Calendar.YEAR], workContractType)
            workload.annualWorkingTime = this.getWorkloadUnitInMinutes(WorkloadItem.UNIT.ANNUALLY, period[Calendar.YEAR], workContractType)
            workload.save(failOnError: true)
            newId = workload.id
        }
        return newId
    }

    def edit(Map params) throws Exception {
        def id = params.id
        Workload workload = Workload.findById(params.id)
        workload.summary = params.summary
        workload.manpowerAvailable = (params.manpowerAvailable) ? Float.parseFloat(params.manpowerAvailable) : 1f
        workload.save(failOnError: true)
        return id
    }

    def delete(params) {
        def id = params.id
        def workload = Workload.findById(params.id)
        if (workload) {
            workload.delete()
            return id
        } else {
            throw new Exception("Workload analysis is not found")
        }
    }

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(Workload, params)
            def rows = []
            ((List<Workload>) (res?.rows))?.each {
                rows << [
                        id: it.id,
                        cell: [
                                it.position,
                                it.section,
                                it.period,
                                it.summary,
                                it.manpowerAvailable,
                                it.workload,
                                it.effectiveness,
                                it.criteria,
                                it.dailyWorkingTime,
                                it.weeklyWorkingTime,
                                it.monthlyWorkingTime,
                                it.annualWorkingTime,
                                null
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            log.error(e.message,e)
            throw e
        }

        return ret
    }

    int getWorkloadUnitInMinutes(String unit, int year, WorkContractType workContractType) {
        int mins = (workContractType) ? appUtilService.getTotalEffectiveMinutes(year, workContractType) : appUtilService.getTotalEffectiveMinutes(year)
        if (WorkloadItem.UNIT.ANNUALLY.equals(unit)) {
            // do nothing
        } else if (WorkloadItem.UNIT.MONTHLY.equals(unit)) {
            mins = mins / 12
        } else if (WorkloadItem.UNIT.WEEKLY.equals(unit)) {
            mins = mins / 60
        } else if (WorkloadItem.UNIT.DAILY.equals(unit)) {
            mins = appUtilService.getEffectiveMinutesDaily(workContractType)
        } else {
            mins = 0
        }
        return mins
    }
}
