package com.ucds.core


class MenuService {

    def gridUtilService

    def getList(def params){
        def ret = null

        try {
            def res = gridUtilService.getDomainList(Menu, params)
            def rows = []
            def counter = 0
            ((List<Menu>)(res?.rows))?.each{
                rows << [
                        id: it.id,
                        cell:[
                             
                            it.details,  
                            it.label,  
                            it.linkAction,  
                            it.linkController,  
                            it.menuCode,   
                            it.seq,  
                            it.targetMode,  
                            it.url  
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception{
        def menu = new Menu()
        
        menu.details = params.details 
        menu.label = params.label 
        menu.linkAction = params.linkAction 
        menu.linkController = params.linkController 
        menu.menuCode = params.menuCode 
        menu.seq = params.int('seq')
        menu.targetMode = params.targetMode 
        menu.url = params.url 

        menu.save()
        if (menu.hasErrors()) {
            throw new Exception("${menu.errors}")
        }
    }

    def edit(params){
        def menu = Menu.findById(params.id)
        if (menu) {
            
            menu.details = params.details 
            menu.label = params.label 
            menu.linkAction = params.linkAction 
            menu.linkController = params.linkController 
            menu.menuCode = params.menuCode 
            menu.seq = params.int('seq')
            menu.targetMode = params.targetMode 
            menu.url = params.url 

            menu.save()
            if (menu.hasErrors()) {
                throw new Exception("${menu.errors}")
            }
        }else{
            throw new Exception("Menu not found")
        }
    }

    def delete(params){
        def menu = Menu.findById(params.id)
        if (menu) {
            menu.delete()
        }else{
            throw new Exception("Menu not found")
        }
    }

}