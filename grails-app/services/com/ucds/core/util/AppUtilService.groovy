package com.ucds.core.util

import com.ucds.core.SystemParameter
import com.ucds.core.business.Holiday
import com.ucds.core.business.Person
import com.ucds.core.business.PersonPosition
import com.ucds.core.business.WorkContractType
import grails.plugin.cache.Cacheable
import org.joda.time.DateTime
import org.joda.time.DateTimeConstants
import org.springframework.context.i18n.LocaleContextHolder

class AppUtilService {

    def messageSource
    def grailsApplication

    def getPeriod = {
        def years = []
        def months = [:]
        def today = Calendar.getInstance()
        for(int i = 0; i < 5; i++) { // 5 years back
            years << today.get(Calendar.YEAR) - i
        }
        months[1] = messageSource.getMessage("ucds.month.january", null, LocaleContextHolder.locale)
        months[2] = messageSource.getMessage("ucds.month.february", null, LocaleContextHolder.locale)
        months[3] = messageSource.getMessage("ucds.month.march", null, LocaleContextHolder.locale)
        months[4] = messageSource.getMessage("ucds.month.april", null, LocaleContextHolder.locale)
        months[5] = messageSource.getMessage("ucds.month.may", null, LocaleContextHolder.locale)
        months[6] = messageSource.getMessage("ucds.month.june", null, LocaleContextHolder.locale)
        months[7] = messageSource.getMessage("ucds.month.july", null, LocaleContextHolder.locale)
        months[8] = messageSource.getMessage("ucds.month.august", null, LocaleContextHolder.locale)
        months[9] = messageSource.getMessage("ucds.month.september", null, LocaleContextHolder.locale)
        months[10] = messageSource.getMessage("ucds.month.october", null, LocaleContextHolder.locale)
        months[11] = messageSource.getMessage("ucds.month.november", null, LocaleContextHolder.locale)
        months[12] = messageSource.getMessage("ucds.month.december", null, LocaleContextHolder.locale)
        return [years: years, months: months]
    }

    def loadParams = {
        def period = getPeriod()
        def years = period.years
        def months = period.months
        def defaultYear = Calendar.getInstance().get(Calendar.YEAR)
        def defaultMonth = Calendar.getInstance().get(Calendar.MONTH) + 1
        def sections = Person.executeQuery("select distinct section from Person order by section", [cache:true])
        def positions = PersonPosition.executeQuery("select name from PersonPosition order by name", [cache:true])
        [years: years, months: months, sections: sections, positions: positions, defaultYear: defaultYear, defaultMonth: defaultMonth]
    }

    def getGapDesc(Integer scoreGap) {
        def desc
        if (scoreGap != null) {
            if (scoreGap == 0) {
                desc = messageSource.getMessage("ucds.evaluation.description.satisfying.message", null, LocaleContextHolder.locale)
            } else if (scoreGap < 0) {
                desc = messageSource.getMessage("ucds.evaluation.description.needtraining.message", null, LocaleContextHolder.locale)
            } else {
                desc = messageSource.getMessage("ucds.evaluation.description.outstanding.message", null, LocaleContextHolder.locale)
            }
        }
        return desc
    }

    Date tryParseDate(String strDate) {
        def res
        try {
            res = (strDate) ? Date.parse(grailsApplication.config.ucds.format.date, strDate) : null
        } catch (Exception e) {
            log.warn(e.getMessage(), e)
        }
        return res
    }

    /**
     * Get number of working days in a month
     * Consider non-weekend holiday & use default monthly annual leave
     * @param m
     * @param y
     * @return
     */
    @Cacheable("TotalWorkingDays")
    def getTotalWorkingDays(int m, int y) {
        def workContractType = WorkContractType.findByName("Default")
        return getTotalWorkingDays(m, y, workContractType)
    }

    /**
     * Get number of working days in a month
     * Consider non-weekend holiday & use given monthly annual leave in workContractType
     * @param m
     * @param y
     * @param workContractType
     * @return
     */
    def getTotalWorkingDays(int m, int y, WorkContractType workContractType) {
        def workingDays = 0f
        def firstDate = new DateTime(y, m, 1, 0, 0)
        int lastDay = firstDate.dayOfMonth().getMaximumValue()
        // weekdays
        for (int i=0;i<lastDay;i++) {
            int day = firstDate.plusDays(i).getDayOfWeek()
            if (!day.equals(DateTimeConstants.SATURDAY) && !day.equals(DateTimeConstants.SUNDAY)) {
                workingDays++
            }
        }
        // non-weekend holiday
        def holiday = Holiday.find { month == m && year == y }
        workingDays = (holiday) ? (workingDays - holiday.number) : workingDays
        // leave
        workingDays = workingDays - (workContractType?.annualLeaveDays / 12)
        return workingDays
    }

    @Cacheable("TotalDayInMonth")
    def getTotalDayInMonth(int d, int m, int y) {
        def total = 0
        def firstDate = new DateTime(y, m, 1, 0, 0)
        int lastDay = firstDate.dayOfMonth().getMaximumValue()
        for (int i=0;i<lastDay;i++) {
            int day = firstDate.plusDays(i).getDayOfWeek()
            if (d.equals(day)) {
                total++
            }
        }
        return total
    }

    @Cacheable("TotalEffectiveMinutes")
    float getTotalEffectiveMinutes(int year) {
        def workContractType = WorkContractType.findByName("Default")
        return getTotalEffectiveMinutes(year, workContractType)
    }

    float getTotalEffectiveMinutes(int year, WorkContractType workContractType) {
        def res = 0f
        if (year >= 1970) {
            def effectiveMinsDaily = this.getEffectiveMinutesDaily(workContractType)
            def effectiveDays = this.getTotalWorkingDays(year, workContractType)
            res = effectiveDays * effectiveMinsDaily
        } else {
            throw Exception("Year must be at least 1970")
        }
        return res
    }

    @Cacheable("TotalWorkingDays")
    float getTotalWorkingDays(int year) {
        def workContractType = WorkContractType.findByName("Default")
        return getTotalWorkingDays(year, workContractType)
    }

    float getTotalWorkingDays(int year, WorkContractType workContractType) {
        def effectiveDays = 0f
        (1..12).each {m -> effectiveDays += this.getTotalWorkingDays(m, year, workContractType)}
        return effectiveDays
    }

    float getEffectiveMinutesDaily() {
        def workContractType = WorkContractType.findByName("Default")
        return getEffectiveMinutesDaily(workContractType)
    }

    float getEffectiveMinutesDaily(WorkContractType workContractType) {
        return workContractType?.averageDailyWorkHours * 60
    }
}
