package com.ucds.core.util

import grails.converters.JSON

class GridUtilService {

    def getDomainList(Class domain, def params){
        def sort = params.sidx && !"".equals(params.sidx) ? params.sidx : null
        def order = params.sord && !"".equals(params.sord) ? params.sord : null
        def max = Math.min(params.rows ? params.int('rows') : 10, 1000)
        def offset = ((params.page ? params.int('page') : 1) - 1) * max

        def whereClause = ""
        def filterData = []
        def whereClauseItems = []

        if (params.boolean('_search')) {
            if (params.filters) {
                def filters = JSON.parse(params.filters)
                filters.rules.each {
                    buildWhereClause(this.getFieldType(domain, it.field), it.field, it.op, it.data.toString(), whereClauseItems, filterData)
                }

                whereClause = whereClauseItems?" where ${whereClauseItems.join(" ${filters.get('groupOp')} ")}":""

            } else if (params.searchField && params.searchOper && params.searchString) {
                buildWhereClause(this.getFieldType(domain, params.searchField), params.searchField, params.searchOper, params.searchString.toString(), whereClauseItems, filterData)
                whereClause = whereClauseItems?" where ${whereClauseItems.join(" and ")}":""

            } else {
                //NA
            }
        } else {
            //NA
        }

        String query = """
            from ${domain.getName()} ${whereClause} ${(sort&&order?("order by ${sort} ${order}"):"")}
        """

        String countQuery = """
            select count(*) from ${domain.getName()} ${whereClause}
        """

        def rows = []
        int totalRecords = 0

        try {
            domain.findAll(query, filterData, [max:max,offset:offset])?.each{
                rows << it
            }
            totalRecords = domain.executeQuery(countQuery, filterData)?.get(0)
        } catch (e) {
            log.error(e.message, e)
        }

        int totalPage = Math.ceil(  totalRecords / max)
        return [rows: rows, totalRecords: totalRecords, page: params.page, totalPage: totalPage]
    }

    def getFieldType(Class domain, String searchField) {
        def field = null
        try {
            field = domain.getDeclaredField(searchField)
        } catch (Exception e) {
            // do nothing
        }
        def type = (field) ? field.type : String.class
    }

    def buildWhereClause(Class fieldType, String field, String op, String data, ArrayList whereClauseItems, ArrayList filterData) {
        switch (op) {
            case "eq":
                switch (field) {
                    case 'nothing':

                        break
                    default:
                        if (data) {
                            if (data == 'true' || data == 'false') {
                                whereClauseItems << "${field} = ?"
                                filterData << Boolean.parseBoolean(data)
                            } else if (fieldType.equals(int.class) || fieldType.equals(int.class)) {
                                whereClauseItems << "${field} = ?"
                                filterData << Integer.parseInt(data)
                            } else if (data.isLong()){
                                whereClauseItems << "${field} = ?"
                                filterData << Long.parseLong(data)
                            } else if (data.isDouble()){
                                whereClauseItems << "${field} = ?"
                                filterData << Double.parseDouble(data)
                            } else{
                                whereClauseItems << "upper(${field}) = upper(?)"
                                filterData << data
                            }
                        } else {
                            whereClauseItems << "${field} is null"
                        }
                        break
                }
                break
            case "ne":
                switch (field) {
                    case 'nothing':

                        break
                    default:
                        if (data) {
                            if (data == 'true' || data == 'false') {
                                whereClauseItems << "${field} != ?"
                                filterData << Boolean.parseBoolean(data)
                            }else if (data.isNumber()){
                                whereClauseItems << "${field} != ?"
                                filterData << data
                            }else{
                                whereClauseItems << "upper(${field}) != upper(?)"
                                filterData << data
                            }
                        } else {
                            whereClauseItems << "${field} is not null"
                        }
                        break
                }
                break
            case "lt":
                //NA
                break
            case "le":
                //NA
                break
            case "gt":
                //NA
                break
            case "ge":
                //NA
                break
            case "bw":
                whereClauseItems << "upper(${field}) like upper(?)||'%'"
                filterData << data
                break
            case "bn":
                whereClauseItems << "upper(${field}) not like upper(?)||'%'"
                filterData << data
                break
            case "ew":
                whereClauseItems << "upper(${field}) like '%'||upper(?)"
                filterData << data
                break
            case "en":
                whereClauseItems << "upper(${field}) not like '%'||upper(?)"
                filterData << data
                break
            case "cn":
                whereClauseItems << "upper(${field}) like '%'||upper(?)||'%'"
                filterData << data
                break
            case "nc":
                whereClauseItems << "upper(${field}) not like '%'||upper(?)||'%'"
                filterData << data
                break
            case "in":
                def inArr = data?.split(",")
                String foo = "${field} in ("
                inArr?.each { n ->
                    foo += "?,"
                    filterData << n?.trim()
                }
                foo += "'xxx')"
                whereClauseItems << foo
                break
            case "ni":
                def inArr = data?.split(",")
                String foo = "${field} not in ("
                inArr?.each { n ->
                    foo += "?,"
                    filterData << n?.trim()
                }
                foo += "'xxx')"
                whereClauseItems << foo
                break
            default:
                whereClauseItems << "upper(${field}) = upper(?)"
                filterData << data == 'true' || data == 'false' ? Boolean.parseBoolean(data) : data
        }
    }

}

