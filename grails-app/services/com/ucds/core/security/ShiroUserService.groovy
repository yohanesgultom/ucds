package com.ucds.core.security

import org.apache.shiro.crypto.hash.Sha256Hash


class ShiroUserService {

    def gridUtilService

    def getList(def params){
        def ret = null

        try {
            def res = gridUtilService.getDomainList(ShiroUser, params)
            def rows = []
            def counter = 0
            ((List<ShiroUser>)(res?.rows))?.each{
                rows << [
                        id: it.id,
                        cell:[
                            it.username,
                            "",
                            it.permissions.join(",")
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception{
        def shiroUser = new ShiroUser()
        
        shiroUser.passwordHash = new Sha256Hash(params.passwordHash).toHex()
        shiroUser.username = params.username
        if (params.permissions) {
            params.permissions.split(",").each { permission ->
                shiroUser.addToPermissions(permission)
            }
        } else {
            shiroUser.addToPermissions("main:*")
        }

        shiroUser.save()
        if (shiroUser.hasErrors()) {
            throw new Exception("${shiroUser.errors}")
        }
    }

    def edit(params){
        def shiroUser = ShiroUser.findById(params.id)
        if (shiroUser) {
            
            shiroUser.passwordHash = new Sha256Hash(params.passwordHash).toHex()
            shiroUser.username = params.username
            if (params.permissions) {
                params.permissions.split(",").each { permission ->
                    shiroUser.addToPermissions(permission)
                }
            } else {
                shiroUser.addToPermissions("main:*")
            }

            shiroUser.save()
            if (shiroUser.hasErrors()) {
                throw new Exception("${shiroUser.errors}")
            }
        }else{
            throw new Exception("ShiroUser not found")
        }
    }

    def delete(params){
        def shiroUser = ShiroUser.findById(params.id)
        if (shiroUser) {
            shiroUser.delete()
        }else{
            throw new Exception("ShiroUser not found")
        }
    }

}