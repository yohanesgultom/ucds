<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Managerial Competency</title>
    <meta name="layout" content="embedded"/>
    <style type="text/css">
    #level-table tr {
        vertical-align: top;
    }
    </style>
    <g:javascript src="jquery-ui.ucds.competency.js"/>
</head>

<body>

<table id="level-table" width="100%">
    <tr>
        <td width="50%">
            <table id="CompetencyGrid"></table>

            <div id="CompetencyPager"></div>
        </td>
        <td width="50%">
            <table id="CompetencyLevelGrid"></table>

            <div id="CompetencyLevelPager"></div>
        </td>
    </tr>
</table>

<script type="text/javascript">

    function initCompetencyGrid() {
        $("#CompetencyGrid").jqGrid({
            url: '${request.getContextPath()}/competency/getFilteredList',
            editurl: '${request.getContextPath()}/competency/edit?type=Managerial',
            postData: {searchField: 'type', searchOper: 'eq', searchString: 'Managerial'},
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames: [
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>'
            ],
            colModel: [
                {name: 'name', index: 'name', editable: true, edittype: 'text', searchoptions: {sopt: ['eq', 'ne', 'cn', 'nc']}},
                {name: 'keyIndicator', index: 'keyIndicator', hidden:true, edittype: 'textarea', searchoptions: {sopt: ['cn', 'nc']}}
            ],
            height: 'auto',
            pager: '#CompetencyPager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Managerial Competency',
            onSelectRow: function (rowid) {
                var rd = $("#CompetencyGrid").jqGrid('getRowData', rowid);
                var listUrl = '${request.getContextPath()}/competencyLevel/getFilteredList';
                var editUrl = '${request.getContextPath()}/competencyLevel/edit?competency_id='+rowid;
                $('#CompetencyLevelGrid').jqGrid('setGridParam', {
                    url: listUrl,
                    editurl: editUrl,
                    postData: {searchField: 'competency_id', searchOper: 'eq', searchString: rowid}
                }).jqGrid('setCaption', 'Competency : ' + rd.name).trigger("reloadGrid");
            },
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#CompetencyGrid').jqGrid(
                'navGrid', '#CompetencyPager',
                {edit: true, add: true, del: true, view: true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function (params, posdata) {
                        var id = $("#CompetencyGrid").jqGrid('getGridParam', 'selrow');
                        var rd = $("#CompetencyGrid").jqGrid('getRowData', id);
                        return {id: rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function initCompetencyLevelGrid() {
        $("#CompetencyLevelGrid").jqGrid({
            editurl: '${request.getContextPath()}/competencyLevel/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames: [
                '<g:message code="ucds.competency.level.label" default="Level"/>',
                '<g:message code="ucds.competency.levelname.label" default="Name"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>',
                '<g:message code="ucds.competency.methods.label" default="Methods"/>'
            ],
            colModel: [
                {name: 'level', index: 'level', width: 20, editable: true, edittype: 'text', editrules: {number:true}, searchoptions: {sopt: ['eq', 'ne']}},
                {name: 'name', index: 'name', width: 50, editable: true, edittype: 'text', searchoptions: {sopt: ['eq', 'ne']}},
                {name: 'keyIndicator', index: 'keyIndicator', width: 100, editable: true, edittype: 'textarea', searchoptions: {sopt: ['cn', 'nc']}},
                {name: 'methods', index: 'methods', hidden: true, editable:true, editrules: {edithidden: true, required: true}, edittype: 'textarea',
                    editoptions: {
                        dataInit:function(el) {
                            initMethodQuestionForm(el,
                                    '${request.getContextPath()}/trainingMethodQuestion/getActiveList',
                                    '${request.getContextPath()}/trainingMethodQuestion/getMethodsByQuestion');
                        }
                    }
                }
            ],
            height: 'auto',
            pager: '#CompetencyLevelPager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Level',
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#CompetencyLevelGrid').jqGrid(
                'navGrid', '#CompetencyLevelPager',
                {edit: true, add: true, del: true, view: true}, // options
                {
                    zIndex: 100,
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    zIndex: 100,
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function (params, posdata) {
                        var id = $("#CompetencyLevelGrid").jqGrid('getGridParam', 'selrow');
                        var rd = $("#CompetencyLevelGrid").jqGrid('getRowData', id);
                        return {id: rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    $(function () {
        initCompetencyGrid()
        initCompetencyLevelGrid();
    });

</script>
</body>
</html>