<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><g:message code="app.name" default="UCDS"/></title>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto"/>
    <link rel="stylesheet" href="${resource(dir: 'css/ucds', file: 'jquery-ui-1.10.4.custom.min.css')}" type="text/css">
    <g:javascript src="jquery-1.8.3.js"/>
    <g:javascript src="jquery-ui-1.10.1.custom.min.js"/>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#signIn').button({
                icons: {
                    primary: 'ui-icon-power'
                }
            }).click(function() {
                $('#signInForm').submit();
                    });

            $('#login-form').dialog({
                closeOnEscape: false,
                resizable: false
            });

            $('#signInForm').keypress(function() {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    this.submit();
                }
            });
        });
    </script>
    <style type="text/css">
        body, input, textarea, select, button {
            font:normal 0.85em Roboto,Tahoma;
        }

        body {
            font-size: 0.7em;
            background-color: #ddd;
        }

        #login-form table td {
            font-size: 0.7em;
        }

        tr#logo-row td {
            text-align: center;
            padding: 1em 0;
        }

        .ui-dialog .ui-dialog-titlebar .ui-dialog-titlebar-close {
            display: none;
        }

        .build-info {
            font-size: 7pt;
            color: rgb(166, 163, 163);
        }
    </style>
</head>

<body>
<div> </div>
<div id="login-form" title="Login">
    <g:if test="${flash.message}">
        <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                ${flash.message}</p>
        </div>
    </g:if>
    <g:form name="signInForm" action="signIn">
        <input type="hidden" name="targetUri" value="${targetUri}"/>
        <table>
            <tbody>
            <tr id="logo-row">
                <td colspan="2"><img src="${resource(dir: 'images', file: 'ucds_logo.png')}" alt="UCDS"/></td>
            </tr>
            <tr>
                <td>Username:</td>
                <td><input type="text" name="username" value="${username}"/></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type="password" name="password" value=""/></td>
            </tr>
            <tr>
                <td>Remember me?:</td>
                <td><g:checkBox name="rememberMe" value="${rememberMe}"/></td>
            </tr>
            <tr>
                <td/>
                <td><span id="signIn">Sign In</span></td>
            </tr>
            </tbody>
        </table>
    </g:form>
</div>
<g:if test="${grailsApplication.config.build.time}">
<div class="build-info">Build ${grailsApplication.config.build.time}</div>
</g:if>
</body>
</html>
