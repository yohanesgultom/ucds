<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="ucds.recruitment.approval" default="Recruitment Approval"></g:message></title>
    <meta name="layout" content="embedded"/>
    <link rel="stylesheet" href="${resource(dir: 'js/tablecloth/css', file: 'tablecloth.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'js/jqueryplot', file: 'jquery.jqplot.min.css')}" type="text/css">
    <style type="text/css">
    td.total-cell, th.total-cell {
        background-color: #DA4F49;
        color: white;
        text-align: center;
    }

    table.matrix {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    table#core-analytical-matrix th,
    table#managerial-analytical-matrix th,
    table#technical-analytical-matrix th {
        background-color: beige;
    }

    td.center {
        text-align: center;
        font-weight: bolder;
    }

    td.ok {
        background-color: white;
        color: black;
    }

    td.not-ok {
        background-color: black;
        color: white;
    }

    p {
        font-weight: bolder;
        color: grey;
    }

    .tab-button {
        font-size: 9pt;
    }

    .tab-button.active {
        color: #dfeffc;
        background: #2e6e9e;
    }
    </style>
</head>

<body>

<!-- tab buttons -->
<span class="tab-button active" value="#core-analytical-matrix"><g:message code="ucds.competency.core" default="Core Competency"/></span>
<span class="tab-button" value="#managerial-analytical-matrix"><g:message code="ucds.competency.managerial" default="Managerial Competency"/></span>
<span class="tab-button" value="#technical-analytical-matrix"><g:message code="ucds.competency.technical" default="Technical Competency"/></span>

<!-- matrix -->
<table id="core-analytical-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>
<table id="managerial-analytical-matrix" class="matrix" style="display:none">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>
<table id="technical-analytical-matrix" class="matrix" style="display:none">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<span id="approve" class="approval-button"><g:message code="ucds.recruitment.approval.approve" default="Approve"/></span>
<span id="reject" class="approval-button"><g:message code="ucds.recruitment.approval.reject" default="Reject"/></span>

<script>
    var coreAnalyticalMatrixTable = $('#core-analytical-matrix');
    var managerialAnalyticalMatrixTable = $('#managerial-analytical-matrix');
    var techAnalyticalMatrixTable = $('#technical-analytical-matrix');

    $(document).ready(function () {

        // initialize page
        initTabButtons();
        initApprovalButtons();
        loadAnalyticalMatrix({});

        // table cloth styling
        $('table.matrix').tablecloth({
            theme: 'stats',
            bordered: true,
            condensed: true
        });

    });

    function initTabButtons() {
        $('.tab-button').button().click(function(){
            $('.matrix').hide();
            $('.tab-button').removeClass('active');
            $(this).addClass('active');
            $($(this).attr('value')).show();
        });
    }

    function initApprovalButtons() {
        $('#approve').button({icons:{primary:'ui-icon-check'}}).click(function() {
            var s = getMatrixSelections();
            if (s.id.length > 0) {
                var msg = "Approve " + s.name + " ?"
                if(confirm(msg)) {
                    $.getJSON('${request.contextPath}/recruitment/candidateApprove', {ids: s.id.toString()}, function(res) {
                        loadAnalyticalMatrix({})
                        alert(res.message);
                    })
                }
            }
        }).css('font-size','0.85em');

        $('#reject').button({icons:{primary:'ui-icon-closethick'}}).click(function() {
            var s = getMatrixSelections();
            if (s.id.length > 0) {
                var msg = "Reject " + s.name + " ?"
                if(confirm(msg)) {
                    $.getJSON('${request.contextPath}/recruitment/candidateReject', {ids: s.id.toString()}, function(res) {
                        loadAnalyticalMatrix({})
                        alert(res.message);
                    })
                }
            }
        }).css('font-size','0.85em');
    }

    function getMatrixSelections() {
        var s = {id:new Array(),name:new Array()};
        $('.matrix-selector:checked',coreAnalyticalMatrixTable).each(function(index,elmt){
            s.id.push(elmt.value);
            s.name.push(elmt.name);
        });
        return s;
    }

    function initMatrixSelector() {
        $('.matrix-selector').on('change', function () {
            $('.matrix-selector[value='+this.value+']').attr('checked',this.checked);
        });
    }

    function loadAnalyticalMatrix(params) {
        // show loading message
        coreAnalyticalMatrixTable.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');
        managerialAnalyticalMatrixTable.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');
        techAnalyticalMatrixTable.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');

        // hide approval buttons
        $('.approval-button').hide();

        // get data from server
        $.get('${g.createLink(controller: "recruitment", action: "analyticalMatrix")}', params, function (response) {
            if (response) {
                if (response.rows && response.rows.length > 0) {
                    populateAnalyticalMatrix(response, coreAnalyticalMatrixTable, 'Core');
                    populateAnalyticalMatrix(response, managerialAnalyticalMatrixTable, 'Managerial');
                    populateAnalyticalMatrix(response, techAnalyticalMatrixTable, 'Technical');

                    // show approval buttons
                    $('.approval-button').show();
                } else {
                    populateMessage(coreAnalyticalMatrixTable, 'No candidate');
                    populateMessage(managerialAnalyticalMatrixTable, 'No candidate');
                    populateMessage(techAnalyticalMatrixTable, 'No candidate');
                }
            } else {
                populateMessage(coreAnalyticalMatrixTable, 'Failed to load data');
                populateMessage(managerialAnalyticalMatrixTable, 'Failed to load data');
                populateMessage(techAnalyticalMatrixTable, 'Failed to load data');
            }
            // init selector
            initMatrixSelector();
        });
    }

    function populateMessage(table, message) {
        table.empty();
        table.append('<tr><td>' + message + '</td></tr>');
    }

    function populateAnalyticalMatrix(response, table, type) {
        // header & subheader
        var header = '<th rowspan=2> </th><th rowspan=2>Name</th><th rowspan=2>Position</th>';
        var subheader = '';
        for (var key in response.header) {
            var value = response.header[key]
            if (value.competencyType == type) {
                header += '<th colspan=' + value.competencyLevel + '>' + value.competencyName + '</th>';
                if (value.competencyLevel > 1) {
                    for (var i = 1; i <= value.competencyLevel; i++) {
                        subheader += '<th>Lv' + i + '</th>';
                    }
                } else {
                    subheader += '<th></th>';
                }
            }
        }
        // add total column
        header += '<th rowspan=2>Total</th>';

        // close header & subheader
        header = '<tr>' + header + '</tr>';
        subheader = '<tr>' + subheader + '</tr>';

        // body
        var body = '';
        var totalByCompetency = new Array();
        $(response.rows).each(function (rowKey, rowValue) {
            var totalByPerson = 0;
            rowKey += 1;
            var checkbox = '<input type="checkbox" class="matrix-selector" name="' + rowValue.personName + '" value="' + rowValue.id + '">';
            var row = '<td>' + checkbox + '</td>' + '<td>' + rowValue.personName + '</td>' + '<td>' + rowValue.personPosition + '</td>';
            for (var headerKey in response.header) {
                var headerValue = response.header[headerKey];
                if (headerValue.competencyType == type) {
                    var competencyScore = rowValue.competencyScore[headerKey];
                    for (var i = 1; i <= headerValue.competencyLevel; i++) {
                        var mark = ((competencyScore && competencyScore.expected == i) || (competencyScore && competencyScore.expected && headerValue.competencyLevel <= 1)) ? competencyScore.gap : '';
                        var classAttr = 'center';
                        var totalByCompetencyKey = headerKey + '' + i;
                        classAttr += (mark != '' && competencyScore) ? ' ' + getClass(competencyScore.gap) : '';
                        classAttr = ' class="' + classAttr + '"';
                        row += '<td' + classAttr + '>' + mark + '</td>';
                        if (mark) {
                            totalByPerson += competencyScore.gap;
                            totalByCompetency[totalByCompetencyKey] = (totalByCompetency[totalByCompetencyKey]) ? totalByCompetency[totalByCompetencyKey] : 0;
                            totalByCompetency[totalByCompetencyKey] += competencyScore.gap;
                        }
                    }
                }
            }
            // append total cell
            row += '<td class="total-cell">' + totalByPerson + '</td>';
            // close row
            body += '<tr>' + row + '</tr>';
        });

        // add total row
        var totalRow = '<td colspan="3"></td>';
        for (var headerKey in response.header) {
            var headerValue = response.header[headerKey];
            if (headerValue.competencyType == type) {
                for (var i = 1; i <= headerValue.competencyLevel; i++) {
                    var totalByCompetencyKey = headerKey + '' + i;
                    var total = (totalByCompetency[totalByCompetencyKey]) ? totalByCompetency[totalByCompetencyKey] : 0;
                    totalRow += '<td class="total-cell">' + total + '</td>';
                }
            }
        }
        totalRow = '<tr>' + totalRow + '<td></td>' + '</tr>';
        body += totalRow;

        // merge
        table.empty();
        table.append(header + subheader + body);
    }

    function getClass(gap) {
        var res;
        if (gap >= 0) {
            res = 'ok';
        } else if (gap < 0) {
            res = 'not-ok';
        }
        return res;
    }

</script>

</body>
</html>