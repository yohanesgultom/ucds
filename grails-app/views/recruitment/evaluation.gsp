<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="ucds.competency.candidateevaluation" default="Candidate Competency Evaluation"></g:message></title>
    <meta name="layout" content="embedded"/>
    <style>
    table td[title='Need training'] {
        color: red;
    }

    #level-table {
        border-spacing: 0;
        border-collapse: collapse;
    }

    #level-table tr td {
        vertical-align: top;
        text-align: justify;
    }

    #level-table tr.even {
        background-color: #f5f5f5;
    }

     #mark-as-evaluated {
         text-align: right;
         margin-bottom: 10px;
     }
    </style>
</head>

<body>

<table id="Evaluation-Grid"></table>
<div id="Evaluation-Pager"></div>

<!-- Evaluation dialog -->
<div id="evaluation-form-dialog">
    <div id="mark-as-evaluated"><g:message code="ucds.recruitment.submitforapproval" default="Submit for Approval"></g:message></div>
    <table id="core-form-grid"></table>
    <div id="core-form-pager"></div>
    <br>
    <table id="managerial-form-grid"></table>
    <div id="managerial-form-pager"></div>
    <br>
    <table id="technical-form-grid"></table>
    <div id="technical-form-pager"></div>
</div>

<!-- Level Selection dialog -->
<div id="level-dialog"></div>

<!-- level table template -->
<table id="level-table-template"></table>

<script type="text/javascript">

    // globals
    var evaluationGrid = $('#Evaluation-Grid');
    var evaluationFormDialog = $('#evaluation-form-dialog');
    var coreFormGrid = $('#core-form-grid');
    var managerialFormGrid = $('#managerial-form-grid');
    var technicalFormGrid = $('#technical-form-grid');

    var levelDialog = $('#level-dialog');
    var levelTableTemplate = $('#level-table-template');
    var markAsEvaluatedButton = $('#mark-as-evaluated');
    var lastSel = '';
    var personPositions = '';
    var competencyLevels = [];

    function initEvaluationGrid() {
        evaluationGrid.jqGrid({
            url: '${request.getContextPath()}/recruitment/newCandidateList',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.name.label" default="Name"/>',
                '<g:message code="ucds.person.gender.label" default="Gender"/>',
                '<g:message code="ucds.person.dateOfBirth.label" default="Date of Birth"/>',
                '<g:message code="ucds.person.applicationlevel.label" default="Application Level"/>',
                '<g:message code="ucds.person.applicationposition.label" default="Application Position"/>',
                '<g:message code="ucds.person.applicationsection.label" default="Application Section"/>',
                '<g:message code="ucds.approval.requester.label" default="Requester"/>',
                '<g:message code="ucds.person.totalgap.label" default="Total Gap"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text',searchoptions:{sopt:['cn']}},
                {name:'gender', index:'gender',editable:true,formatter:'select',edittype:'select',editoptions:{value:'M:Male;F:Female'},search:false},
                {name:'dateOfBirth', index:'dateOfBirth',editable:true,formatter:'date',formatoptions:{newformat:'d/m/Y'},editoptions:{readonly:'readonly', dataInit: initDOBDatePicker},search:false},
                {name:'level', index:'level',editable:true, edittype:'text',search:false},
                {name:'position', index:'position',editable:true, edittype:'select',editoptions:{value:getLOV('${request.contextPath}/recruitment/personPositions', personPositions, true)},formatter:'select',search:false},
                {name:'section', index:'section',editable:true, edittype:'text',search:false},
                {name:'requester',index:'requester',editable:false,search:false},
                {name:'totalGap',index:'totalGap',hidden:true,editable:false,search:false}
            ],
            height: 'auto',
            pager: '#Evaluation-Pager',
            width: 550,
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '<g:message code="ucds.recruitmentcandidates" default="Recruitment Candidates"></g:message>',
            onSelectRow: function (rowid) {
                var rd = evaluationGrid.jqGrid('getRowData', rowid);
                var personName = rd.name;

                // core grid
                coreFormGrid.jqGrid('setGridParam', {
                    url: '${request.getContextPath()}/recruitment/evaluationList?type=Core',
                    editurl: '${request.getContextPath()}/recruitment/evaluate?candidateId=' + rowid,
                    postData: {candidateId: rowid}
                }).trigger('reloadGrid');

                // managerial grid
                managerialFormGrid.jqGrid('setGridParam', {
                    url: '${request.getContextPath()}/recruitment/evaluationList?type=Managerial',
                    editurl: '${request.getContextPath()}/recruitment/evaluate?candidateId=' + rowid,
                    postData: {candidateId: rowid}
                }).trigger('reloadGrid');

                // technical grid
                technicalFormGrid.jqGrid('setGridParam', {
                    url: '${request.getContextPath()}/recruitment/evaluationList?type=Technical',
                    editurl: '${request.getContextPath()}/recruitment/evaluate?candidateId=' + rowid,
                    postData: {candidateId: rowid}
                }).trigger('reloadGrid');

                markAsEvaluatedButton
                        .button({icons: {primary: 'ui-icon-refresh'}})
                        .off()
                        .on('click', function() {
                    $.getJSON('${request.contextPath}/recruitment/markAsEvaluated',{candidateId:rowid},function(res) {
                        if (res) {
                            if (!res.success) {
                                alert(res.message);
                            } else {
                                evaluationFormDialog.dialog('close');
                                evaluationGrid.trigger('reloadGrid');
                            }
                        } else {
                            alert('<g:message code="networkfailure" default="Network failure. Please try again"></g:message>');
                        }
                    });
                });

                evaluationFormDialog.dialog('option', {'title': 'Candidate Competency Evaluation | Name: ' + personName}).dialog('open');
            },
            sortname: 'name',
            sortorder: 'asc'
        });

        evaluationGrid.jqGrid(
                'navGrid', '#Evaluation-Pager',
                {edit: false, add: false, del: false, view: false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );

    }

    /*
     Init evaluation grid with Level Selector
     */
    function initEvaluationFormGrid(caption, gridid, pagerId) {
        var grid = $('#' + gridid);
        grid.jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: false,
            colNames: [
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>',
                '<g:message code="ucds.competency.expectedscore.label" default="Expected Score"/>',
                '<g:message code="ucds.competency.actualscore.label" default="Actual Score"/>',
                '<g:message code="ucds.competency.gap.label" default="Gap"/>',
                '<g:message code="ucds.competency.description.label" default="Description"/>',
                '<g:message code="ucds.competency.competencyId.label" default="Competency ID"/>'
            ],
            colModel: [
                {name: 'name', width: 80, index: 'name', editable: false},
                {name: 'keyIndicator', width: 100, index: 'keyIndicator', editable: false, hidden: true},
                {name: 'expectedScore', width: 50, index: 'expectedScore', editable: true},
                {name: 'actualScore', width: 50, index: 'actualScore', editable: true},
                {name: 'gap', width: 50, index: 'gap', editable: false},
                {name: 'description', width: 100, index: 'description', editable: false},
                {name: 'competencyId', index: 'competencyId', editable: false, hidden: true}
            ],
            width: 980,
            height: 'auto',
            pager: '#' + pagerId,
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: caption,
            onSelectRow: function (rowid) {
                var rowdata = grid.jqGrid('getRowData', rowid);
                openLevelDialog(gridid, rowid, rowdata);
            },
            sortname: 'id',
            sortorder: 'asc'
        });

        grid.jqGrid(
                'navGrid', '#' + pagerId,
                {edit: false, add: false, del: false, search: false, view: false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );
    }

    /*
     Init evaluation grid with Inline Editing (No Level)
     */
    function initEvaluationFormGridNoLevel(caption, gridid, pagerId) {
        var grid = $('#' + gridid);
        grid.jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: false,
            colNames: [
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>',
                '<g:message code="ucds.competency.expectedscore.label" default="Expected Score"/>',
                '<g:message code="ucds.competency.actualscore.label" default="Actual Score"/>',
                '<g:message code="ucds.competency.gap.label" default="Gap"/>',
                '<g:message code="ucds.competency.description.label" default="Description"/>',
                '<g:message code="ucds.competency.competencyId.label" default="Competency ID"/>'
            ],
            colModel: [
                {name: 'name', width: 80, index: 'name', editable: false},
                {name: 'keyIndicator', width: 100, index: 'keyIndicator', editable: false},
                {name: 'expectedScore', width: 50, index: 'expectedScore', editable: true},
                {name: 'actualScore', width: 50, index: 'actualScore', editable: true},
                {name: 'gap', width: 50, index: 'gap', editable: false},
                {name: 'description', width: 100, index: 'description', editable: false},
                {name: 'competencyId', index: 'competencyId', editable: false, hidden: true}
            ],
            width: 980,
            height: 'auto',
            pager: '#' + pagerId,
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: caption,
            onSelectRow: function (rowid) {
                var rowdata = grid.jqGrid('getRowData', rowid);
                if (lastSel && rowid != lastSel) {
                    grid.jqGrid('saveRow', lastSel, {
                        aftersavefunc: afterSave
                    });
                }
                grid.jqGrid('editRow', rowid, {
                    keys: true,
                    aftersavefunc: afterSave
                });
                lastSel = rowid;
            },
            sortname: 'id',
            sortorder: 'asc'
        });

        grid.jqGrid(
                'navGrid', '#' + pagerId,
                {edit: false, add: false, del: false, search: false, view: false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );
    }


    /* Init Dialog */

    function initDialog() {
        evaluationFormDialog.dialog({
            modal: true,
            autoOpen: false,
            height: 'auto',
            width: 1000,
            title: 'Evaluation Form',
            position: {my: 'center top', at: 'center top', of: window}
        }).parent().css({'font-size': '90%'}).css({'z-index': '100'});

        levelDialog.dialog({
            autoOpen: false,
            modal: true,
            title: 'Select Level',
            height: 'auto',
            width: 600,
            position: {my: 'center top', at: 'center top', of: window},
            buttons: {
                "Save": function () {
                    showOverlay();
                    var rowid = $('#rowid', this).val();
                    var gridid = $('#gridid', this).val();
                    var grid = $('#' + gridid);

                    // set value to grid
                    grid.jqGrid('setCell', rowid, 'expectedScore', $('input[name=competencyLevelExpected]:checked', levelDialog).val());
                    grid.jqGrid('setCell', rowid, 'actualScore', $('input[name=competencyLevelActual]:checked', levelDialog).val());

                    // open for edit
                    grid.jqGrid('editRow', rowid, {
                        aftersavefunc: afterSave
                    });
                    grid.jqGrid('saveRow', rowid, {
                        aftersavefunc: afterSave
                    });
                    $(this).dialog('close');
                },
                "Cancel": function () {
                    $(this).dialog('close');
                }
            }
        }).parent().css({'font-size': '90%'}).css({'z-index': '200'});
        ;
    }

    function openLevelDialog(gridid, rowid, rowdata) {
        levelDialog.html(spinner);
        // try to get from cache
        var levels = competencyLevels[rowdata.competencyId];
        if (levels) {
            populateLevelDialog(gridid, rowid, rowdata, levels);
        } else {
            // get from server
            var data = {competencyId: rowdata.competencyId}
            $.getJSON('${request.getContextPath()}/competency/getCompetencyLevels', data, function (response) {
                if (response) {
                    levels = response;
                    competencyLevels[rowdata.competencyId] = levels;
                    populateLevelDialog(gridid, rowid, rowdata, levels);
                }
            });
        }
        // open dialog
        levelDialog.dialog('open');
    }

    function populateLevelDialog(gridid, rowid, rowdata, levels) {
        if (levels) {
            // prepare table
            var levelTableCopy = levelTableTemplate.clone();
            $(levelTableCopy).attr('id', 'level-table').attr('class', 'table table-striped table-condensed');
            levelTableCopy
                    .append('<tr><th colspan="2">Expected Level</th><th colspan="2">Actual Level</th></tr>');
            $(levels).each(function (index) {
                // alternating class
                var altClass = (index % 2 == 0) ? 'even' : 'odd';
                // replace newline if exists
                if (this.keyIndicator.indexOf('\n') > -1) this.keyIndicator = this.keyIndicator.replace(/\n/g, '<br />');
                var levelLabel = 'Level ';
                var keyIndicatorLabel = 'Key Indicator(s):<br>';
                levelTableCopy
                        .append('<tr class=' + altClass + '>' + '<td><input type="radio" name="competencyLevelExpected" value="' + this.level + '" /></td><td>' + levelLabel + this.level + ' ' + this.name + ' <img src="${g.resource(dir: "images", file: "info.png")}" title="' + keyIndicatorLabel + this.keyIndicator + '">' + '</td>' + '<td><input type="radio" name="competencyLevelActual" value="' + this.level + '" /></td><td>' + levelLabel + this.level + ' ' + this.name + ' <img src="${g.resource(dir: "images", file: "info.png")}" title="' + keyIndicatorLabel + this.keyIndicator + '">' + '</td>' + '</tr>');
            });

            // set default value
            if (rowdata.expectedScore && rowdata.expectedScore != '') {
                $('input[name=competencyLevelExpected][value=' + rowdata.expectedScore + ']', levelTableCopy).attr('checked', 'checked');
            } else {
                $('input[name=competencyLevelExpected]:first', levelTableCopy).attr('checked', 'checked');
            }

            if (rowdata.actualScore && rowdata.actualScore != '') {
                $('input[name=competencyLevelActual][value=' + rowdata.actualScore + ']', levelTableCopy).attr('checked', 'checked');
            } else {
                $('input[name=competencyLevelActual]:first', levelTableCopy).attr('checked', 'checked');
            }

            // hidden element
            var rowidElement = '<input id="rowid" type="hidden" value="' + rowid + '"/>';
            var grididElement = '<input id="gridid" type="hidden" value="' + gridid + '"/>';

            // arrange elements in dialog
            levelDialog.empty();
            levelDialog.append(rowidElement).append(grididElement).append(levelTableCopy);
        } else {
            levelDialog.empty();
        }
    }

    function afterSubmitForm(response, postdata) {
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true, ""];
    }

    function afterSave(rowid, response) {
        var res = $.parseJSON(response.responseText);
        $(this).jqGrid('setCell', rowid, 'gap', res.gap);
        $(this).jqGrid('setCell', rowid, 'description', res.desc);
        hideOverlay();
        return true;
    }

    $(document).ready(function () {
        initEvaluationGrid();
        initEvaluationFormGrid('Core Competency', 'core-form-grid', 'core-form-pager');
        initEvaluationFormGrid('Managerial Competency', 'managerial-form-grid', 'managerial-form-pager');
        initEvaluationFormGrid('Technical Competency', 'technical-form-grid', 'technical-form-pager');
        initDialog();
    });

</script>

</body>
</html>