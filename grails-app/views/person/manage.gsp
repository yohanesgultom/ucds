<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>People</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="Person-Grid"></table>
<div id="Person-Pager"></div>

<script type="text/javascript">

    var positions = '';

    function initPersonGrid(){

        $('#Person-Grid').jqGrid({
            url: '${request.getContextPath()}/person/getList',
            editurl: '${request.getContextPath()}/person/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.firstName.label" default="First Name"/>',
                '<g:message code="ucds.person.lastName.label" default="Last Name"/>',
                '<g:message code="ucds.person.gender.label" default="Gender"/>',
                '<g:message code="ucds.person.dateOfBirth.label" default="Date of Birth"/>',
                '<g:message code="ucds.person.level.label" default="Level"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>',
                '<g:message code="ucds.person.section.label" default="Section"/>'
            ],
            colModel :[
                {name:'firstName', index:'firstName',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'lastName', index:'lastName',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'gender', index:'gender',editable:true,formatter:'select',edittype:'select',editoptions:{value:'M:Male;F:Female'}},
                {name:'dateOfBirth', index:'dateOfBirth',editable:true,formatter:'date',formatoptions:{newformat:'d/m/Y'},editoptions:{readonly:'readonly', dataInit: initDOBDatePicker}},
                {name:'level', index:'level',editable:true, edittype:'text'},
                {name:'position', index:'position',editable:true,formatter:'select',edittype:'select', editoptions:{value:positions}},
                {name:'section', index:'section',editable:true, edittype:'text'}
            ],
            height: 'auto',
            pager: '#Person-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Person',
            onSelectRow: function(rowid){
                //NA
            },
            gridComplete: function(){

            },
            sortname: 'firstName',
            sortorder: 'asc'
        });

        $('#Person-Grid').jqGrid(
                'navGrid','#Person-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#Person-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#Person-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
    }
    }, // del options
    {}, // search options
    {} // view options
    );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        $.ajax({
            type: 'GET',
            url:'${request.getContextPath()}/person/selectPositions'
        }).done(function(res){
            positions = res;
            initPersonGrid();
        });
    });

</script>
</body>
</html>