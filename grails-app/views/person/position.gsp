<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Person Position</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="PersonPosition-Grid"></table>
<div id="PersonPosition-Pager"></div>

<script type="text/javascript">

    function initPersonPositionGrid(){
        $('#PersonPosition-Grid').jqGrid({
            url: '${request.getContextPath()}/person/getPositionList',
            editurl: '${request.getContextPath()}/person/editPosition',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.personposition.name.label" default="Name"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}
            ],
            height: 'auto',
            pager: '#PersonPosition-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Person Position',
            onSelectRow: function(rowid){
                var rd = $("#PersonPosition-Grid").jqGrid('getRowData', rowid);
                //TODO
            },
            gridComplete: function(){

            },
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#PersonPosition-Grid').jqGrid(
                'navGrid','#PersonPosition-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#PersonPosition-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#PersonPosition-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        initPersonPositionGrid();
    });

</script>
</body>
</html>