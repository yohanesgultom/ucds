<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title><g:message code="ucds.competency.required" default="Required Competency"/></title>
    <meta name="layout" content="embedded"/>
    <style type="text/css">
    #required-competency-table tr {
        vertical-align: top;
    }
    </style>
</head>
<body>

<table id="Person-Grid"></table>
<div id="Person-Pager"></div>

<div id="required-competency-dialog">
    <table id="required-competency-table" width="80%">
        <tr>
            <td width="50%">
                <table id="required-competency-grid"></table>
                <div id="required-competency-pager"></div>
            </td>
            <td width="50%">
                <table id="competency-grid"></table>
                <div id="competency-pager"></div>
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">

    // globals
    var personGrid = $('#Person-Grid');
    var competencyGrid = $("#competency-grid");
    var requiredCompetencyGrid = $('#required-competency-grid');
    var competencyDialog = $('#required-competency-dialog');
    var lastSel = '';

    function initpersonGrid(){
        personGrid.jqGrid({
            url: '${request.getContextPath()}/person/getEvaluationList',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.firstName.label" default="First Name"/>',
                '<g:message code="ucds.person.lastName.label" default="Last Name"/>',
                '<g:message code="ucds.person.level.label" default="Level"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>'
            ],
            colModel :[
                {name:'firstName', index:'firstName',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'lastName', index:'lastName',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'level', index:'level'},
                {name:'position', index:'position'}
            ],
            height: 'auto',
            pager: '#Person-Pager',
            width: 550,
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'People',
            onSelectRow: function(rowid){
                var rd = personGrid.jqGrid('getRowData', rowid);
                var personName = rd.firstName + ' ' + rd.lastName;
                var listUrl = '${request.getContextPath()}/person/getRequiredCompetency';
                requiredCompetencyGrid.jqGrid('setGridParam', {
                    url: listUrl,
                    postData: {personId: rowid}
                }).trigger("reloadGrid");
                competencyDialog.dialog('option', {'title' : 'Required Competency Name : ' + personName}).dialog('open');
            },
            sortname: 'firstName',
            sortorder: 'asc'
        });

        personGrid.jqGrid(
                'navGrid','#Person-Pager',
                {edit:false,add:false,del:false,view:true,search:false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );

    }

    function initRequiredCompetencyGrid() {
        requiredCompetencyGrid.jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames: [
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>'
            ],
            colModel: [
                {name: 'name', index: 'name', editable: true, edittype: 'text', searchoptions: {sopt: ['eq', 'ne', 'cn', 'nc']}},
                {name: 'keyIndicator', index: 'keyIndicator', hidden: true, editable: true, edittype: 'textarea', editrules: {edithidden: true}, searchoptions: {sopt: ['cn', 'nc']}}
            ],
            height: 'auto',
            pager: '#required-competency-pager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Required Competency',
            sortname: 'id',
            sortorder: 'asc'
        });

        requiredCompetencyGrid.jqGrid(
                'navGrid', '#required-competency-pager',
                {edit: false, add: false, del: false, view: true, search:false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        ).jqGrid('navSeparatorAdd', '#required-competency-pager',{sepclass : 'ui-separator',sepcontent: ''})
                .jqGrid('navButtonAdd','#required-competency-pager',{ caption:'Remove', buttonicon:'ui-icon-circle-triangle-e', onClickButton:removeRequiredCompetency, position: 'last', title:'', cursor: 'pointer'});
    }

    function initCompetencyGrid() {
        competencyGrid.jqGrid({
            url: '${request.getContextPath()}/competency/getFilteredList',
            postData: {searchField: 'type', searchOper: 'eq', searchString: 'Technical'},
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames: [
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>'
            ],
            colModel: [
                {name: 'name', index: 'name', editable: true, edittype: 'text', searchoptions: {sopt: ['eq', 'ne', 'cn', 'nc']}},
                {name: 'keyIndicator', index: 'keyIndicator', hidden: true, editable: true, edittype: 'textarea', editrules: {edithidden: true}, searchoptions: {sopt: ['cn', 'nc']}}
            ],
            height: 'auto',
            pager: '#competency-pager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Competency',
            sortname: 'id',
            sortorder: 'asc'
        });

        competencyGrid.jqGrid(
                'navGrid', '#competency-pager',
                {edit: false, add: false, del: false, view: true, search:false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        ).jqGrid('navSeparatorAdd', '#competency-pager',{sepclass : 'ui-separator',sepcontent: ''})
         .jqGrid('navButtonAdd','#competency-pager',{ caption:'Add', buttonicon:'ui-icon-circle-triangle-w', onClickButton:addRequiredCompetency, position: 'last', title:'', cursor: 'pointer'});
    }

    /* Init Dialog */

    function initDialog() {
        competencyDialog.dialog({
            autoOpen: false,
            height: 600,
            width: 950,
            modal: true,
            title: 'Evaluation Form'
        }).parent().css({'font-size' : '90%'});
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    function addRequiredCompetency(){
        var competencyRowid = competencyGrid.jqGrid('getGridParam', 'selrow');
        if (competencyRowid) {
            var personRowid = personGrid.jqGrid('getGridParam', 'selrow');
            var data = {personId: personRowid, competencyId: competencyRowid};
            var addUrl = '${request.getContextPath()}/person/addRequiredCompetency';
            $.post(addUrl, data, function(response) {
                if (response) {
                    //alert(response.message);
                    requiredCompetencyGrid.trigger("reloadGrid");
                } else {
                    alert('error');
                }
            });
        } else {
            alert('Please select a record');
        }
    }

    function removeRequiredCompetency(){
        var competencyRowid = requiredCompetencyGrid.jqGrid('getGridParam', 'selrow');
        if (competencyRowid) {
            var personRowid = personGrid.jqGrid('getGridParam', 'selrow');
            var data = {personId: personRowid, competencyId: competencyRowid};
            var addUrl = '${request.getContextPath()}/person/removeRequiredCompetency';
            $.post(addUrl, data, function(response) {
                if (response) {
                    //alert(response.message);
                    requiredCompetencyGrid.trigger("reloadGrid");
                } else {
                    alert('error');
                }
            });
        } else {
            alert('Please select a record');
        }
    }

    $(document).ready(function(){
        initpersonGrid();
        initRequiredCompetencyGrid();
        initCompetencyGrid();
        initDialog();
    });

</script>


</body>
</html>