<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Working day & Holiday</title>
    <meta name="layout" content="embedded"/>
    <style type="text/css">
        .summary {font-size: 9pt; margin-bottom: 20px;}
    </style>
</head>
<body>

<div class="action-bar">
    <select id="year"></select>
</div>

<div class="summary">
    <div><g:message code="monthlyLeave" default="Monthly leave"/> : <span id="monthlyLeave"></span> day(s)</div>
    <div><g:message code="effectiveDays" default="Working days"/> : <span id="effectiveDays"></span> day(s)</div>
    <div><g:message code="effectiveMins" default="Working mins"/> : <span id="effectiveMins"></span> minute(s)</div>
    <div><g:message code="effectiveMinsMonthly" default="Avg working mins / month"/> : <span id="effectiveMinsMonthly"></span> minute(s)</div>
    <div><g:message code="effectiveMinsWeekly" default="Avg working mins / week"/> : <span id="effectiveMinsWeekly"></span> minute(s)</div>
    <div><g:message code="effectiveMinsDaily" default="Avg working mins / day"/> : <span id="effectiveMinsDaily"></span> minute(s)</div>
</div>

<table id="Holiday-Grid"></table>
<div id="Holiday-Pager"></div>

<script type="text/javascript">

    var months = '${months}';
    var years = '${years}';

    function initHolidayGrid(){
        $('#Holiday-Grid').jqGrid({
            url: '${request.getContextPath()}/holiday/getList',
            editurl: '${request.getContextPath()}/holiday/edit',
            datatype: 'json',
            mtype: 'POST',
            search: true,
            postData: {searchField: 'year', searchOper: 'eq', searchString: $('#year').val()},
            autowidth: true,
            colNames:[

                '<g:message code="app.holiday.month.label" default="Month"/>',
                '<g:message code="app.holiday.year.label" default="Year"/>',
                '<g:message code="app.holiday.number.label" default="Non-Weekend Holiday (days)"/>',
                '<g:message code="app.holiday.workingDay.label" default="Working Day (days)"/>',
                '<g:message code="app.holiday.note.label" default="Note"/>'

            ],
            colModel :[
                {name:'month', index:'month',editable:true,formatter:'select',edittype:'select',editoptions:{value:months},searchoptions:{sopt:['eq','ne']}, width: 50},
                {name:'year', index:'year',editable:true,formatter:'select',edittype:'select',editoptions:{value:years},searchoptions:{sopt:['eq','ne']}, width: 30},
                {name:'number', index:'number',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne']},editrules: {number:true}, width: 50},
                {name:'workingDay', index:'workingDay',editable:false,searchoptions:{sopt:['eq','ne']},width: 50},
                {name:'note', index:'note',editable:true,edittype:'textarea'}
            ],
            height: 'auto',
            pager: '#Holiday-Pager',
            rowNum: 12,
            rowList:[12],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Working Day & Holiday',
            onSelectRow: function(rowid){
                var rd = $("#Holiday-Grid").jqGrid('getRowData', rowid);
                //TODO
            },
            gridComplete: function(){

            },
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#Holiday-Grid').jqGrid(
                'navGrid','#Holiday-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitFormHoliday,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitFormHoliday,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitFormHoliday,
                    onclickSubmit: function(params, posdata){
                        var id = $("#Holiday-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#Holiday-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    function populateDropDown(sel, val) {
        if (sel && val) {
            $(sel).empty();
            var arr = val.split(';');
            if (arr && arr.length > 0) {
                for (var i=0;i<arr.length;i++) {
                    var opt = arr[i].split(':');
                    if (opt && opt.length == 2) {
                        $(sel).append($('<option>').attr('value', opt[0]).text(opt[1]));
                    }
                }
            }
        }
    }

    function loadSummary() {
        showOverlay();
        var year = $('#year').val();
        $.getJSON('${request.getContextPath()}/holiday/getSummary', {year: year}, function(res) {
            if (res) {
                $.each(res, function(key, value) {
                    $('#' + key).text(value);
                });
            }
            hideOverlay();
        });
    }

    /**
     * Parse JSON response and alert response.message
     * @param response
     * @param postdata
     * @returns {*[]}
     */
    function afterSubmitFormHoliday(response, postdata){
        var res = $.parseJSON(response.responseText);
        //alert(res.message);
        var success = (res.message) && ((res.message.toLowerCase().indexOf('success') > -1) || (res.message.toLowerCase().indexOf('sukses') > -1));
        loadSummary();
        return [success, res.message];
    }

    $(document).ready(function(){
        var sel = $('#year');
        populateDropDown(sel, years);
        initHolidayGrid();
        sel.change(function() {
            loadSummary();
            $('#Holiday-Grid').jqGrid('setGridParam', {
                postData: {searchField: 'year', searchOper: 'eq', searchString: sel.val()}
            }).trigger("reloadGrid");
        });
        loadSummary();
    });

</script>
</body>
</html>