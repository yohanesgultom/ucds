<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>ShiroUser</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="ShiroUser-Grid"></table>
<div id="ShiroUser-Pager"></div>

<script type="text/javascript">

    function initShiroUserGrid(){
        $("#ShiroUser-Grid").jqGrid({
            url: '${request.getContextPath()}/shiroUser/getList',
            editurl: '${request.getContextPath()}/shiroUser/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.shiroUser.username.label" default="Username"/>',
                '<g:message code="app.shiroUser.password.label" default="Password"/>',
                '<g:message code="app.shiroUser.permissions.label" default="Permissions"/>'
            ],
            colModel :[
                {name:'username', index:'username',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'passwordHash', index:'password',editable:true,edittype:'password'},
                {name:'permissions', index:'permissions',editable:true,edittype:'textarea'}
            ],
            height: 'auto',
            pager: '#ShiroUser-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'ShiroUser',
            onSelectRow: function(rowid){
                var rd = $("#ShiroUser-Grid").jqGrid('getRowData', rowid);
                //TODO
            },
            gridComplete: function(){

            },
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#ShiroUser-Grid').jqGrid(
                'navGrid','#ShiroUser-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    // todo add password confirmation & validation
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    // todo add password confirmation & validation
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#ShiroUser-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#ShiroUser-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        initShiroUserGrid();
    });

</script>

</body>
</html>