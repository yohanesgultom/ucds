<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Training Method</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="TrainingMethod-Grid"></table>
<div id="TrainingMethod-Pager"></div>

<script type="text/javascript">

    function initTrainingMethodGrid(){
        $("#TrainingMethod-Grid").jqGrid({
            url: '${request.getContextPath()}/TrainingMethod/getList',
            editurl: '${request.getContextPath()}/TrainingMethod/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.trainingmethod.name.label" default="Name"/>',
                '<g:message code="app.trainingmethod.description.label" default="Description"/>'
            ],
            colModel :[
                {name:'name', index:'name',editable:true,edittype:'text',editrules: {required:true}},
                {name:'description', index:'description',editable:true,edittype:'textarea'}
            ],
            height: 'auto',
            pager: '#TrainingMethod-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'TrainingMethod',
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#TrainingMethod-Grid').jqGrid(
                'navGrid','#TrainingMethod-Pager',
                {edit:true,add:true,del:true,view:true,search:false}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#TrainingMethod-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#TrainingMethod-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {}, // view options
                {} // search options
        );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        initTrainingMethodGrid();
    });

</script>

</body>
</html>