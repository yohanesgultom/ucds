<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Workload Analysis</title>
    <meta name="layout" content="embedded"/>
    <style type="text/css">
    .grid {
        margin-bottom: 3em;
    }
    </style>
</head>
<body>

<div id="Person-Grid-Container" class="grid">
    <table id="Person-Grid"></table>
    <div id="Person-Pager"></div>
</div>

<div id="Workload-Grid-Container" class="grid" style="display: none">
    <table id="Workload-Grid"></table>
    <div id="Workload-Pager"></div>
</div>

<!-- Evaluation dialog -->
<div id="workload-form-dialog">
    <table id="workload-form-grid"><tr><td>&nbsp;</td></tr></table>
    <div id="workload-form-pager"></div>
</div>

<script type="text/javascript">

    // globals
    var personGridContainer = $('#Person-Grid-Container');
    var workloadGridContainer = $('#Workload-Grid-Container');
    var personGrid = $('#Person-Grid');
    var workloadGrid = $('#Workload-Grid');
    var workloadFormDialog = $('#workload-form-dialog');
    var workloadFormGrid = $('#workload-form-grid');

    // todo parameterized?
    var unitOutput = 'Data:Data;Document:Document;Activity:Activity';
    var unitWorkload = 'DAILY:Daily;WEEKLY:Weekly;MONTHLY:Monthly;ANNUALLY:Annually';
    var criteria = 'A:Very High;B:High;C:Medium;D:Low;E:Very Low'
    var workContractTypes = getLOV('${g.createLink(controller: "workContractType", action: "getLOV")}');

    var lastSel;

    function initPersonGrid(){
        personGrid.jqGrid({
            url: '${request.getContextPath()}/person/getEvaluationList',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="ucds.person.firstName.label" default="First Name"/>',
                '<g:message code="ucds.person.lastName.label" default="Last Name"/>',
                '<g:message code="ucds.person.level.label" default="Level"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>'
            ],
            colModel :[
                {name:'firstName', index:'firstName'},
                {name:'lastName', index:'lastName'},
                {name:'level', index:'level'},
                {name:'position', index:'position'}
            ],
            height: 'auto',
            pager: '#Person-Pager',
            width: 550,
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Workload Analysis',
            onSelectRow: function (rowid) {
                toggleGrid('Workload-Grid');
                var personData = personGrid.jqGrid('getRowData', rowid);
                var personFullName = (personData.lastName) ? personData.firstName + ' ' + personData.lastName : personData.firstName;
                var listUrl = '${request.getContextPath()}/workload/getFilteredList';
                var editUrl = '${request.getContextPath()}/workload/edit?personId='+rowid;
                workloadGrid.jqGrid('setGridParam', {
                    url: listUrl,
                    editurl: editUrl,
                    postData: {searchField: 'person.id', searchOper: 'eq', searchString: rowid}
                }).jqGrid('setCaption', 'Workload Form Analysis for ' + personFullName).trigger('reloadGrid');
            },
            sortname: 'firstName',
            sortorder: 'asc'
        });

        personGrid.jqGrid(
                'navGrid','#Person-Pager',
                {edit:false,add:false,del:false,search:false,view:false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );

    }

    function initWorkloadGrid(){

        workloadGrid.jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.workload.position.label" default="Position"/>',
                '<g:message code="app.workload.section.label" default="Section"/>',
                '<g:message code="app.workload.period.label" default="Period"/>',
                '<g:message code="app.workload.summary.label" default="Summary"/>',
                '<g:message code="app.workload.manpoweravailable.label" default="Available Manpower"/>',
                '<g:message code="app.workload.workload.label" default="Workload"/>',
                '<g:message code="app.workload.effectiveness.label" default="Effectiveness"/>',
                '<g:message code="app.workload.criteria.label" default="Criteria"/>',
                '<g:message code="app.workload.dailyWorkingTime.label" default="Daily"/>',
                '<g:message code="app.workload.weeklyWorkingTime.label" default="Weekly"/>',
                '<g:message code="app.workload.monthlyWorkingTime.label" default="Monthly"/>',
                '<g:message code="app.workload.annualWorkingTime.label" default="Annual"/>',
                '<g:message code="app.workload.workContractType.label" default="Work Contract Type"/>'
            ],
            colModel :[
                {name:'position',index:'position',editable:false},
                {name:'section',index:'section',editable:false},
                {name:'period', index:'period',editable:false},
                {name:'summary',index:'summary',editable:true,edittype:'textarea'},
                {name:'manpowerAvailable',index:'manpowerAvailable',width:50,editable:true,edittype:'text',editrules:{number:true}},
                {name:'workload',index:'workload',width:50,editable:false},
                {name:'effectiveness',index:'effectiveness',width:50,editable:false,formatter:'number', formatoptions:{decimalSeparator:".", thousandsSeparator:",",decimalPlaces:4,defaultValue: '0.00'}},
                {name:'criteria', index:'criteria',width:50,formatter:'select',editoptions:{value:criteria},editable:false},
                {name:'dailyWorkingTime',index:'dailyWorkingTime',width:50,formatter:'text',editable:false},
                {name:'weeklyWorkingTime',index:'weeklyWorkingTime',width:50,formatter:'text',editable:false},
                {name:'monthlyWorkingTime',index:'monthlyWorkingTime',width:50,formatter:'text',editable:false},
                {name:'annualWorkingTime',index:'annualWorkingTime',width:50,formatter:'text',editable:false},
                {name:'workContractType',index:'workContractType',edittype:'select',formatter:'select',editoptions:{value:workContractTypes},editrules:{edithidden:true},hidden:true,editable:true}
            ],
            height: 'auto',
            pager: '#Workload-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Workload Analysis List',
            sortname: 'id',
            sortorder: 'asc'
        });

        workloadGrid.jqGrid(
                'navGrid','#Workload-Pager',
                {edit:true,add:true,del:true,search:false,view:false}, // options
                {
                    closeAfterEdit:true,
                    beforeShowForm: function(form) {
                        $('#tr_workContractType', form).hide();
                    },
                    afterShowForm: afterShowWorkloadForm
                }, // edit options
                {
                    closeAfterAdd:true,
                    afterSubmit: afterSubmitWorkload,
                    afterShowForm: afterShowWorkloadForm
                }, // add options
                {}, // del options
                {}, // search options
                {} // view options
        )
                .jqGrid('navButtonAdd','#Workload-Pager',{ caption:'Workload Analysis Form', buttonicon:'ui-icon-pencil', onClickButton:editSelectedWorkloadAnalysis, position: 'last', title:'', cursor: 'pointer'})
                .jqGrid('navButtonAdd','#Workload-Pager',{ caption:'Back', buttonicon:'ui-icon-arrowreturnthick-1-w', onClickButton:function() {toggleGrid('Person-Grid');}, position: 'last', title:'', cursor: 'pointer'});
    }

    function initWorkloadFormGrid(){

        workloadFormGrid.jqGrid({
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                '<g:message code="app.workloaditem.name.label" default="Activity Name"/>',
                '<g:message code="app.workloaditem.unitoutput.label" default="Output Unit"/>',
                '<g:message code="app.workloaditem.unitworkload.label" default="Workload Unit"/>',
                '<g:message code="app.workloaditem.workload.label" default="Output Load"/>',
                '<g:message code="app.workloaditem.duration.label" default="Duration (min)"/>',
                '<g:message code="app.workloaditem.effectiveworktime.label" default="Effective Worktime"/>',
                '<g:message code="app.workloaditem.manpowerrequired.label" default="Required Manpower"/>',
                '<g:message code="app.workloaditem.workloadduration.label" default="Workload Duration"/>',
                '<g:message code="app.workloaditem.effectiveworkloadduration.label" default="Effective Workload Duration"/>'
            ],
            colModel :[
                {name:'name',index:'name',editable:true,edittype:'text'},
                {name:'unitOutput',width:100,index:'unitOutput',editable:true,edittype:'select',editoptions:{value:unitOutput},formatter:'select'},
                {name:'unitWorkload',width:100,index:'unitWorkload',editable:true,edittype:'select',editoptions:{value:unitWorkload},formatter:'select'},
                {name:'workloadValue',width:80,index:'workloadValue',editable:true,edittype:'text',editrules:{number:true}},
                {name:'duration',width:80,index:'duration',editable:true,edittype:'text',editrules:{number:true}},
                {name:'effectiveWorktime',width:80,index:'effectiveWorktime',editable:false},
                {name:'manpowerRequired',width:100,index:'manpowerRequired',editable:false,formatter:'number', formatoptions:{decimalSeparator:".", thousandsSeparator:",",decimalPlaces:9,defaultValue: '0.00'}},
                {name:'workloadDuration',width:100,index:'workloadDuration',editable:false,formatter:'number', formatoptions:{decimalSeparator:".", thousandsSeparator:",",decimalPlaces:2,defaultValue: '0.00'}},
                {name:'effectiveWorkloadDuration',width:100,index:'effectiveWorkloadDuration',editable:false,formatter:'number',formatoptions:{decimalSeparator:".", thousandsSeparator:",",decimalPlaces:2,defaultValue: '0.00'}}
            ],
            height: 'auto',
            pager: '#workload-form-pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Workload Form',
            onSelectRow: function(rowid){
                if(rowid && rowid!=lastSel){
                    workloadFormGrid.jqGrid('saveRow',lastSel, {
                        aftersavefunc: afterSaveWorkloadItem
                    });
                    lastSel=rowid;
                }
                workloadFormGrid.jqGrid('editRow', rowid, {
                    keys: true,
                    aftersavefunc: afterSaveWorkloadItem
                });
            },
            sortname: 'id',
            sortorder: 'asc'
        });

        workloadFormGrid.jqGrid(
                'navGrid','#workload-form-pager',
                {edit:false,add:true,del:true,view:false}, // options
                {
                    closeAfterEdit:true,
                    afterShowForm:afterShowWorkloadItemForm
                }, // edit options
                {
                    closeAfterAdd:true,
                    afterShowForm:afterShowWorkloadItemForm
                }, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );
    }


    workloadFormDialog.dialog({
        autoOpen: false,
        height: 600,
        width: 1000,
        modal: false,
        title: 'Workload Analysis Form',
        close: function(event, ui) {
            workloadGrid.trigger('reloadGrid');
        }
    }).parent().css({'font-size' : '90%'}).css({'z-index': '100'});

    function editSelectedWorkloadAnalysis() {
        var selRow = workloadGrid.jqGrid('getGridParam', 'selrow');
        if (selRow) {
            editWorkloadAnalysis(selRow);
        } else {
            alert('Please select a row');
        }
    }

    function editWorkloadAnalysis(id) {
        var listUrl = '${request.getContextPath()}/workload/getItemList';
        var editUrl = '${request.getContextPath()}/workload/editItem?workloadId=' + id;
        var personId = personGrid.jqGrid('getGridParam', 'selrow');
        var personData = personGrid.jqGrid('getRowData', personId);
        var personFullName = (personData.lastName) ? personData.firstName + ' ' + personData.lastName : personData.firstName;
        workloadFormGrid.jqGrid('setGridParam', {
            url: listUrl,
            editurl: editUrl,
            postData: {searchField: 'workload_id', searchOper: 'eq', searchString: id}
        }).trigger('reloadGrid');
        workloadFormDialog.dialog('option', 'title', 'Workload Form Analysis for ' + personFullName);
        workloadFormDialog.dialog('open').scrollLeft('0');
    }

    function afterSubmitWorkload(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        editWorkloadAnalysis(res.id);
        return [true, res.message, res.id];
    }

    function afterSaveWorkloadItem(rowid, response) {
        workloadFormGrid.trigger('reloadGrid');
        return true;
    }

    function toggleGrid(gridId) {
        personGridContainer.slideToggle(gridId == 'Person-Grid');
        workloadGridContainer.slideToggle(gridId == 'Workload-Grid');
    }

    function afterShowWorkloadForm(form) {
        addHelpIcons(form, [
            {name: 'workContractType',value: '${g.message(code: "workContractType.help", default: "Contract type that defines average working hours and annual leaves")}'}
        ]);
    }

    function afterShowWorkloadItemForm(form) {
        addHelpIcons(form, [
            {name:'workloadValue',value:'${g.message(code: "workloadValue.help", default: "Average Output target per Workload Unit")}'},
            {name:'duration',value:'${g.message(code: "duration.help", default: "Average duration (in minutes) required to generate a unit of Output")}'}
        ]);
    }

    $(document).ready(function(){
        initPersonGrid();
        initWorkloadGrid();
        initWorkloadFormGrid();
    });
    
</script>


</body>
</html>