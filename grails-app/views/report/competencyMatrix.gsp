<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="ucds.report.competencymatrix" default="Competency Matrix"></g:message></title>
    <meta name="layout" content="embedded"/>
    <link rel="stylesheet" href="${resource(dir: 'js/tablecloth/css', file: 'tablecloth.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'js/tablecloth/css', file: 'prettify.css')}" type="text/css">
    <style>
    table#core-competency-matrix th,
    table#managerial-competency-matrix th,
    table#technical-competency-matrix th {
        background-color: beige;
    }

    td.center {
        text-align: center;
        font-weight: bolder;
    }

    td.satisfying {
        background-color: yellow;
    }

    td.outstanding {
        background-color: aquamarine;
    }

    td.need-training {
        background-color: red;
    }

    p {
        font-size: 11px;
        font-family: verdana;
        font-weight: bolder;
        color: grey;
    }
    </style>
</head>

<body>

<g:render template="/periodAndEmployeeFilter" model="[years:years,months:months,defaultYear:defaultYear,defaultMonth:defaultMonth,sections:sections,positions:positions]" />

<table class="legend">
    <tr><td class="outstanding">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><g:message
            code="ucds.competency.result.outstanding" default="Outstanding"/></td></tr>
    <tr><td class="satisfying">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><g:message
            code="ucds.competency.result.satisfying" default="Satisfying"/></td></tr>
    <tr><td class="need-training">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><g:message
            code="ucds.competency.result.needtraining" default="Need Training"/></td></tr>
</table>

<p><g:message code="ucds.competency.core" default="Core Competency"/></p>
<table id="core-competency-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<p><g:message code="ucds.competency.managerial" default="Managerial Competency"/></p>
<table id="managerial-competency-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<p><g:message code="ucds.competency.technical" default="Technical Competency"/></p>
<table id="technical-competency-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<script type="text/javascript">
    var coreCompetencyMatrixTable = $('#core-competency-matrix');
    var managerialCompetencyMatrixTable = $('#managerial-competency-matrix');
    var techCompetencyMatrixTable = $('#technical-competency-matrix');

    $(function () {
        $('table.matrix').tablecloth({
            theme: 'stats',
            bordered: true,
            condensed: true
        });

        $('table.legend').tablecloth({
            theme: 'stats',
            condensed: true
        });

        $('#reload').button({
            icons: {
                primary: 'ui-icon-refresh'
            }
        }).click(function () {
            var params = getFilterParams();
            loadCompetencyMatrix(params);
        }).css({'font-size': '90%'});

        var params = getFilterParams();
        loadCompetencyMatrix(params);

    });

    function loadCompetencyMatrix(params) {
        // show loading message
        coreCompetencyMatrixTable.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');
        managerialCompetencyMatrixTable.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');
        techCompetencyMatrixTable.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');

        // get data from server
        $.get('${g.createLink(controller: "report", action: "getCompetencyMatrixData")}', params, function (response) {
            if (response) {
                populateCompetencyMatrix(response, coreCompetencyMatrixTable, 'Core');
                populateCompetencyMatrix(response, managerialCompetencyMatrixTable, 'Managerial');
                populateCompetencyMatrix(response, techCompetencyMatrixTable, 'Technical');
            } else {
                populateMessage(coreCompetencyMatrixTable, 'Failed to load data');
                populateMessage(managerialCompetencyMatrixTable, 'Failed to load data');
                populateMessage(techCompetencyMatrixTable, 'Failed to load data');
            }
        });
    }

    function populateMessage(table, message) {
        table.empty();
        table.append('<tr><td>' + message + '</td></tr>');
    }

    function populateCompetencyMatrix(response, table, type) {
        // header & subheader
        var header = '<th rowspan=2>#</th><th rowspan=2>Name</th><th rowspan=2>Position</th>';
        var subheader = '';
        for (var key in response.header) {
            var value = response.header[key]
            if (value.competencyType == type) {
                header += '<th colspan=' + value.competencyLevel + '>' + value.competencyName + '</th>';
                if (value.competencyLevel > 1) {
                    for (var i = 1; i <= value.competencyLevel; i++) {
                        subheader += '<th>Level ' + i + '</th>';
                    }
                } else {
                    subheader += '<th></th>';
                }
            }
        }
        header = '<tr>' + header + '</tr>';
        subheader = '<tr>' + subheader + '</tr>';

        // body
        var body = '';
        $(response.rows).each(function (rowKey, rowValue) {
            rowKey += 1;
            var row = '<td>' + rowKey + '</td>' + '<td>' + rowValue.personName + '</td>' + '<td>' + rowValue.personPosition + '</td>';
            for (var headerKey in response.header) {
                var headerValue = response.header[headerKey];
                if (headerValue.competencyType == type) {
                    var competencyScore = rowValue.competencyScore[headerKey];
                    for (var i = 1; i <= headerValue.competencyLevel; i++) {
                        var mark = ((competencyScore && competencyScore.expected == i) || (competencyScore && competencyScore.expected && headerValue.competencyLevel <= 1)) ? 'X' : '';
                        var classAttr = 'center';
                        classAttr += (mark != '' && competencyScore) ? ' ' + getClass(competencyScore.gap) : '';
                        classAttr = ' class="' + classAttr + '"';
                        row += '<td' + classAttr + '>' + mark + '</td>';
                    }
                }
            }
            body += '<tr>' + row + '</tr>';
        });

        // merge
        table.empty();
        table.append(header + subheader + body);
    }

    function getClass(gap) {
        var res;
        if (gap == 0) {
            res = 'satisfying';
        } else if (gap > 0) {
            res = 'outstanding';
        } else if (gap < 0) {
            res = 'need-training';
        }
        return res;
    }
</script>
</body>
</html>