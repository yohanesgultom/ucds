<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Strategy Objective Report</title>
    <meta name="layout" content="embedded"/>
    <link rel="stylesheet" href="${resource(dir: 'js/js-graph-it_1.0', file: 'js-graph-it.css')}" type="text/css">
    <style>
    .canvas {
        font: 8pt tahoma;
        position: absolute;
    }
    .block {
        position: absolute;
        border: 1px solid #7DAB76;
        padding: 10px;
        text-align: center;
        vertical-align: middle;
    }
    .connector {
        background-color: #FF9900;
    }
    </style>

    <g:javascript src="js-graph-it_1.0/js-graph-it.js"/>
    <script>
    var detailDialog = $('#detail-dialog');

    var openDetailDialog = function(nodeId, nodeLabel){
        detailDialog.empty().append(spinner);
        var params = getFilterParams();
        $.getJSON('${request.getContextPath()}/report/getUnderTargetKeySuccessFactors', {soid:nodeId, period:params.period}, function(response) {
            detailDialog.empty().append($('<h3></h3>').text(nodeLabel));
            $.each(response, function(i, item) {
                var ksf = $('<div></div>')
                        .text(item.ksf.title)
                        .attr('id', item.ksf.id);
                var positions = $('<ul></ul>');
                $.each(item.responsibles, function(j, pos) {
                    var nameAndPos = pos.name + ' (' + pos.position + ')';
                    positions.append($('<li></li>').text(nameAndPos).attr('id', pos.id));
                });
                detailDialog.append(ksf).append(positions);
            });
        });
        detailDialog.dialog('open');
    }

    var draw = function(canvasId, data) {
        var canvas = $('#' + canvasId);
        if (canvas && data) {
            var border = 10;
            var boxw = 100;
            var boxh = 30;
            var space = 100;
            var w = parseInt(canvas.css('width'));
            var h = parseInt(canvas.css('height'));
            var middle = w / 2;

            // nodes
            for (var i= 0; i < data.nodes.length; i++) {
                var n = data.nodes[i].length;
                var required = (n * boxw) + ((n-1) * space);
                var start = (w - required) / 2;
                for (var j = 0; j < n; j++) {
                    var node = data.nodes[i][j];
                    var color = '#BAFFB0';
                    var nodeClasses = 'block draggable';
                    if (node.warning) {
                        color = 'tomato';
                        nodeClasses = 'block draggable block-warning'
                    }
                    var box = $('<div></div>')
                            .attr('id', node.id)
                            .attr('title', 'Score:' + node.actual + '/' + node.target)
                            .text(node.label)
                            .attr('class', nodeClasses)
                            .css('width', boxw)
                            .css('height', boxh)
                            .css('background-color', color)
                            .css('top', border + i * (boxh + space))
                            .css('left', start + j * (boxw + space));
                    if (node.warning) {
                        box.on('click', function() {
                            openDetailDialog($(this).attr('id'), $(this).text());
                        });
                    }
                    canvas.append(box);
                }
            }

            // connectors
            for (var i = 0; i < data.edges.length; i++) {
                var edges = data.edges[i];
                var connectorClasses = 'connector ' + edges.source + ' ' + edges.target;
                var connector = $('<div></div>')
                        .attr('class', connectorClasses)
                        .append('<img src="${resource(dir:'images',file: 'arrow.gif')}" class="connector-end"/>');
                canvas.append(connector);
            }
        }

        initPageObjects();

    }

    function loadGraph(params) {
        var canvasId = 'mainCanvas';
        var canvas = $('#'+canvasId);
        canvas.html(spinner);
        delCanvas(canvasId);
        $.getJSON('${request.getContextPath()}/report/strategyObjectiveGraph', params, function(response){
            canvas.empty();
            draw(canvasId, response);
        });
    }

    $(document).ready(function(){
        // dialog
        detailDialog.dialog({
            modal: true,
            autoOpen: false,
            height: 'auto',
            width: 400,
            title: 'Strategy Objective Details',
            buttons: {
                OK : function() {
                    $(this).dialog('close');
                }
            }
        }).parent().css({'font-size' : '90%'}).css({'z-index' : '100'});

        // reload button
        $('#reload').button({
            icons: {
                primary: 'ui-icon-refresh'
            }
        }).click(function() {
            var params = getFilterParams();
            loadGraph(params);
        }).css({'font-size' : '90%'});

        var params = getFilterParams();
        loadGraph(params);
    });
    </script>
</head>
<body>
<g:render template="/periodFilter" model="[years:years,months:months,defaultYear:defaultYear,defaultMonth:defaultMonth]" />
<div id="detail-dialog"></div>
<div id="mainCanvas" class="canvas" style="width:800px; height:500px"></div>
</body>
</html>