<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="ucds.report.analyticalmatrix" default="Analytical Matrix"></g:message></title>
    <meta name="layout" content="embedded"/>
    <link rel="stylesheet" href="${resource(dir: 'js/tablecloth/css', file: 'tablecloth.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'js/jqueryplot', file: 'jquery.jqplot.min.css')}" type="text/css">
    <style type="text/css">
    td.total-cell, th.total-cell {
        background-color: #DA4F49;
        color: white;
        text-align: center;
    }

    table#core-analytical-matrix th,
    table#managerial-analytical-matrix th,
    table#technical-analytical-matrix th {
        background-color: beige;
    }

    td.center {
        text-align: center;
        font-weight: bolder;
    }

    td.ok {
        background-color: white;
        color: black;
    }

    td.not-ok {
        background-color: black;
        color: white;
    }

    #show-quality-chart, #show-competency-chart {
        background: url(${resource(dir: 'images', file: 'chart_bar.png')}) center left no-repeat;
        padding-left: 22px;
        margin-right: 33px;
    }

    p {
        font-size: 11px;
        font-weight: bolder;
        color: grey;
    }
    </style>
</head>

<body>

<g:render template="/periodAndEmployeeFilter" model="[years:years,months:months,defaultYear:defaultYear,defaultMonth:defaultMonth,sections:sections,positions:positions]" />

<div class="action-bar">
    <a href="#"
       id="show-quality-chart">${g.message(code: "ucds.action.chart.resourcequality.label", default: "Quality Chart")}</a>
    <a href="#"
       id="show-competency-chart">${g.message(code: "ucds.action.chart.competency.label", default: "Competency Chart")}</a>

    <div id="quality-chart-dialog">
        <div id="quality-chart" style="width:95%; height:90%;"></div>
    </div>

    <div id="competency-chart-dialog">
        <div id="competency-chart" style="width:95%; height:90%;"></div>
    </div>
</div>

<p><g:message code="ucds.competency.core" default="Core Competency"/></p>
<table id="core-analytical-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<p><g:message code="ucds.competency.managerial" default="Managerial Competency"/></p>
<table id="managerial-analytical-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<p><g:message code="ucds.competency.technical" default="Technical Competency"/></p>
<table id="technical-analytical-matrix" class="matrix">
    <tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>
</table>

<script>
    var qualityPlot, competencyPlot = null;
    var qualityChartDialog = $('#quality-chart-dialog');
    var competencyChartDialog = $('#competency-chart-dialog');
    var qualityChart = $('#quality-chart');
    var competencyChart = $('#competency-chart');

    var coreAnalyticalMatrixTable = $('#core-analytical-matrix');
    var managerialAnalyticalMatrixTable = $('#managerial-analytical-matrix');
    var techAnalyticalMatrixTable = $('#technical-analytical-matrix');

    $(document).ready(function () {

        // initialize page
        var params = getFilterParams();
        loadAnalyticalMatrix(params);
        loadCharts(params);

        $('table.matrix').tablecloth({
            theme: 'stats',
            bordered: true,
            condensed: true
        });

        qualityChartDialog.dialog({
            autoOpen: false,
            title: 'Resource Quality Chart',
            width: 850,
            height: 500,
            modal: true,
            open: function (event, ui) {
                if (qualityPlot._drawCount === 0) qualityPlot.replot();
            }
        }).parent().css({'font-size': '90%'});

        competencyChartDialog.dialog({
            autoOpen: false,
            title: 'Competency Chart',
            width: 850,
            height: 500,
            modal: true,
            open: function (event, ui) {
                if (competencyPlot._drawCount === 0) competencyPlot.replot();
            }
        }).parent().css({'font-size': '90%'});

        $('#reload').button({
            icons: {
                primary: 'ui-icon-refresh'
            }
        }).click(function () {
            var params = getFilterParams();
            loadAnalyticalMatrix(params);
            loadCharts(params);
        }).css({'font-size': '90%'});

        $('#show-quality-chart').click(function () {
            qualityChartDialog.dialog('open');
        });
        $('#show-competency-chart').click(function () {
            competencyChartDialog.dialog('open');
        });

    });

    function loadAnalyticalMatrix(params) {
        // show loading message
        coreAnalyticalMatrixTable.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');
        managerialAnalyticalMatrixTable.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');
        techAnalyticalMatrixTable.html('<tr><td>${g.message(code: "loading", default: "Loading")}...</td></tr>');

        // get data from server
        $.get('${g.createLink(controller: "report", action: "getAnalyticalMatrixData")}', params, function (response) {
            if (response) {
                populateAnalyticalMatrix(response, coreAnalyticalMatrixTable, 'Core');
                populateAnalyticalMatrix(response, managerialAnalyticalMatrixTable, 'Managerial');
                populateAnalyticalMatrix(response, techAnalyticalMatrixTable, 'Technical');
            } else {
                populateMessage(coreAnalyticalMatrixTable, 'Failed to load data');
                populateMessage(managerialAnalyticalMatrixTable, 'Failed to load data');
                populateMessage(techAnalyticalMatrixTable, 'Failed to load data');
            }
        });
    }

    function populateMessage(table, message) {
        table.empty();
        table.append('<tr><td>' + message + '</td></tr>');
    }

    function populateAnalyticalMatrix(response, table, type) {
        // header & subheader
        var header = '<th rowspan=2>#</th><th rowspan=2>Name</th><th rowspan=2>Position</th>';
        var subheader = '';
        for (var key in response.header) {
            var value = response.header[key]
            if (value.competencyType == type) {
                header += '<th colspan=' + value.competencyLevel + '>' + value.competencyName + '</th>';
                if (value.competencyLevel > 1) {
                    for (var i = 1; i <= value.competencyLevel; i++) {
                        subheader += '<th>Level ' + i + '</th>';
                    }
                } else {
                    subheader += '<th></th>';
                }
            }
        }
        // add total column
        header += '<th rowspan=2>Total</th>';

        // close header & subheader
        header = '<tr>' + header + '</tr>';
        subheader = '<tr>' + subheader + '</tr>';

        // body
        var body = '';
        var totalByCompetency = new Array();
        $(response.rows).each(function (rowKey, rowValue) {
            var totalByPerson = 0;
            rowKey += 1;
            var row = '<td>' + rowKey + '</td>' + '<td>' + rowValue.personName + '</td>' + '<td>' + rowValue.personPosition + '</td>';
            for (var headerKey in response.header) {
                var headerValue = response.header[headerKey];
                if (headerValue.competencyType == type) {
                    var competencyScore = rowValue.competencyScore[headerKey];
                    for (var i = 1; i <= headerValue.competencyLevel; i++) {
                        var mark = ((competencyScore && competencyScore.expected == i) || (competencyScore && competencyScore.expected && headerValue.competencyLevel <= 1)) ? competencyScore.gap : '';
                        var classAttr = 'center';
                        var totalByCompetencyKey = headerKey + '' + i;
                        classAttr += (mark != '' && competencyScore) ? ' ' + getClass(competencyScore.gap) : '';
                        classAttr = ' class="' + classAttr + '"';
                        row += '<td' + classAttr + '>' + mark + '</td>';
                        if (mark) {
                            totalByPerson += competencyScore.gap;
                            totalByCompetency[totalByCompetencyKey] = (totalByCompetency[totalByCompetencyKey]) ? totalByCompetency[totalByCompetencyKey] : 0;
                            totalByCompetency[totalByCompetencyKey] += competencyScore.gap;
                        }
                    }
                }
            }
            // append total cell
            row += '<td class="total-cell">' + totalByPerson + '</td>';
            // close row
            body += '<tr>' + row + '</tr>';
        });

        // add total row
        var totalRow = '<td colspan="3"></td>';
        for (var headerKey in response.header) {
            var headerValue = response.header[headerKey];
            if (headerValue.competencyType == type) {
                for (var i = 1; i <= headerValue.competencyLevel; i++) {
                    var totalByCompetencyKey = headerKey + '' + i;
                    var total = (totalByCompetency[totalByCompetencyKey]) ? totalByCompetency[totalByCompetencyKey] : 0;
                    totalRow += '<td class="total-cell">' + total + '</td>';
                }
            }
        }
        totalRow = '<tr>' + totalRow + '<td></td>' + '</tr>';
        body += totalRow;

        // merge
        table.empty();
        table.append(header + subheader + body);
    }

    function getClass(gap) {
        var res;
        if (gap >= 0) {
            res = 'ok';
        } else if (gap < 0) {
            res = 'not-ok';
        }
        return res;
    }

    function loadCharts(params) {
        qualityChart.html(spinner);
        competencyChart.html(spinner);

        $.getJSON('${request.getContextPath()}/report/resourceQualityDiagram', params, function (diagram) {
            var data = diagram.data;
            var ticks = diagram.ticks;
            var series = diagram.series;
            qualityPlot = $.jqplot('quality-chart', data, {
                seriesDefaults: {
                    renderer: $.jqplot.BarRenderer,
                    rendererOptions: {fillToZero: true},
                    pointLabels: { show: true, location: 'n', edgeTolerance: -15 }
                },
                series: series,
                legend: {
                    show: true,
                    placement: 'insideGrid'
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    },
                    yaxis: {
                        label: 'People',
                        min: 0,
                        tickInterval: 1,
                        tickOptions: {formatString: '%d'}
                    }
                }
            });
        });

        $.getJSON('${request.getContextPath()}/report/competencyDiagram', params, function (diagram) {
            var data = diagram.data;
            var ticks = diagram.ticks;
            competencyPlot = $.jqplot('competency-chart', data, {
                seriesDefaults: {
                    renderer: $.jqplot.BarRenderer,
                    rendererOptions: {fillToZero: true, barWidth: 30, varyBarColor: true},
                    pointLabels: { show: true, location: 'n', edgeTolerance: -15 }
                },
                axesDefaults: {
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        angle: -90,
                        fontSize: '8pt'
                    }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    },
                    yaxis: {
                        label: 'Score',
                        max: 0,
                        tickInterval: 1,
                        tickOptions: {formatString: '%d'}
                    }
                }
            });
        });
    }

</script>

</body>
</html>