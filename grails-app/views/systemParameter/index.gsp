<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>System Parameter</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="SystemParameter-Grid"></table>
<div id="SystemParameter-Pager"></div>

<script type="text/javascript">

    function initSystemParameterGrid(){
        $('#SystemParameter-Grid').jqGrid({
            url: '${request.getContextPath()}/systemParameter/getList',
            editurl: '${request.getContextPath()}/systemParameter/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                
                '<g:message code="app.systemParameter.code.label" default="Code"/>',  
                '<g:message code="app.systemParameter.value.label" default="Value"/>',
                '<g:message code="app.systemParameter.status.label" default="Status"/>'

            ],
            colModel :[
                
                {name:'code', index:'code',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'value', index:'value',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'inactive', index:'inactive',editable:true,formatter:'select',edittype:'select',editoptions:{value:'false:Active;true:Inactive'},searchoptions:{sopt:['eq','ne']}}

            ],
            height: 'auto',
            pager: '#SystemParameter-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'System Parameter',
            onSelectRow: function(rowid){
                var rd = $("#SystemParameter-Grid").jqGrid('getRowData', rowid);
                //TODO
            },
            gridComplete: function(){

            },
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#SystemParameter-Grid').jqGrid(
                'navGrid','#SystemParameter-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#SystemParameter-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#SystemParameter-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
    }
    }, // del options
    {}, // search options
    {} // view options
    );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        initSystemParameterGrid();
    });

</script>
</body>
</html>