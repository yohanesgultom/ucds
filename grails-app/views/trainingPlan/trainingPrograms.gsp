<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Training Programs</title>
    <meta name="layout" content="embedded"/>
    <style>
    #training-table tr {
        vertical-align: top;
    }
    #limit-table {
        font-size: 90%;
        vertical-align: middle;
    }
    #costLimit, #timeLimit {
        width: 150px;
    }
    </style>
</head>

<body>

    <g:render template="/periodFilter" model="[years:years,months:months,defaultYear:defaultYear,defaultMonth:defaultMonth]" />
    <input type="hidden" id="totalCost"/>
    <input type="hidden" id="totalTime"/>
    <table id="limit-table">
        <tr>
            <td>${g.message(code: "trainingplan.costlimit", default: "Cost Limit (IDR)")} &nbsp;&nbsp; </td>
            <td><span id="costLimit"></span> &nbsp;&nbsp; ${g.img(dir: "images", file: 'gear.png', class: 'edit-button')}</td>
        </tr>
        <tr>
            <td>${g.message(code: "trainingplan.timelimit", default: "Time Limit (Day)")} &nbsp;&nbsp;</td>
            <td><span id="timeLimit"></span> &nbsp;&nbsp; ${g.img(dir: "images", file: 'gear.png', class: 'edit-button')}</td>
        </tr>
    </table>

    <br>

    <table id="training-table" width="100%">
        <tr>
            <td width="50%">
                <table id="TrainingProgramGrid"></table>
                <div id="TrainingProgramPager"></div>
            </td>
            <td width="50%">
                <table id="TrainingProgramDetailsGrid"></table>
                <div id="TrainingProgramDetailsPager"></div>
            </td>
        </tr>
    </table>

</table>

<script type="text/javascript">
    var periodMonth = $('#periodMonth');
    var periodYear = $('#periodYear');
    var lastSel;

    var grid = $("#TrainingProgramGrid");
    var gridListUrl = '${request.getContextPath()}/trainingProgram/getList';

    var detailsGrid = $("#TrainingProgramDetailsGrid");
    var detailsGridListUrl = '${request.getContextPath()}/trainingProgram/getDetailsList';

    function initTrainingProgramGrid() {
        grid.jqGrid({
            url: gridListUrl,
            postData: getFilterParams(),
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames: [
                '<g:message code="ucds.person.firstName.label" default="First Name"/>',
                '<g:message code="ucds.person.lastName.label" default="Last Name"/>',
                '<g:message code="ucds.person.position.label" default="Position"/>',
                '<g:message code="ucds.competency.name.label" default="Competency"/>',
                '<g:message code="ucds.competency.actualscore.label" default="Actual Score"/>',
                '<g:message code="ucds.competency.expectedscore.label" default="Expected Score"/>',
                '<g:message code="ucds.competency.keyIndicator.label" default="Key Indicator"/>'
            ],
            colModel: [
                {name: 'firstName', index: 'firstName', editable: false, edittype: 'text'},
                {name: 'lastName', index: 'lastName', editable: false, edittype: 'text', hidden: true, editrules: {edithidden: true}},
                {name: 'position', index: 'position',editable: false, edittype: 'text'},
                {name: 'competency', index: 'competency', editable: false, edittype: 'text'},
                {name: 'actualScore', index: 'actualScore', editable: false, edittype: 'text', width: 60},
                {name: 'expectedScore', index: 'expectedScore', editable: false, edittype: 'text', width: 60},
                {name: 'expectedIndicator', index: 'expectedIndicator', editable: false, edittype: 'text'}
            ],
            height: 'auto',
            pager: '#TrainingProgramPager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: false,
            loadui: 'block',
            rownumbers: true,
            caption: 'Training Programs',
            onSelectRow: function (rowid) {
                var rd = grid.jqGrid('getRowData', rowid);
                var listUrl = '${request.getContextPath()}/trainingProgram/getDetailsList';
                var editUrl = '${request.getContextPath()}/trainingProgram/edit';
                detailsGrid.jqGrid('setGridParam', {
                    url: listUrl,
                    editurl: editUrl,
                    postData: {personCompetencyId: rowid}
                }).trigger("reloadGrid");
            },
            sortname: 'id',
            sortorder: 'asc'
        });

        grid.jqGrid(
                'navGrid', '#TrainingProgramPager',
                {edit: false, add: false, del: false, view: false, search: false}, // options
                {}, // edit options
                {}, // add options
                {}, // del options
                {}, // search options
                {} // view options
        );
    }

    function initTrainingProgramDetailsGrid() {
        detailsGrid.jqGrid({
            postData: getFilterParams(),
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames: [
                '<g:message code="ucds.trainingmethod.level.label" default="Level"/>',
                '<g:message code="ucds.trainingmethod.label" default="Method"/>',
                '<g:message code="ucds.trainingmethod.description.label" default="Description"/>',
                '<g:message code="ucds.trainingProgram.trainerOrPic.label" default="Trainer/PIC"/>',
                '<g:message code="ucds.trainingProgram.startDate.label" default="Start Date"/>',
                '<g:message code="ucds.trainingProgram.endDate.label" default="End Date"/>',
                '<g:message code="ucds.trainingProgram.cost.label" default="Cost (IDR)"/>',
                '<g:message code="ucds.trainingProgram.note.label" default="Note"/>'
            ],
            colModel: [
                {name: 'competencyLevel', width: 50, index: 'competencyLevel', editable: false},
                {name: 'trainingMethodDisplay', index: 'trainingMethodDisplay', editable: false},
                {name: 'trainingMethodDescDisplay', index: 'trainingMethodDescDisplay', editable: false},
                {name: 'trainerOrPic', index: 'trainerOrPic', editable: true, edittype: 'text'},
                {name: 'startDate', index: 'startDate', editable: true, edittype: 'text', formatter: 'date', formatoptions: {newformat: 'd-m-Y'}, editoptions: {
                    dataInit:function(el){
                        $(el)
                                .datepicker({dateFormat:'dd-mm-yy', changeYear: true, changeMonth: true, minDate: new Date()})
                                .change(function() {
                                    var val = $(el).val();
                                    if (val) {
                                        var dp = $('#endDate');
                                        var minDate = parseUcdsDate(val);
                                        if (dp.val()) {
                                            var endDate = parseUcdsDate(dp.val());
                                            if (endDate && endDate.getTime() < minDate.getTime()) {
                                                dp.val('');
                                                alert('End date is before start date');
                                            }
                                        }
                                        dp.datepicker('option', 'minDate', minDate);
                                    }
                                });
                    },
                    defaultValue: function() {
                        var currentTime = new Date();
                        return currentTime.getDate() + "-" + parseInt(currentTime.getMonth() + 1) + "-" + currentTime.getFullYear();
                    }
                }},
                {name: 'endDate', index: 'endDate', editable: true, edittype: 'text', formatter: 'date', formatoptions: {newformat: 'd-m-Y'}, editoptions: {
                    dataInit:function(el){
                        $(el)
                                .datepicker({dateFormat:'dd-mm-yy', changeYear: true, changeMonth: true, minDate: new Date() })
                                .change(function() {
                                    var val = $(el).val();
                                    if (val) {
                                        var dp = $('#startDate');
                                        var maxDate = parseUcdsDate(val);
                                        if (dp.val()) {
                                            var startDate = parseUcdsDate(dp.val());
                                            if (startDate && startDate.getTime() > maxDate.getTime()) {
                                                dp.val('');
                                                alert('Start date is after end date');
                                            }
                                        }
                                        dp.datepicker("option", "maxDate", maxDate);
                                    }
                                });
                    },
                    defaultValue: function() {
                        var currentTime = new Date();
                        return currentTime.getDate() + "-" + parseInt(currentTime.getMonth() + 1) + "-" + currentTime.getFullYear();
                    }
                }},
                {name: 'cost', index: 'cost', editable: true, edittype: 'text', editrules:{number: true}, formatter: 'currency', formatoptions: {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2, suffix:"", defaultValue: '0.00'}},
                {name: 'note', index: 'note', editable: true, edittype: 'textarea'}
            ],
            height: 'auto',
            pager: '#TrainingProgramDetailsPager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: false,
            loadui: 'block',
            rownumbers: true,
            caption: 'Training Program Details',
            sortname: 'id',
            sortorder: 'asc'
        });

        detailsGrid.jqGrid(
                'navGrid', '#TrainingProgramDetailsPager',
                {edit: true, add: false, del: true, view: true, search: false}, // options
                {
                    closeAfterEdit: true,
                    afterSubmit: afterSubmitFormTrainingProgramDetails
                }, // edit options
                {
                    closeAfterAdd: true,
                    afterSubmit: afterSubmitFormTrainingProgramDetails
                }, // add options
                {
                    afterSubmit: afterSubmitFormTrainingProgramDetails
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function parseUcdsDate(val, separator) {
        separator = (separator) ? separator : '-';
        var arrVal = val.split(separator);
        return new Date(parseInt(arrVal[2]), parseInt(arrVal[1]) - 1, parseInt(arrVal[0]));
    }

    function afterSubmitFormTrainingProgramDetails(response, postdata){
        loadTotal();
        var res = $.parseJSON(response.responseText);
        var success = (res.message) && ((res.message.toLowerCase().indexOf('success') > -1) || (res.message.toLowerCase().indexOf('sukses') > -1));
        return [success, res.message];
    }

    function loadLimit() {
        var costLimit = $('#costLimit');
        var timeLimit = $('#timeLimit');
        costLimit.html(spinner);
        timeLimit.html(spinner);
        $.getJSON('${request.getContextPath()}/trainingPlan/getLimitCostAndTime', getFilterParams(), function(response) {
            costLimit.html($('<input>').attr('type','text').val(response.costLimit).attr('disabled',true).on('keypress', function(e) {if (e.keyCode == "13") {saveLimit(this); return false;}}));
            timeLimit.html($('<input>').attr('type','text').val(response.timeLimit).attr('disabled',true).on('keypress', function(e) {if (e.keyCode == "13") {saveLimit(this); return false;}}));
        });
    }

    function saveLimit(element) {
        var textbox;
        if ($(element).is('img')) {
            // on img click
            textbox = $(element).prev().find('input');
        } else {
            // on key press
            textbox = $(element);
        }
        var save = !textbox.attr('disabled');
        textbox.attr('disabled', save).focus();
        if (save) {
            var params = getFilterParams();
            var postData = {};
            postData['period'] = params.period;
            postData[textbox.parent().attr('id')] = textbox.val();
            $.getJSON('${request.getContextPath()}/trainingPlan/edit', postData, function(response) {
                if (response.error) {
                    alert(response.error);
                }
                // reload total
                loadTotal();
            });
        }
    }

    function loadTotal() {
        var postData = {};
        var params = getFilterParams();
        postData['period'] = params.period;
        $.getJSON('${request.getContextPath()}/trainingPlan/getTotalCostAndTime', postData, function(response) {
            if (response) {
                var costLimit = $('#costLimit').find('input').val();
                var timeLimit = $('#timeLimit').find('input').val();
                $('#totalCost').val(response.totalCost);
                $('#totalTime').val(response.totalTime);
                var costDelta = costLimit - response.totalCost;
                var timeDelta = timeLimit - response.totalTime;
                var costColor = (costDelta > 0) ? 'green' : 'tomato';
                var timeColor = (timeDelta > 0) ? 'green' : 'tomato';
                var costTitle = (costDelta > 0) ? '' : '${g.message(code: "trainingplan.exceedcost", default: "Cost limit exceeded")} ';
                var timeTitle = (timeDelta > 0) ? '' : '${g.message(code: "trainingplan.exceedtime", default: "Time limit exceeded")} ';
                $('#costLimit').find('input').attr('title', costTitle + costDelta).css('border-color', costColor);
                $('#timeLimit').find('input').attr('title', timeTitle + timeDelta).css('border-color', timeColor);
            }
        });
    }

    // reload button
    $('#reload').button({
        icons: {
            primary: 'ui-icon-refresh'
        }
    }).click(function() {
        loadLimit();

        grid.jqGrid('setGridParam', {
            url: gridListUrl,
            postData: getFilterParams()
        }).trigger("reloadGrid");

        detailsGrid.jqGrid('setGridParam', {
            url: detailsGridListUrl,
            postData: {personCompetencyId: null}
        }).trigger("reloadGrid");
    }).css({'font-size' : '90%'});

    // edit limit button
    $('.edit-button','#limit-table').click(function(){saveLimit(this)});

    $(function () {
        initTrainingProgramGrid();
        initTrainingProgramDetailsGrid();
        loadLimit();
        loadTotal();
    });

</script>
</body>
</html>