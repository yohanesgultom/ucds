<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Work Contract</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="WorkContractType-Grid"></table>
<div id="WorkContractType-Pager"></div>

<script type="text/javascript">

    function initWorkContractTypeGrid(){
        $('#WorkContractType-Grid').jqGrid({
            url: '${request.getContextPath()}/workContractType/getList',
            editurl: '${request.getContextPath()}/workContractType/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[

                '<g:message code="app.workContractType.name.label" default="Name"/>',
                '<g:message code="app.workContractType.description.label" default="Description"/>',
                '<g:message code="app.workContractType.averageDailyWorkHours.label" default="Daily Work Hours"/>',
                '<g:message code="app.workContractType.annualLeaveDays.label" default="Annual Leaves"/>'

            ],
            colModel :[

                {name:'name', index:'name',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'description', index:'description',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'averageDailyWorkHours', index:'averageDailyWorkHours',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}},
                {name:'annualLeaveDays', index:'annualLeaveDays',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}}

            ],
            height: 'auto',
            pager: '#WorkContractType-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: 'Work Contract',
            sortname: 'id',
            sortorder: 'asc'
        });

        $('#WorkContractType-Grid').jqGrid(
                'navGrid','#WorkContractType-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true,
                    afterShowForm: afterShowForm
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true,
                    afterShowForm: afterShowForm
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#WorkContractType-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#WorkContractType-Grid").jqGrid('getRowData', id);
                        return {id : rd.id};
                    }
                }, // del options
                {}, // search options
                {} // view options
        );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    function afterShowForm(form) {
        addHelpIcons(form, [
            {name:'averageDailyWorkHours',value:'${g.message(code: "averageDailyWorkHours.help", default: "Total working hours in a week divided by 5 (five regular working days)")}'},
            {name:'annualLeaveDays',value:'${g.message(code: "annualLeaveDays.help", default: "Average leave days in a year")}'}
        ]);
    }

    $(function(){
        initWorkContractTypeGrid();
    });

</script>
</body>
</html>