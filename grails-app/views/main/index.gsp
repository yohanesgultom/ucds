<%@ page import="org.apache.shiro.SecurityUtils" %>
<!DOCTYPE html>
<html>
<head>

    <title><g:message code="app.name" default="ucds"/></title>

    <!-- $ UI Layout -->
    <g:javascript src="jquery-1.8.3.js"/>
    <g:javascript src="jquery-ui-1.10.1.custom.min.js"/>
    <g:javascript src="jquery.layout-latest.min.js"/>

    <g:javascript src="jquery.jqGrid.min.js"/>
    <g:javascript src="grid.locale-en.js"/>

    <!-- Vertical Accordion -->
    <g:javascript src="jquery.cookie.js"/>
    <g:javascript src="jquery.hoverIntent.minified.js"/>
    <g:javascript src="jquery.dcjqaccordion.2.9.js"/>

    <g:javascript src="tablecloth/js/jquery.tablecloth.js"/>
    <g:javascript src="jqueryplot/jquery.jqplot.min.js"/>
    <!--[if lt IE 9]><g:javascript src="jqueryplot/excanvas.min.js"/><![endif]-->
    <g:javascript src="jqueryplot/plugins/jqplot.dateAxisRenderer.min.js"/>
    <g:javascript src="jqueryplot/plugins/jqplot.canvasTextRenderer.min.js"/>
    <g:javascript src="jqueryplot/plugins/jqplot.canvasAxisTickRenderer.min.js"/>
    <g:javascript src="jqueryplot/plugins/jqplot.barRenderer.min.js"/>
    <g:javascript src="jqueryplot/plugins/jqplot.categoryAxisRenderer.min.js"/>
    <g:javascript src="jqueryplot/plugins/jqplot.pointLabels.min.js"/>

<script type="text/javascript">

        var confirmMsg = '${g.message(code: "default.confirm.message", default: "Are you sure?")}';
        var myLayout; // a var is required because this page utilizes: myLayout.allowOverflow() method
        var spinner = $('<img>').attr('src', '${g.resource(dir: "images", file: 'ajax-loader.gif')}');

        // tooltip
        $(document).tooltip({
            content: function() { return $(this).attr('title'); }
        });

        $(document).ready(function () {
            // $ UI Layout
            myLayout = $('body').layout({
                // enable showOverflow on west-pane so popups will overlap north pane
                //west__showOverflowOnHover: true
                //,	west__fxSettings_open: { easing: "easeOutBounce", duration: 750 }
            });

            // Vertical Accordion
            initMenu();
        });

        /**
         * init vertical accordion menu
         */
        function initMenu() {
            $('.menu', '#dc_jqaccordion_widget-7-item').dcAccordion({
                eventType: 'click',
                hoverDelay: 0,
                menuClose: false,
                autoClose: false,
                saveState: true,
                autoExpand: false,
                classExpand: 'current-menu-item',
                classDisable: '',
                showCount: false,
                disableLink: true,
                cookie: 'dc_jqaccordion_widget-7',
                speed: 'fast'
            });
        }

        function loadPage(url) {
            var content = $('#content');
            $('.ui-dialog').remove();
            content.empty().append(spinner);
            content.load(url, function () {
            });
        }

        function showOverlay() {
            var overlay = $('div.overlay');
            overlay.show();
        }

        function hideOverlay() {
            var overlay = $('div.overlay');
            overlay.hide();
        }

        /* jqGrid Datepicker */

        function initDatePicker(elm) {
            setTimeout(function () {
                $(elm).datepicker({dateFormat: 'dd/mm/yy'});
                $('.ui-datepicker').css({'font-size': '90%'});
            }, 200);
        }

        function initDOBDatePicker(elm) {
            setTimeout(function () {
                $(elm).datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeYear: true,
                    changeMonth: true,
                    defaultDate: '-30y',
                    showOn: 'both',
                    buttonImage: '${resource(dir: "images", file: "calendar-icon.png")}',
                    buttonImageOnly: true
                });
                $('.ui-datepicker').css({'font-size': '90%'});
            }, 200);
        }

        function getJSONDataSync(url) {
            var data = $.ajax({
                url: url,
                dataType: 'json',
                async: false,
                success: function (data, result) {
                    if (!result) {
                        alert('Fail to retrieve lookup data.');
                    }
                }
            }).responseText;
            return JSON.parse(data);
        }



        function parseUcdsDate(val, separator) {
            separator = (separator) ? separator : '-';
            var arrVal = val.split(separator);
            return new Date(parseInt(arrVal[2]), parseInt(arrVal[1]) - 1, parseInt(arrVal[0]));
        }

        /**
         * Parse JSON response and alert response.message
         * @param response
         * @param postdata
         * @returns {*[]}
         */
        function afterSubmitForm(response, postdata){
            var res = $.parseJSON(response.responseText);
            //alert(res.message);
            var success = (res.message) && ((res.message.toLowerCase().indexOf('success') > -1) || (res.message.toLowerCase().indexOf('sukses') > -1));
            return [success, res.message];
        }

        /**
         * Get jqgrid select LOV string from server asyncronously
         * @param url
         * @param cache
         */
        function getLOV(url, cache, flush) {
            if (!cache || flush) {
                cache = '';
                var data = getJSONDataSync(url);
                for (var i = 0; i < data.length; i++) {
                    cache += data[i].id + ':' + data[i].name;
                    if (i < (data.length - 1)) {
                        cache += ';';
                    }
                }
            }
            return cache;
        }

        /**
        * Return mapper of lov
        * @param values
        * @returns {{}}
         */
        function getLOVMapper(values) {
            var mapper = {};
            if (values) {
                $.each(values.split(';'), function(i, str) {
                    var lov = str.split(':');
                    if (lov && lov.length == 2) {
                        mapper[lov[0]] = lov[1];
                    }
                })
            }
            return mapper;
        }

        function addHelpIcons(form, helpList) {
            var imgInfo = $('<img>').attr('src','${g.resource(dir: "images", file: "info.png")}').css('padding-left','3px');
            $.each(helpList, function(i, help){
                $('#tr_'+help.name+' .CaptionTD',form).append(imgInfo.clone().attr('title', help.value));
            });
        }

</script>

    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto"/>
    <link rel="stylesheet" type="text/css" media="screen" href="${g.resource(dir: 'css/ucds', file: 'jquery-ui-1.10.4.custom.min.css')}"/>
    <link rel="stylesheet" type="text/css" media="screen" href="${g.resource(dir: 'css', file: 'ui.jqgrid.css')}"/>

    <!-- Vertical Accordion -->
    <link rel="stylesheet" href="${resource(dir: 'css/accordion', file: 'vertical-accordion.css')}" type="text/css">

    <!-- $ UI Layout -->
    <style type="text/css">
        /**
         *	Basic Layout Theme
         *
         *	This theme uses the default layout class-names for all classes
         *	Add any 'custom class-names', from options: paneClass, resizerClass, togglerClass
         */

    .ui-layout-pane {
        /* all 'panes' */
        background: #FFF;
        border: 1px solid #BBB;
        padding: 10px;
        overflow: auto;
    }

    .ui-layout-north {
        overflow: hidden;
    }

    .ui-layout-west {
        padding: 0;
        background: #ECECEC;
    }

    .ui-layout-resizer {
        /* all 'resizer-bars' */
        background: #DDD;
    }

    .ui-layout-toggler {
        /* all 'toggler-buttons' */
        background: #AAA;
    }

    .overlay {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba(255, 255, 255, .8) url(${resource(dir: "images", file: "ajax-loader.gif")}) 50% 50% no-repeat;
    }

    h3 {
        color: #2E6E9E;
    }

    body, input, textarea, select, button {
        font:normal 0.9em Roboto,Tahoma;
    }

    p {
        margin: 1em 0;
    }

    .ui-layout-north ul ul {
        /* Drop-Down */
        bottom: auto;
        margin: 0;
        margin-top: 1.45em;
    }

    #logo {
        margin-bottom: 2em;
    }

    #app-title {
        margin-left: 0em;
        color: #595959;
        font-size: 0.7em;
    }

    #current-user {
        margin-top: 1em;
        font-size: 0.85em;
        color: gray;
    }

    #current-user a {
        color: red;
        text-decoration: none;
        font-weight: bold;
    }

    .ui-tooltip {
        font: 9pt Roboto,sans-serif;
    }

    .build-info {
        font-size: 7pt;
        color: rgb(225, 222, 222);
        float: right;
    }

</style>
</head>

<body>

<!-- manually attach allowOverflow method to pane -->
<div class="ui-layout-north">
    <g:if test="${grailsApplication.config.build.time}">
    <div class="build-info">Build ${grailsApplication.config.build.time}</div>
    </g:if>
    <div id="logo" role="banner">
        <a href="${resource(uri:'/')}"><img src="${resource(dir: 'images', file: 'ucds_logo.png')}" alt="UCDS"/></a>
        <div id="app-title"><g:message code="app.name" default="UlsoID Competency Development System"></g:message></div>
    </div>
    <div id="current-user">
        <g:message code="menu.user.loginas.label" default="Currently login as"/>: <strong>${SecurityUtils.getSubject().getPrincipals().oneByType(String.class)}</strong> | <g:link controller="auth" action="signOut" onclick="return confirm(confirmMsg)"><g:message
            code="default.signout.label" default="logout"/></g:link>
    </div>
</div>

<!-- allowOverflow auto-attached by option: west__showOverflowOnHover = true -->
<div class="ui-layout-west">
    <div class="dcjq-accordion" id="dc_jqaccordion_widget-7-item">
        <ul id="menu" class="menu">
            <g:each in="${menuInstanceList}" status="i" var="menuInstance">
                <li id="menu-item-${i}" class="menu-item menu-item-type-custom menu-item-object-custom">
                    <a href="#">${g.message(code: menuInstance.label, default: menuInstance.label)}</a>
                    <g:if test="${menuInstance.children?.size() > 0}">
                        <ul class="sub-menu">
                            <g:each in="${menuInstance.children}" status="j" var="menuChild">
                                <li id="menu-item-${i}${j}"
                                    class="menu-item menu-item-type-custom menu-item-object-custom">
                                    <a href="#" target="${menuChild.targetMode}"
                                       onclick="loadPage('${g.createLink(controller: menuChild.linkController, action: menuChild.linkAction)}')">${g.message(code: menuChild.label, default: menuChild.label)}</a>
                                </li>
                            </g:each>
                        </ul>
                    </g:if>
                </li>
            </g:each>
%{--
            <li id="menu-item-user" class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="#"><g:message code="menu.user.label" default="User Menu"/></a>
                <ul class="sub-menu">
                    <li id="menu-item-signout" class="menu-item menu-item-type-custom menu-item-object-custom">
                        <g:link controller="auth" action="signOut" onclick="return confirm(confirmMsg)"><g:message
                                code="default.signout.label" default="Sign out"/></g:link>
                    </li>
                </ul>
            </li>
--}%
        </ul>
    </div>
</div>

<div id="content" class="ui-layout-center"></div>
<div class="overlay"></div>

</body>
</html>