package com.ucds.core

class Menu {
    static self   = '_self'
    static blank = '_blank'

    String menuCode
    String label
    String details
    String url
    String linkController
    String linkAction
    String targetMode
    int seq = 0
    static hasMany = [children:Menu]
    static belongsTo = [parent:Menu]
    static mapping = {
        children sort: 'seq', order: 'asc'
        cache false
    }
    static constraints = {
        menuCode(maxSize: 50)
        label(maxSize: 100)
        details(maxSize: 200, nullable: true)
        url(maxSize: 255, nullable: true)
        linkController(maxSize: 50, nullable: true)
        linkAction(maxSize: 50, nullable: true)
        targetMode(inList: [self,blank], nullable: true)
        parent(nullable: true)
    }
}
