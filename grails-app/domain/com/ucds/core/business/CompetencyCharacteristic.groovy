package com.ucds.core.business

class CompetencyCharacteristic {
    static Types = [Cognitive:"Cognitive", Psychomotor:"Psychomotor", Affective:"Affective"]
    static SubTypes = [
            Cognitive: [Knowledge:"Knowledge", Comperehension: "Comprehension", Application: "Application", Analysis: "Analysis", Synthesis: "Synthesis", Evaluation: "Evaluation"],
            Affective: [Receiving:"Receiving", Responding: "Responding", Valuing: "Valuing", Organizing: "Organizing", Characterizing: "Characterizing"],
            Psychomotor: [Perception:"Perception", Set: "Set", GuidedResponse: "Guided Response", Mechanism: "Mechanism", ComplexOvertResponse: "Complex Overt Response", Adaption: "Adaption", Origination: "Origination"]
    ]

    String name
    String type
    String subType
    static belongsTo = [competencies: Competency]
    static hasMany = [competencies:Competency]
    static constraints = {
        name blank: false
        subType blank: false
    }

    def beforeInsert() {
        this.type = getType(this.subType)
    }

    def beforeUpdate() {
        this.type = getType(this.subType)
    }

    def getType(String subType) {
        def type
        if (subType) {
            if (SubTypes.Affective.containsValue(subType)) {
                type = Types.Affective
            } else if (SubTypes.Psychomotor.containsValue(subType)) {
                type = Types.Psychomotor
            } else if (SubTypes.Cognitive.containsValue(subType)) {
                type = Types.Cognitive
            }
        }
        return type
    }
}
