package com.ucds.core.business

class TrainingPlan {
    Date period
    double costLimit = 0
    double timeLimit = 0
    Date dateCreated
    Date lastUpdated
    String createdBy
    String updatedBy
    static hasMany = [trainingPrograms: TrainingProgram]
    static constraints = {
        period unique: true
        updatedBy nullable: true
        trainingPrograms nullable: true
    }
    static mapping = {
        cache false
    }
}
