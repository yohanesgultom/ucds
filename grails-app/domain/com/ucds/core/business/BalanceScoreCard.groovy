package com.ucds.core.business

class BalanceScoreCard {
    String name
    boolean inactive = false
    static hasMany = [strategyObjectives:StrategyObjective]
    static constraints = {
        name (blank: false)
        strategyObjectives (nullable: true)
    }
    static mapping = {
        cache false
    }
}
