package com.ucds.core.business

class Competency {
    static Types = [Core: "Core", Managerial: "Managerial", Technical: "Technical"]
    String name
    String type
    String keyIndicator
    static belongsTo = [Person,PersonCandidate]
    static hasMany = [
            competencyLevels:CompetencyLevel,
            personCompetencies:PersonCompetency,
            characteristics:CompetencyCharacteristic,
            peopleRequiring:Person,
            candidatesRequiring:PersonCandidate
    ]
    static constraints = {
        name(blank: false)
        type(nullable: true)
        keyIndicator(nullable: true)
    }
    static mapping = {
        cache false
        competencyLevels lazy: false, sort: 'level', order: 'asc'
    }
}
