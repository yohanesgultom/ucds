package com.ucds.core.business

class Person {
    static Genders = [Male:"M", Female:"F"]
    String employeeNumber
    String level
    String position
    String section
    String firstName
    String lastName
    String gender
    Date dateOfBirth
    boolean active = true
    Date dateCreated
    Date lastUpdated
    static hasMany = [
            personCompetencies:PersonCompetency,
            requiredCompetencies:Competency,
            keySuccessFactors:KeySuccessFactor
    ]
    static hasOne = [personPosition:PersonPosition]
    static constraints = {
        employeeNumber(nullable: true)
        firstName(blank: false)
        lastName(nullable: true)
        level(nullable: true)
        position(nullable: true)
        section(nullable: true)
        personPosition(nullable: true)
    }
    static mapping = {
        cache false
    }
}
