package com.ucds.core.business

class StrategyObjective {
    String name
    boolean inactive
    static hasMany = [
            keySuccessFactors:KeySuccessFactor,
            dependencies:StrategyObjectiveDependency,
            affectees:StrategyObjectiveDependency
    ]
    static hasOne = [perspective:BalanceScoreCard]
    static constraints = {
        keySuccessFactors nullable: true
        dependencies nullable: true
    }
    static mappedBy = [
            dependencies: "affectee",
            affectees: "strategyObjective"
    ]
    static mapping = {
        cache false
    }
}
