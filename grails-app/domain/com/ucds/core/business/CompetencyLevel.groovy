package com.ucds.core.business

class CompetencyLevel {
    int level = 0
    String name
    String keyIndicator
    static belongsTo = [competency:Competency]
    static hasMany = [trainingMethods:TrainingMethod]
    static constraints = {
        name(nullable: true)
        keyIndicator(nullable: true)
    }
    static mapping = {
        keyIndicator type: 'text'
        cache false
    }
}
