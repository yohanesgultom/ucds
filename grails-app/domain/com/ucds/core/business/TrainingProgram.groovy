package com.ucds.core.business

class TrainingProgram {
    String trainerOrPIC
    Date startDate
    Date endDate
    String note
    String trainingMethodDisplay
    String trainingMethodDescDisplay
    Integer competencyLevel
    TrainingMethod trainingMethod
    Long cost
    static transients = ['trainingMethodDisplay','trainingMethodDescDisplay']
    static belongsTo = [trainingPlan: TrainingPlan, personCompetency:PersonCompetency]
    static constraints = {
        personCompetency (nullable: false)
        trainerOrPIC (nullable: true)
        startDate (nullable: true)
        endDate (nullable: true)
        note (nullable: true)
        competencyLevel (nullable: true)
        trainingPlan (nullable: true)
        trainingMethod (nullable: true)
        cost (nullable: true)
    }
    static mapping = {
        cache false
    }
}
