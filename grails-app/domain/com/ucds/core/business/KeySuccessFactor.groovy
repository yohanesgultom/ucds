package com.ucds.core.business

class KeySuccessFactor {
    String title
    String kri
    String balanceScoreCard = ""
    String keySuccessIndicatorCategory = ""
    String selectedKpi = ""
    Double percentage
    KeySuccessFactor parent
    Person person
    StrategyObjective strategyObjective
    boolean useKri
    static hasMany = [kpis:Kpi, responsiblePositions:PersonPosition]
    static constraints = {
        title (blank: false)
        kri (nullable: true)
        balanceScoreCard (blank: true)
        keySuccessIndicatorCategory (blank: true)
        selectedKpi (blank: true)
        percentage (nullable: true)
        kpis (nullable: true)
        strategyObjective (nullable: true)
        parent (nullable: true)
        responsiblePositions (nullable: true)
        person (nullable: true)
    }
    static mapping = {
        title type: 'text'
        balanceScoreCard type: 'text'
        keySuccessIndicatorCategory type: 'text'
        cache false
    }

}
