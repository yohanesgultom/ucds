package com.ucds.core.business

class KeySuccessIndicatorCategory {
    String name
    boolean inactive = false
    static constraints = {
        name (blank: false)
    }
    static mapping = {
        cache false
    }
}
