package com.ucds.core.business

class WorkloadItem {
    static UNIT = [DAILY:"DAILY", WEEKLY: "WEEKLY", MONTHLY: "MONTHLY", ANNUALLY: "ANNUALLY"]
    String name
    String unitOutput
    float unitWorkload = 0
    float workloadValue = 0
    float duration = 0
    float manpowerRequired = 0
    float workloadDuration = 0
    float effectiveWorkloadDuration = 0
    static belongsTo = [workload:Workload]
    static constraints = {
        name(blank: false)
    }
    static mapping = {
        cache false
    }
    def recalculate() {
        if (unitWorkload > 0) manpowerRequired = workloadValue * duration / unitWorkload
        if (workload) effectiveWorkloadDuration = manpowerRequired * workload.getUnitWorkload(WorkloadItem.UNIT.ANNUALLY)
        workloadDuration = workloadValue * duration
    }
    def beforeInsert() {
        recalculate()
    }
    def beforeUpdate() {
        recalculate()
    }
    def afterInsert() {
        workload?.recalculate()
    }
    def afterUpdate() {
        workload?.recalculate()
    }
    def afterDelete() {
        workload?.recalculate()
    }
}
