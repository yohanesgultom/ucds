package com.ucds.core.business

class Approval {
    static Category = [Recruitment:"RECRUITMENT"]
    String refCategory
    Long refId
    String status
    String requester
    String approver
    Date dateCreated
    Date lastUpdated
    static hasMany = [histories:ApprovalHistory]
    static constraints = {
        refCategory blank: false
        refId nullable: false
        status blank: false
        requester nullable: true
        approver nullable: true
    }
    static mapping = {
        cache false
    }

    def afterInsert() {
        new ApprovalHistory(requester: requester, approver: approver, status: status, approval: this).save()
    }

    def afterUpdate() {
        new ApprovalHistory(requester: requester, approver: approver, status: status, approval: this).save()
    }
}
