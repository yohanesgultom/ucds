package com.ucds.core.business

class PersonPosition {
    String name
    static hasMany = [people:Person, candidates:PersonCandidate, keySuccessFactors:KeySuccessFactor]
    static belongsTo = [KeySuccessFactor]
    static constraints = {
        name blank: false, unique: true
        people nullable: true
        candidates nullable: true
        keySuccessFactors nullable: true
    }
    static mapping = {
        cache false
    }
}
