package com.ucds.core.business

class StrategyObjectiveDependency {
    Double weight
    static hasOne = [
            strategyObjective:StrategyObjective,
            affectee:StrategyObjective
    ]
    static constraints = {
        weight nullable: true
        affectee nullable: true
        strategyObjective nullable: true
    }
    static mappedBy = {
        affectee: "dependencies"
        strategyObjective: "affectees"
    }
    static mapping = {
        cache false
    }
}
