package com.ucds.core

class SystemParameter {
    String code
    String value
    boolean inactive = false
    static constraints = {
        code(blank: false)
        value(blank: false)
    }
    static mapping = {
        cache false
    }
}
