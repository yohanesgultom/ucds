package com.ucds.core.security

import org.codehaus.groovy.grails.web.json.JSONObject

class ShiroUser implements Serializable {
    String username
    String passwordHash
    String filterString
    Map filter = [:]
    static hasMany = [ roles: ShiroRole, permissions: String ]
    static transients = ['filter']
    static constraints = {
        username(nullable: false, blank: false, unique: true)
        filterString(nullable: true)
    }
    static mapping = {
        cache true
    }

    def afterLoad() {
        filter = (filterString) ? new JSONObject(filterString) : [:]
    }

    def beforeInsert() {
        filterString = (new JSONObject(filter)).toString()
    }

    def beforeUpdate() {
        filterString = (new JSONObject(filter)).toString()
    }
}
