package com.ucds.core

import org.apache.shiro.SecurityUtils

class MainController {

    def index() {
        def filteredMenus = session.getAttribute("menus")
        if (!filteredMenus) {
            filteredMenus = []
            def menus = Menu.findAll("from Menu where parent is null order by seq asc")
            for (Menu menu : menus) {
                def removeChildren = []
                // check non-permitted children menu
                for (Menu childMenu : menu.children) {
                    def permission = "$childMenu.linkController:$childMenu.linkAction"
                    if (!SecurityUtils.subject.isPermitted(permission)) {
                        removeChildren << childMenu
                    }
                }
                // remove non-permitted children
                if (!removeChildren.empty) {
                    menu.children.removeAll(removeChildren)
                }
                // if there is children display it
                if (menu.children && !menu.children.empty) {
                    filteredMenus << menu
                }
            }
            session.setAttribute("menus", filteredMenus)
        }
        [menuInstanceList: filteredMenus]
    }
}
