package com.ucds.core

import grails.converters.JSON

class MenuController {

    def menuService

    def index() { }

    def getList = {
        def res = menuService.getList(params)
        def ret = [rows:res.rows,records:res.totalRecords,page:params.page,total:res.totalPage]
        render ret as JSON
    }

    def edit = {
        def res = [:]
        switch(params.oper){
            case "add" :
                try {
                    menuService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "edit" :
                try {
                    menuService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
            case "del" :
                try {
                    menuService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message?:e.cause?.message
                }
                break
        }

        render res as JSON
    }

}
