package com.ucds.core.business

import grails.converters.JSON

class WorkloadController {

    def workloadService
    def workloadItemService

    def index() {
    }

    def getFilteredList = {
        params._search = true // force reuse search mechanism
        def res = workloadService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def edit = {
        def res = [:]
        switch (params.oper) {
            case "add":
                try {
                    def newId = workloadService.add(params)
                    res.message = "Add Success"
                    res.id = newId
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    def id = workloadService.edit(params)
                    res.message = "Edit Success"
                    res.id = id
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    def id = workloadService.delete(params)
                    res.message = "Delete Success"
                    res.id = id
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }
        render res as JSON
    }

    def getItemList = {
        params._search = true // force reuse search mechanism
        // type checking
        switch(params.searchField) {
            case "workload_id" :
                params.searchString = (params.searchString instanceof String) ? Long.parseLong(params.searchString) : params.searchString;
                break;
        }
        def res = workloadItemService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }


    def editItem = {
        def res = [:]
        switch (params.oper) {
            case "add":
                try {
                    def newId = workloadItemService.add(params)
                    res.message = "Add Success"
                    res.id = newId
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    def id = workloadItemService.edit(params)
                    res.message = "Edit Success"
                    res.id = id
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    def id = workloadItemService.delete(params)
                    res.message = "Delete Success"
                    res.id = id
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }
        render res as JSON
    }

}
