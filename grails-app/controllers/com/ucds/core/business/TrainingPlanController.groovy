package com.ucds.core.business

import grails.converters.JSON
import org.apache.shiro.SecurityUtils

class TrainingPlanController {

    def trainingPlanService
    def trainingProgramService
    def appUtilService

    def index() {}

    def trainingPrograms = {
        return appUtilService.loadParams()
    }

    def getList = {
        def res = trainingPlanService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def edit = {
        def error
        try {
            params.period = params.date('period',"d-M-yyyy")
            def trainingPlan = TrainingPlan.findByPeriod(params.period)
            if (trainingPlan) {
                if(params.costLimit) {
                    trainingPlan.costLimit = params.double('costLimit')
                } else if (params.timeLimit) {
                    trainingPlan.timeLimit = params.double('timeLimit')
                }
                trainingPlan.save(failOnError: true)
            } else {
                error = "Can't find Training Plan"
            }
        } catch (Exception e) {
            error = e.message
            log.error(e)
        }
        def res = [error:error]
        render res as JSON
    }

    def getLimitCostAndTime = {
        def period = Date.parse("d-M-yyyy", params.period)
        def trainingPlan = TrainingPlan.findByPeriod(period)
        trainingPlan = (trainingPlan) ?: new TrainingPlan(period: period, createdBy: SecurityUtils.getSubject().getPrincipals().oneByType(String.class), dateCreated: new Date()).save(failOnError: true)
        def res = [id: trainingPlan.id, period: trainingPlan.period, costLimit: trainingPlan.costLimit, timeLimit: trainingPlan.timeLimit]
        render res as JSON
    }

    def getTotalCostAndTime = {
        def period = Date.parse("d-M-yyyy", params.period)
        def res = trainingProgramService.getTotalCostAndTime(period)
        render res as JSON
    }
}
