package com.ucds.core.business

import grails.converters.JSON

class PersonCompetencyController {

    def personCompetencyService

    def index() {}

    def getList = {
        def res = personCompetencyService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def edit = {
        def res = [:]
        try {
            res = personCompetencyService.edit(params)
            res.message = "success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render res as JSON
    }

    def getEvaluationList = {
        def res = personCompetencyService.getEvaluationList(params)
        def ret = [rows:res.rows,records:res.totalRecords,page:params.page,total:res.totalPage]
        render ret as JSON
    }

}
