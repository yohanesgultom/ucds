package com.ucds.core.business

import grails.converters.JSON

class KpiController {

    def kpiService

    def getEvaluationList = {
        def res = kpiService.getEvaluationList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def clearKpi = {
        def res = [message:null,success:true]
        try {
            kpiService.clearKpiList(params)
        } catch (Exception e) {
            log.error(e.message,e)
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def edit = {
        def res = [:]
        try {
            res = kpiService.edit(params)
            res.message = "success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render res as JSON
    }

}
