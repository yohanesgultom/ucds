package com.ucds.core.business

import com.ucds.core.security.ShiroUser
import grails.converters.JSON
import org.apache.shiro.SecurityUtils

class PersonController {

    def personService
    def competencyService
    def appUtilService

    def index = {
        manage()
    }

    def manage = {}

    def getList = {
        def res = personService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def edit = {
        def res = [:]
        switch (params.oper) {
            case "add":
                try {
                    personService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    personService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    personService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }

        render res as JSON
    }

    def evaluation = {
        return appUtilService.loadParams()
    }

    def evaluationActual = {
        return appUtilService.loadParams()
    }

    def evaluationExpected = {
        return appUtilService.loadParams()
    }

    def getEvaluationList = {
        def user = SecurityUtils.subject.principals.oneByType(ShiroUser)
        def rules = []
        user.filter?.each {
            rules << "{'op':'eq','field':'${it.key}','data':'${it.value}'}"
        }
        if (rules.size() > 0) {
            params._search = true
            params.filters = "{'groupOp':'and','rules':[${rules.join(",")}]}"
        }
        def res = personService.getEvaluationList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def requiredCompetency() {

    }

    def getRequiredCompetency() {
        def res = competencyService.getRequiredCompetencyList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def addRequiredCompetency() {
        def ret
        String msg
        boolean success = false
        try {
            personService.addRequiredCompetency(params)
            msg = "Add success"
            success = true
        } catch (e) {
            msg = "Add failed. Reason: " + e.message
        } finally {
            ret = [success: success, message: msg]
        }
        render ret as JSON
    }

    def removeRequiredCompetency() {
        def ret
        String msg
        boolean success = false
        try {
            personService.removeRequiredCompetency(params)
            msg = "Remove success"
            success = true
        } catch (e) {
            msg = "Remove failed. Reason: " + e.message
        } finally {
            ret = [success: success, message: msg]
        }
        render ret as JSON
    }

    def keySuccessFactor() {
        def cards = BalanceScoreCard.findAllByInactive(false, [sort: "name", order: "asc"])
        def categories = KeySuccessIndicatorCategory.findAllByInactive(false, [sort: "name", order: "asc"])
        [cards: cards, categories: categories]
    }

    def kpi() {
        return appUtilService.loadParams()
    }

    def position = {}

    def getPositionList = {
        def res = personService.getPositionList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def selectPositions = {
        def res = ":--"
        PersonPosition.all?.each {
            res += ";${it.id}:${it.name}"
        }
        render res
    }

    def editPosition = {
        def res = [:]
        switch (params.oper) {
            case "add":
                try {
                    personService.addPosition(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    personService.editPosition(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    personService.deletePosition(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }

        render res as JSON
    }
}
