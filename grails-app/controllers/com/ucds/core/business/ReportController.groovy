package com.ucds.core.business

import com.ucds.core.security.ShiroUser
import grails.converters.JSON
import org.apache.shiro.SecurityUtils

class ReportController {
    def reportService
    def appUtilService

    def index = {}

    def processParams(params) {
        def user = SecurityUtils.subject.principals.oneByType(ShiroUser)
        params.filters = params.filters ? JSON.parse(params.filters) : user.filter
        params.period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
    }

    def competencyMatrix = {
        return appUtilService.loadParams()
    }

    def analyticalMatrix = {
        return appUtilService.loadParams()
    }

    def getCompetencyMatrixData = {
        processParams params
        def ret = reportService.getCompetencyMatrixData(params)
        render ret as JSON
    }

    def getAnalyticalMatrixData = {
        processParams params
        def ret = reportService.getAnalyticalMatrixData(params)
        render ret as JSON
    }

    def resourceQualityDiagram = {
        processParams params
        def data = reportService.getQualityDiagram(params)
        def ticks = [message(code: "ucds.report.description.farbelow.message"), message(code: "ucds.report.description.below.message"), message(code: "ucds.report.description.satisfy.message"), message(code: "ucds.report.description.above.message"), message(code: "ucds.report.description.farabove.message"), message(code: "ucds.report.description.unevaluated.message")];
        def series = [[label:message(code: "ucds.report.competency.category.core.label")],[label:message(code: "ucds.report.competency.category.managerial.label")],[label:message(code: "ucds.report.competency.category.technical.label")]]
        def res = [data: data, ticks: ticks, series: series]
        render res as JSON
    }

    def competencyDiagram = {
        processParams params
        def diagram = reportService.getCompetencyDiagram(params)
        def res = [data: [diagram.scores], ticks: diagram.competencies]
        render res as JSON
    }

    def strategy = {
        return appUtilService.loadParams()
    }

    def strategyObjectiveGraph = {
        def graph = [:]
        params.period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
        try {
            graph = reportService.getStrategyObjectiveGraph(params)
        } catch (Exception e) {
            log.error(e.message, e)
        }
        render graph as JSON
    }

    def getUnderTargetKeySuccessFactors = {
        params.period = Date.parse(grailsApplication.config.ucds.format.date, params.period)
        def graph = reportService.getUnderTargetKeySuccessFactors(params)
        render graph as JSON
    }
}
