package com.ucds.core.business

import grails.converters.JSON
import pl.touk.excel.export.WebXlsxExporter

class TrainingProgramController {

    def trainingProgramService

    def index() {}

    def getList = {
        def res = trainingProgramService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def getFilteredList = {
        params._search = true // force reuse search mechanism
        // type checking
        switch(params.searchField) {
            case "trainingPlan.id" :
                params.searchString = (params.searchString instanceof String) ? Long.parseLong(params.searchString) : params.searchString;
                break;
        }
        def res = trainingProgramService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def getDetailsList = {
        def res = trainingProgramService.getDetailsList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def exportToExcel = {
        def trainingPlan = TrainingPlan.findById(params.trainingPlanId)
        List<TrainingProgram> trainingPrograms = (trainingPlan) ? TrainingProgram.findAllByTrainingPlan(trainingPlan) : []
        def headers = ['Name', 'Category', 'Objective', 'Trainer', 'Certification', 'Classification']
        def withProperties = ['name', 'category', 'objective', 'trainer', 'certification', 'classification']
        new WebXlsxExporter().with {
            setResponseHeaders(response)
            fillHeader(headers)
            add(trainingPrograms, withProperties)
            save(response.outputStream)
        }
    }

    def edit = {
        def res = [:]
        try {
            switch (params.oper) {
                case "add":
                    try {
                        res.message = "TODO"
                    } catch (e) {
                        log.error(e.message, e)
                        res.message = e.message ?: e.cause?.message
                    }
                    break
                case "edit":
                    try {
                        res.description = trainingProgramService.edit(params)
                        res.message = "Edit Success"
                    } catch (e) {
                        log.error(e.message, e)
                        res.message = e.message ?: e.cause?.message
                    }
                    break
                case "del":
                    try {
                        res.description = trainingProgramService.delete(params)
                        res.message = "Delete Success"
                    } catch (e) {
                        log.error(e.message, e)
                        res.message = e.message ?: e.cause?.message
                    }
                    break
            }
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render res as JSON
    }

}
