package com.ucds.core.business

import com.ucds.core.security.ShiroUser
import grails.converters.JSON
import org.apache.shiro.SecurityUtils

class RecruitmentController {

    def appUtilService
    def recruitmentService

    def registration() {}

    def requirements() {}

    def evaluation() {}

    def approval() {
        return appUtilService.loadParams()
    }

    def candidateList() {
        def res = recruitmentService.getCandidateList(params)
        render res as JSON
    }

    def newCandidateList() {
        def res = recruitmentService.getCandidateList(params, PersonCandidate.Status.New)
        render res as JSON
    }

    def approvalList() {
        def res = recruitmentService.getCandidateList(params, PersonCandidate.Status.Evaluated)
        render res as JSON
    }

    def evaluationList() {
        def res = recruitmentService.getEvaluationList(params)
        render res as JSON
    }

    def candidateEdit() {
        def res = [message: null, success: true]
        try {
            params.requester = SecurityUtils.getSubject().getPrincipals().oneByType(String.class)
            switch (params.oper) {
                case "add":
                    recruitmentService.candidateAdd(params)
                    res.message = "Add Success"
                    break
                case "edit":
                    recruitmentService.candidateEdit(params)
                    res.message = "Edit Success"
                    break
                case "del":
                    recruitmentService.candidateDelete(params)
                    res.message = "Delete Success"
                    break
            }
        } catch (e) {
            log.error(e.message, e)
            res.success = false
            res.message = e.message ?: e.cause?.message
        }
        render res as JSON
    }

    def candidateApprove() {
        def res = [message: null, success: true]
        try {
            recruitmentService.candidateApprove(params)
            res.message = g.message(code:"ucds.approval.candidateapproved",default: "Candidate(s) approved")
        } catch (Exception e) {
            log.error(e.message, e)
            res.success = false
            res.message = e.message ?: e.cause?.message
        }
        render res as JSON
    }

    def candidateReject() {
        def res = [message: null, success: true]
        try {
            recruitmentService.candidateReject(params)
            res.message = g.message(code:"ucds.approval.candidaterejected",default: "Candidate(s) rejected")
        } catch (Exception e) {
            log.error(e.message, e)
            res.success = false
            res.message = e.message ?: e.cause?.message
        }
        render res as JSON
    }

    def personPositions() {
        def res = []
        res << [id:null,name:""]
        PersonPosition.all?.each {
            res << [id:it.id,name:it.name]
        }
        render res as JSON
    }

    def getRequiredCompetency() {
        def res = recruitmentService.getRequiredCompetencyList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def addRequiredCompetency() {
        def ret
        String msg
        boolean success = false
        try {
            recruitmentService.addRequiredCompetency(params)
            msg = "Add success"
            success = true
        } catch (e) {
            msg = "Add failed. Reason: " + e.message
        } finally {
            ret = [success: success, message: msg]
        }
        render ret as JSON
    }

    def removeRequiredCompetency() {
        def ret
        String msg
        boolean success = false
        try {
            recruitmentService.removeRequiredCompetency(params)
            msg = "Remove success"
            success = true
        } catch (e) {
            msg = "Remove failed. Reason: " + e.message
        } finally {
            ret = [success: success, message: msg]
        }
        render ret as JSON
    }

    def evaluate() {
        def res = [:]
        try {
            res = recruitmentService.evaluate(params)
            res.success = true
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
            res.success = false
        }
        render res as JSON
    }

    def markAsEvaluated() {
        def res = [:]
        try {
            def approver = SecurityUtils.getSubject().getPrincipals().oneByType(String.class)
            recruitmentService.markAsEvaluated(params.candidateId, approver)
            res.success = true
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
            res.success = false
        }
        render res as JSON
    }

    def analyticalMatrix() {
        def user = SecurityUtils.subject.principals.oneByType(ShiroUser)
        params.filters = params.filters ? JSON.parse(params.filters) : user.filter
        def ret = recruitmentService.getCandidateCompetencyMatrixData(params)
        render ret as JSON
    }
}

