hibernate {
    cache.use_second_level_cache = false
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}

// environment specific settings
environments {
    development {
        dataSource {
            pooled = true
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = org.hibernate.dialect.MySQLDialect
            dbCreate = "create-drop"
            url = "jdbc:mysql://localhost:3306/ucds_dev"
            username = "ucds"
            password = "ucds"
        }
    }
    test {
        dataSource {
            pooled = true
            driverClassName = "org.h2.Driver"
            dialect = "org.hibernate.dialect.H2Dialect"
            username = "sa"
            password = ""
            dbCreate = "create-drop"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
        }
    }
    heroku {
        dataSource {
            pooled = true
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = org.hibernate.dialect.MySQLDialect
            dbCreate = "update"
            uri = new URI(System.env.CLEARDB_DATABASE_URL)
            url = "jdbc:mysql://"+uri.host+uri.path
            username = uri.userInfo.split(":")[0]
            password = uri.userInfo.split(":")[1]
        }
    }
    staging {
        dataSource {
            pooled = true
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = org.hibernate.dialect.MySQLDialect
            dbCreate = "update"
            url = "jdbc:mysql://localhost:3306/ucds_staging"
            username = "ucds"
            password = "ucds"
        }
    }
    production {
        dataSource {
            pooled = true
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = org.hibernate.dialect.MySQLDialect
            dbCreate = "update"
            url = "jdbc:mysql://localhost:3306/ucds"
            username = "ucds"
            password = "ucds"
        }
    }
}
