import com.ucds.core.Menu
import com.ucds.core.SystemParameter
import com.ucds.core.business.*
import com.ucds.core.security.ShiroUser
import grails.converters.JSON
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

    def grailsApplication

    def init = { servletContext ->

        environments {
            // for development expect create-drop mode
            // for others expect update mode (data migration required)
            development {
                if (ShiroUser.count() > 0) return false
                initApp()
                demoData()
            }
            heroku {
                if (ShiroUser.count() > 0) return false
                initApp()
                demoData()
            }
        }
        
        return true
    }

    def destroy = {
    }

    def initApp = {
        
        // user
        def admin = new ShiroUser(username: "admin", passwordHash: new Sha256Hash("ulsoindo2014").toHex())
        admin.addToPermissions("*:*")
        admin.save()

        def hrd = new ShiroUser(username: "hrd", passwordHash: new Sha256Hash("ulsoid123").toHex())
        hrd.addToPermissions("main:*")
        hrd.addToPermissions("competency:getCompetencyLevels")
        hrd.addToPermissions("personCompetency:getEvaluationList")
        hrd.addToPermissions("personCompetency:edit")
        hrd.addToPermissions("person:getEvaluationList")
        hrd.addToPermissions("person:evaluationExpected")
        hrd.save()

        def spv = new ShiroUser(username: "spv", passwordHash: new Sha256Hash("ulsoid123").toHex())
        spv.addToPermissions("main:*")
        spv.addToPermissions("competency:getCompetencyLevels")
        spv.addToPermissions("personCompetency:getEvaluationList")
        spv.addToPermissions("personCompetency:edit")
        spv.addToPermissions("person:getEvaluationList")
        spv.addToPermissions("person:evaluationActual")
        spv.filter["personPosition.name"] = "Implementer"
        spv.save()

        // for demo
        def allExceptUserManagement = ["main:*", "competency:*", "competencyLevel:*", "keySuccessFactor:*", "kpi:*", "kpiCategory:*", "personCompetency:*", "person:*", "report:*", "trainingPlan:*", "trainingProgram:*"]
        def user = new ShiroUser(username: "user123", passwordHash: new Sha256Hash("password").toHex())
        for (String permission : allExceptUserManagement) {
            user.addToPermissions(permission)
        }
        user.save()

        // menus
        def count = 0
        def defineGroup = new Menu(menuCode: "DEFINE", label: "Define", seq: 0)
        new Menu(menuCode: "COMPETENCY_CORE", label: "Core Competency", linkController: "competency", linkAction: "core", url: "#", parent: defineGroup, seq: count++).save()
        new Menu(menuCode: "COMPETENCY_MANAGERIAL", label: "Managerial Competency", linkController: "competency", linkAction: "managerial", url: "#", parent: defineGroup, seq: count++).save()
        new Menu(menuCode: "COMPETENCY_TECHNICAL", label: "Technical Competency", linkController: "competency", linkAction: "technical", url: "#", parent: defineGroup, seq: count++).save()
        new Menu(menuCode: "COMPETENCY_REQUIRED", label: "Required Competency", linkController: "person", linkAction: "requiredCompetency", url: "#", parent: defineGroup, seq: count++).save()
        new Menu(menuCode: "KEY_SUCCESS_FACTOR", label: "Key Success Factor Personal", linkController: "person", linkAction: "keySuccessFactor", url: "#", parent: defineGroup, seq: count++).save()
        new Menu(menuCode: "STRATEGY_OBJECTIVE", label: "Strategy Objective", linkController: "strategyObjective", linkAction: "index", url: "#", parent: defineGroup, seq: count++).save()
        defineGroup.save(failOnError: true)

        count = 0
        def measureGroup = new Menu(menuCode: "MEASURE", label: "Measure", seq: 1)
        new Menu(menuCode: "COMPETENCY_EVALUATION", label: "Competency Evaluation", linkController: "person", linkAction: "evaluation", url: "#", parent: measureGroup, seq: count++).save()
        new Menu(menuCode: "COMPETENCY_EVALUATION_ACTUAL", label: "Competency Evaluation Actual", linkController: "person", linkAction: "evaluationActual", url: "#", parent: measureGroup, seq: count++).save()
        new Menu(menuCode: "COMPETENCY_EVALUATION_EXPECTED", label: "Competency Evaluation Expected", linkController: "person", linkAction: "evaluationExpected", url: "#", parent: measureGroup, seq: count++).save()
        new Menu(menuCode: "KPI_EVALUATION", label: "KPI Evaluation", linkController: "person", linkAction: "kpi", url: "#", parent: measureGroup, seq: count++).save()
        new Menu(menuCode: "WORKLOAD_ANALYSIS", label: "Workload Analysis", linkController: "workload", linkAction: "index", url: "#", parent: measureGroup, seq: count++).save()
        measureGroup.save(failOnError: true)

        count = 0
        def analyzeGroup = new Menu(menuCode: "ANALYZW", label: "Analyze", seq: 2)
        new Menu(menuCode: "COMPETENCY_MATRIX", label: "Competency Matrix", linkController: "report", linkAction: "competencyMatrix", url: "#", parent: analyzeGroup, seq: count++).save()
        new Menu(menuCode: "ANALYTICAL_MATRIX", label: "Analytical Matrix", linkController: "report", linkAction: "analyticalMatrix", url: "#", parent: analyzeGroup, seq: count++).save()
        analyzeGroup.save(failOnError: true)

        count = 0
        def improveGroup = new Menu(menuCode: "IMPROVE", label: "Improve", seq: 3)
        new Menu(menuCode: "TRAINING_PROGRAMS", label: "Training Programs", linkController: "trainingPlan", linkAction: "trainingPrograms", url: "#", parent: improveGroup, seq: count++).save()
        improveGroup.save(failOnError: true)

        count = 0
        def controlGroup = new Menu(menuCode: "CONTROL", label: "Control", seq: 4)
        new Menu(menuCode: "STRATEGY_DIAGRAM", label: "Strategy Diagram", linkController: "report", linkAction: "strategy", url: "#", parent: controlGroup, seq: count++).save()
        controlGroup.save(failOnError: true)

        count = 0
        def recruitmentGroup = new Menu(menuCode: "RECRUITMENT", label: "Recruitment", seq: 5)
        new Menu(menuCode: "RECRUITMENT_REGISTRATION", label: "Registration", linkController: "recruitment", linkAction: "registration", url: "#", parent: recruitmentGroup, seq: count++).save()
        new Menu(menuCode: "RECRUITMENT_REQUIREMENTS", label: "Competency Requirement", linkController: "recruitment", linkAction: "requirements", url: "#", parent: recruitmentGroup, seq: count++).save()
        new Menu(menuCode: "RECRUITMENT_EVALUATION", label: "Competency Evaluation", linkController: "recruitment", linkAction: "evaluation", url: "#", parent: recruitmentGroup, seq: count++).save()
        new Menu(menuCode: "RECRUITMENT_APPROVAL", label: "Approval", linkController: "recruitment", linkAction: "approval", url: "#", parent: recruitmentGroup, seq: count++).save()
        recruitmentGroup.save(failOnError: true)

        count = 0
        def administrationMenuGroup = new Menu(menuCode: "ADMINISTRATION", label: "Administration", seq: 99)
        new Menu(menuCode: "USER_MANAGEMENT", label: "User Management", linkController: "shiroUser", linkAction: "index", url: "#", parent: administrationMenuGroup, seq: count++).save()
        new Menu(menuCode: "MENU_MANAGEMENT", label: "Menu Management", linkController: "menu", linkAction: "index", url: "#", parent: administrationMenuGroup, seq: count++).save()
        new Menu(menuCode: "SYSTEM_PARAMETER", label: "System Parameter", linkController: "systemParameter", linkAction: "index", url: "#", parent: administrationMenuGroup, seq: count++).save()
        new Menu(menuCode: "PEOPLE", label: "People", linkController: "person", linkAction: "manage", url: "#", parent: administrationMenuGroup, seq: count++).save()
        new Menu(menuCode: "POSITION", label: "Position", linkController: "person", linkAction: "position", url: "#", parent: administrationMenuGroup, seq: count++).save()
        new Menu(menuCode: "TRAINING_METHOD", label: "Training Method", linkController: "trainingMethod", linkAction: "index", url: "#", parent: administrationMenuGroup, seq: count++).save()
        new Menu(menuCode: "TRAINING_METHOD_QUESTION", label: "Training Method Question", linkController: "trainingMethodQuestion", linkAction: "index", url: "#", parent: administrationMenuGroup, seq: count++).save()
        new Menu(menuCode: "BALANCE_SCORE_CARD", label: "Balance Score Card", linkController: "balanceScoreCard", linkAction: "index", url: "#", parent: administrationMenuGroup, seq: count++).save()
        new Menu(menuCode: "KSI_CATEGORY", label: "Key Success Indicator Category", linkController: "keySuccessIndicatorCategory", linkAction: "index", url: "#", parent: administrationMenuGroup, seq: count++).save()
        new Menu(menuCode: "HOLIDAY", label: "Working Day & Holiday", linkController: "holiday", linkAction: "index", url: "#", parent: administrationMenuGroup, seq: count++).save()
        new Menu(menuCode: "WORK_CONTRACT", label: "Work Contract", linkController: "workContractType", linkAction: "index", url: "#", parent: administrationMenuGroup, seq: count++).save()
        administrationMenuGroup.save(failOnError: true)

        // workcontracttype
        new WorkContractType(name: "Default", description: "5 working days and 12 annual leaves", averageDailyWorkHours: 8f, annualLeaveDays: 12f).save(failOnError: true)

    }

    def demoData = {
        // training method
        def lecture = new TrainingMethod(name: "Lecture", description: "Useful for competency knowledge transfer").save(flush: true)
        def jobShadowing = new TrainingMethod(name: "Job Shadowing", description: "Helps to absorb knowledge, skill, mindset and behavior of senior staff").save(flush: true)

        // training method question
        new TrainingMethodQuestion(value: "Apakah kompetensi termasuk dalam ranah Afektif?", trainingMethods: [lecture]).save()
        new TrainingMethodQuestion(value: "Apakah kompetensi termasuk dalam ranah Kognitif?", trainingMethods: [jobShadowing]).save()
        new TrainingMethodQuestion(value: "Apakah kompetensi termasuk dalam ranah Psikomotorik?", trainingMethods: [lecture]).save()
        new TrainingMethodQuestion(value: "Apakah kompetensi termasuk dalam ranah Psikomotorik & Kognitif?", trainingMethods: [lecture, jobShadowing]).save()
        new TrainingMethodQuestion(value: "Apakah kompetensi termasuk dalam ranah Afektif & Kognitif?", trainingMethods: [lecture, jobShadowing]).save()

        // core competency
        def competency = new Competency(name: "Job Quality", type: Competency.Types.Core).save(flush: true)
        competency.addToCompetencyLevels(new CompetencyLevel(level: 1,
                name: "Mengetahui standar kerja yang berhubungan dengan deskripsi pekerjaan",
                competency: competency,
                keyIndicator: "Mengidentifikasikan standar kerja yang diharapkan yang berhubungan dengan deskripsi pekerjaan.\n" +
                        "Mengetahui tahapan-tahapan proses kerja yang sesuai dengan standar kualitas",
                trainingMethods: [jobShadowing]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 2,
                name: "Mampu mengkomunikasikan standar kualitas kerja kepada orang lain",
                competency: competency,
                keyIndicator: "Menjelaskan standar kualitas kerja kepada orang lain \n" +
                        "Memberi contoh aplikasi standar kualitas kerja kepada orang lain\n" +
                        "Mendemonstrasikan kemapuan komunikasi mengenai standar kerja kepada orang lain",
                trainingMethods: [jobShadowing]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 3,
                name: "Mampu mengajarkan standar kualitas kerja kepada orang lain",
                competency: competency,
                keyIndicator: "Mendemonstrasikan  standar kualitas kerja kepada orang lain.\n" +
                        "Menunjukan proses-proses  kerja yang sesuai dengan standar kualitas.\n" +
                        "Mempraktekan kemampuan mengajar standar kualitas kerja",
                trainingMethods: [jobShadowing]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 4,
                name: "Proaktif terhadap tuntutan perubahan bisnis kaitannya dengan pengembangan SDM bawahan",
                competency: competency,
                keyIndicator: "Menganalisa perubahan yang terjadi dan menyeuaikanya dengan kondisi yang ada.\n" +
                        "Merancang  strategi pengembangan bawahan kaitanya dengan perubahan bisnis yang terjadi.\n" +
                        "Memperbaiki kualitas SDM sesuai dengan perubahan yang terjadi ",
                trainingMethods: [jobShadowing]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 5,
                name: "Mampu membuat kerangka ide yang dapat digunakan untuk mengadapi tuntutan perubahan bisnis dan mengaplikasikannya bersama karyawan lainnya",
                competency: competency,
                keyIndicator: "Menyimpulkan sebuah ide yang berasal dari alternatif solusi hasil analisa untuk mengadapi tuntutan perubahan bisnis dan mengaplikasikannya bersama karyawan lainnya.\n" +
                        "Merancang straetgi pelatihan dan pengembangan sdm",
                trainingMethods: [jobShadowing]).save())
        competency.save(flush: true)

        competency = new Competency(name: "Understanding", type: Competency.Types.Core).save(flush: true)
        competency.addToCompetencyLevels(new CompetencyLevel(level: 1,
                name: "Mampu berperan sebagai bagian dari masyarakat",
                competency: competency,
                keyIndicator: "Memberi perhatian dan hormat kepada masyarakat sekitar \n" +
                        "Meperhatikan  kesejahteraan masyarakat sekitar\n" +
                        "Mengidentifikasi hal-hal positif yang dapt diberikan oleh masyarakat demi berlangsungnya kelancaran operasional bisnis",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 2,
                name: "Menjaga komunikasi dan hubungan yang baik dengan masyarakat sekitar",
                competency: competency,
                keyIndicator: "Memperaktekan keramahan terhadap masyarakat sekitar pada kehidupan sehari-hari \n" +
                        "Membantu kegiatan yang berhubungan dengan peningkatan kesejahteraan masyarakat sekitar \n" +
                        "Menyesuaikan kondisi pekerjaan dengan kebutuhan masyarakat sekitar\n" +
                        "Mentaati norma, etika, dan nilai-nilai budaya masyarakat sekitar",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 3,
                name: "Mengidentifikasikan  hal-hal yang diperlukan guna mencapai kesejahteraan masyarakat sekitar dan memotivasi seluruh karyawan untuk peduli terhadap hal-hal tersebut",
                competency: competency,
                keyIndicator: "Mengidentifikasikan potensi masyarakat yang dapat dimanfaatkan dalam mencapai tujuan perusahaan \n" +
                        "Menunjukan hal-hal positif yang dapat masyarakat kontribusikan bagi perusahaan\n" +
                        "Menganalisa potensi perusahan untuk meningkatkan kesejahteraan masyarakat sekitar. \n" +
                        "Berinisiatif untuk memotivasi karyawan lainnya/anak buah untuk peduli  dan membantu meningkatkan kesejahteraan masyarakat sekitar \n" +
                        "Mengidentifikasikan pengaruh masyarakat yang dapat memeberikan pengaruh positif bagi perusahaan \n" +
                        "Meminta karyawan lainnya untuk peduli kepada masyarakat sekitar",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 4,
                name: "Menetapkan  kredibilias perusahaan sebagai pihak yang peduli terhadap masyarakat sekitar dan mempertahankan kepercayaan masyarakat terhadap perusahaan",
                competency: competency,
                keyIndicator: "Mengkontraskan kekuatan dan peluang yang ada pada masyarakat sekitar yang dapat memberikan keuntungan bagi perusahaan\n" +
                        "Mengorganisir potensi masyarakat sekitar demi tercapainya tujuan perusahaan.\n" +
                        "Mengitegrasikan unit kerja yang ada dengan kondisi masyarakat yang ada sehingga dapat ditemukan  hubungan yang saling menguntungkan diantara kedua belah pihak.\n" +
                        "Mengkombinasikan antara kebutuhan perusahaan dan kebutuhan masyarakat sehingga kedua belah pihak dapat saling memberikan kontribusi satu sama lain yang menguntungkan kedua belah pihak.\n" +
                        "Mampu menganalisa kebutuhan masyarakat dan mengkaitkannya dengan peluang kesempatan yang dapat diberikan perusahaan kepada mereka.\n" +
                        "Merancang strategi perusahaan yang dapat melibatkan masyarakat dalam mencapai tujuan",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 5,
                name: "Mendapatkan kepercayaan dari masyarakat sekitar",
                competency: competency,
                keyIndicator: "Menyesuaikan kondisi perusahaan dengan kondisi masyarakat sekitar\n" +
                        "Menunjukan komitmen terhadap pemberdayaan masyarakat sekitar\n" +
                        "Mengkaji ulang kefektifitasan masyarakat dalam perannya mencapai tujuan perusahaan dan melakukan tindakan perbaikan ketika ditemukan hambatan\n" +
                        "Merevisi program pemberdayaan masyarakat yang merugikan masyarakat\n" +
                        "Mempertahankan sistem/aturan/strategi/norma yang dapat menjadikan masyarakat sekitar menjadi bagian dari perusahaan\n" +
                        "Membuktikan bahwa perusahaan senantiasa peduli kepada masyaratak melalui rencana/program perusahaan yang mengikutsertakan masyarakat sebagai bagian dari perusahaan",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.save(flush: true)

        competency = new Competency(name: "Agent of Change", type: Competency.Types.Core).save(flush: true)
        competency.addToCompetencyLevels(new CompetencyLevel(level: 1,
                name: "Mampu merespon ketidaksesuaian proses kerja yang tidak sesuai dengan perubahan tuntutan bisnis",
                competency: competency,
                keyIndicator: "Berusaha mengetahui tahapan dan kualitas dari proses kerja yang seharusnya\n" +
                        "Mengidentifikasikan langkah perbaikan yang harus dilakukan ketika menemukan ketidaksesuaian proses kerja",
                trainingMethods: [jobShadowing]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 2,
                name: "Mampu memudahkan proses perubahan",
                competency: competency,
                keyIndicator: "Berusaha menganalogikan faktor-faktor yang dapat membantu perubahan\n" +
                        "Menjelaskan cara-cara melakukan perubahan ke orang lain",
                trainingMethods: [jobShadowing]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 3,
                name: "Mengidentifikasikan perubahan yang diperlukan untuk strategi bisnis",
                competency: competency,
                keyIndicator: "Menemukan cara untuk berubah sesuai dengan standarisasi tuntutan bisnis \n" +
                        "Menerapkan cara mengaplikasikan perubahan sesuai standarisasi dari tuntutan bisnis",
                trainingMethods: [jobShadowing]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 4,
                name: "Mampu mengidentifikasikan masalah utama yang dapat menjadi penghalang keberhasilan bisnis",
                competency: competency,
                keyIndicator: "Mampu mencari pemecahan  masalah dan menyederhanakannya menjadi beberapa bagian\n" +
                        "Meganalisa langkah-langkah perbaikan berkelajutan yang diperlukan dalam menghadapi perubahan yang terjadi",
                trainingMethods: [jobShadowing]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 5,
                name: "Memberikan pandangan alternatif solusi mengenai masalah bisnis",
                competency: competency,
                keyIndicator: "Mengkaji ulang perubahan yang terjadi dan menemukan solusi alternatif pemecahan masalah\n" +
                        "Memperhitungkan konsukuensi dari pengaplikasian solusi pemecahan masalah yang dipilih",
                trainingMethods: [jobShadowing]).save())
        competency.save(flush: true)

        competency = new Competency(name: "Reliability", type: Competency.Types.Core).save(flush: true)
        competency.addToCompetencyLevels(new CompetencyLevel(level: 1,
                name: "Mengetahui proses bisnis sesuai dengan sistem manajemen internasional",
                competency: competency,
                keyIndicator: "Mengetahui standar kualitas pelayanan/produk\n" +
                        "Mendefinisikan harapan pelanggan \n" +
                        "Mengidentifikasikan standar kualitas layanan ",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 2,
                name: "Mampu mengkomunikasikan standar produk/layanan yang seharusnya kepada orang lain",
                competency: competency,
                keyIndicator: "Menjelaskan standar  kualitas produk kepada anak buah/orang lain\n" +
                        "Memperlihatkan kemampuan komunikasi yang baik saat menjelaskan standar kualitas\n" +
                        "Memberikan contoh layanan sesuai standarisasi yang berlaku\n" +
                        "Menanggapi orang yang tidak mendukung standar dengan penjelasan yang sesuai standar  \n" +
                        "Menginterpretasikan standar kualitas yang diharapkan oleh pasar\n" +
                        "Bereaksi untuk melakukan perbaikan saat melihat ada layanan/produk yang tidak sesuai dengan standar yang telah ditentukan ",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 3,
                name: "Mampu mempengaruhi orang lain untuk menghasilkan layanan/produk sesuai standar kalitas internasional",
                competency: competency,
                keyIndicator: "Membuktikan kepada orang lain bahwa standar kulitas yang telah ditentukan dapat memebrikan keuntungan bagi perusahaan \n" +
                        "Mampu menggerakan orang lain memperbaiki kesalahan atau ketidaksesuaian yang terjadi\n" +
                        "Membuktikan kepada orang lain bahwa standar kulitas proses layanan yang telah ditentukan dapat memberikan keuntungan bagi perusahaan\n" +
                        "Mengendalikan proses layanan yang ada sesuai dengan standar yang telah ditentukan \n" +
                        "Menyesuaikan konsep stadar kualitas yang teleh ditentukan dengan konsep yang dimiliki oleh orang lain/anak buah\n" +
                        "Mempertajam kemampuan anak buah/orang lain dalam menjalankan proses layanan/produk yang sesuia dengan harapan pasar",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 4,
                name: "Proaktif menggerakan orang lain untuk menghasilkan layanan/produk sesuai standar kualitas internasional",
                competency: competency,
                keyIndicator: "Membedakan orang yang tidak bekerja sesuai dengan kualitas yang telah ditentukan\n" +
                        "Memperbaiki ketidaksesuaian kualitas yang ada\n" +
                        "Menghubungkan ketidaksesuain standar kualitas layanan dengan pedoman standar kualitas yang ada\n" +
                        "Memastikan anak buah melaksanakan sesuai standar pelayanan\n" +
                        "Merancang sebuah sistem yang dapat menghasilkan kualitas sesuai dengan harapan pasar\n" +
                        "Megukur kemampuan untuk menyesuakan produk/layanan sesuai dengan harapan pasar",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 5,
                name: "Memiliki kredibilitas sebagai orang yang senantiasa selalu mempertahankan layanan/produk yang sesuai kualitas internasional",
                competency: competency,
                keyIndicator: "Mengkaji ulang seluruh layanan/produk sesuai dengan Standar kualitas Internasional \n" +
                        "Menjadi pionir dalam perubahan kualitas/layanan sesuai standar internasional\n" +
                        "Memperlihatkan integritas terhadap standar kualitas internasional \n" +
                        "Mempertahankan kualitas sesuai dengan standar kualitas yang ada\n" +
                        "Menunjukan komitmen yang  tinnggi terhadap harapan klien/pelanggan \n" +
                        "Mengevaluasi layanan/produk yang ada dengan tujuan mempertahankan kredibilitas yang ada",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.save(flush: true)

        competency = new Competency(name: "Aware of Process", type: Competency.Types.Core).save(flush: true)
        competency.addToCompetencyLevels(new CompetencyLevel(level: 1,
                name: "Mengetahui proses bisnis sesuai dengan sistem manajemen internasional",
                competency: competency,
                keyIndicator: "Mengetahui standar kualitas pelayanan/produk\n" +
                        "Mendefinisikan harapan pelanggan \n" +
                        "Mengidentifikasikan standar kualitas layanan ",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 2,
                name: "Merancang program yang memudahkan orang lain memahami standar sistem manajemen berkualitas internasional",
                competency: competency,
                keyIndicator: "Menjelaskan standar  kualitas produk kepada anak buah/orang lain\n" +
                        "Memperlihatkan kemampuan komunikasi yang baik saat menjelaskan standar kualitas\n" +
                        "Memberikan contoh layanan sesuai standarisasi yang berlaku\n" +
                        "Menanggapi orang yang tidak mendukung standar dengan penjelasan yang sesuai standar  \n" +
                        "Menginterpretasikan standar kualitas yang diharapkan oleh pasar\n" +
                        "Bereaksi untuk melakukan perbaikan saat melihat ada layanan/produk yang tidak sesuai dengan standar yang telah ditentukan ",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 3,
                name: "Proaktif terhadap tuntutan perubahan bisnis kaitannya dengan pengambangan SDM bawahan",
                competency: competency,
                keyIndicator: "Membuktikan kepada orang lain bahwa standar kulitas yang telah ditentukan dapat memebrikan keuntungan bagi perusahaan \n" +
                        "Mampu menggerakan orang lain memperbaiki kesalahan atau ketidaksesuaian yang terjadi\n" +
                        "Membuktikan kepada orang lain bahwa standar kulitas proses layanan yang telah ditentukan dapat memberikan keuntungan bagi perusahaan\n" +
                        "Mengendalikan proses layanan yang ada sesuai dengan standar yang telah ditentukan \n" +
                        "Menyesuaikan konsep stadar kualitas yang teleh ditentukan dengan konsep yang dimiliki oleh orang lain/anak buah\n" +
                        "Mempertajam kemampuan anak buah/orang lain dalam menjalankan proses layanan/produk yang sesuia dengan harapan pasar",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 4,
                name: "Mempertahankan kredibilitas perusahaann dengan menjaga kualitas sistem manajemen berkualitas internasional",
                competency: competency,
                keyIndicator: "Menganalisa kendala yang tidak sesuai dengan performance proses bisnis yang diharapkan \n" +
                        "Menunjukan sikap peduli terhadap kredibilitas perusahaan di mata pasar\n" +
                        "Mendesain change management untuk beradaptasi dengan perubahan yang terjadi\n" +
                        "Berinisiatif menjaga kredibilitas perusahaan denan senantiasa memperthankan keunggulan\n" +
                        "Mempengaruhi orang lain untuk senantiasa menjaga disiplin kerja",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.addToCompetencyLevels(new CompetencyLevel(level: 5,
                name: "Memiliki kredibilitas sebagai orang yang senantiasa selalu mempertahankan layanan/produk yang sesuai kualitas internasional",
                competency: competency,
                keyIndicator: "Mengkaji ulang seluruh layanan/produk sesuai dengan Standar kualitas Internasional \n" +
                        "Menjadi pionir dalam perubahan kualitas/layanan sesuai standar internasional\n" +
                        "Memperlihatkan integritas terhadap standar kualitas internasional \n" +
                        "Mempertahankan kualitas sesuai dengan standar kualitas yang ada\n" +
                        "Menunjukan komitmen yang  tinnggi terhadap harapan klien/pelanggan \n" +
                        "Mengevaluasi layanan/produk yang ada dengan tujuan mempertahankan kredibilitas yang ada",
                trainingMethods: [jobShadowing, lecture]).save())
        competency.save(flush: true)

        // managerial competency

        competency = new Competency(name: "Planning & Organizing", type: Competency.Types.Managerial).save(flush: true)
        competency.addToCompetencyLevels(new CompetencyLevel(level: 1, name: "Level 1", competency: competency, keyIndicator: "Indicator 1", trainingMethods: [jobShadowing]).save(flush: true))
        competency.addToCompetencyLevels(new CompetencyLevel(level: 2, name: "Level 2", competency: competency, keyIndicator: "Indicator 2", trainingMethods: [jobShadowing]).save(flush: true))
        competency.addToCompetencyLevels(new CompetencyLevel(level: 3, name: "Level 3", competency: competency, keyIndicator: "Indicator 3", trainingMethods: [jobShadowing]).save(flush: true))
        competency.save(flush: true)

        // technical competency

        def businessProcess = new Competency(name: "Business Process", type: Competency.Types.Technical).save(flush: true)
        businessProcess.addToCompetencyLevels(new CompetencyLevel(level: 1, name: "Level 1", competency: businessProcess, keyIndicator: "Indicator 1", trainingMethods: [jobShadowing, lecture]).save(flush: true))
        businessProcess.addToCompetencyLevels(new CompetencyLevel(level: 2, name: "Level 2", competency: businessProcess, keyIndicator: "Indicator 2", trainingMethods: [jobShadowing, lecture]).save(flush: true))
        businessProcess.addToCompetencyLevels(new CompetencyLevel(level: 3, name: "Level 3", competency: businessProcess, keyIndicator: "Indicator 3", trainingMethods: [jobShadowing, lecture]).save(flush: true))
        businessProcess.addToCompetencyLevels(new CompetencyLevel(level: 4, name: "Level 4", competency: businessProcess, keyIndicator: "Indicator 4", trainingMethods: [jobShadowing, lecture]).save(flush: true))
        businessProcess.save(flush: true)

        def networking = new Competency(name: "Networking", type: Competency.Types.Technical).save(flush: true)
        networking.addToCompetencyLevels(new CompetencyLevel(level: 1, name: "Level 1", competency: networking, keyIndicator: "Indicator 1", trainingMethods: [lecture]).save(flush: true))
        networking.addToCompetencyLevels(new CompetencyLevel(level: 2, name: "Level 2", competency: networking, keyIndicator: "Indicator 2", trainingMethods: [lecture]).save(flush: true))
        networking.addToCompetencyLevels(new CompetencyLevel(level: 3, name: "Level 3", competency: networking, keyIndicator: "Indicator 3", trainingMethods: [lecture]).save(flush: true))
        networking.addToCompetencyLevels(new CompetencyLevel(level: 4, name: "Level 4", competency: networking, keyIndicator: "Indicator 4", trainingMethods: [lecture]).save(flush: true))
        networking.save(flush: true)

        def installation = new Competency(name: "Computer Installation", type: Competency.Types.Technical).save(flush: true)
        installation.addToCompetencyLevels(new CompetencyLevel(level: 1, name: "Level 1", competency: installation, keyIndicator: "Indicator 1", trainingMethods: [jobShadowing, lecture]).save(flush: true))
        installation.addToCompetencyLevels(new CompetencyLevel(level: 2, name: "Level 2", competency: installation, keyIndicator: "Indicator 2", trainingMethods: [jobShadowing, lecture]).save(flush: true))
        installation.addToCompetencyLevels(new CompetencyLevel(level: 3, name: "Level 3", competency: installation, keyIndicator: "Indicator 3", trainingMethods: [jobShadowing, lecture]).save(flush: true))
        installation.addToCompetencyLevels(new CompetencyLevel(level: 4, name: "Level 4", competency: installation, keyIndicator: "Indicator 4", trainingMethods: [jobShadowing, lecture]).save(flush: true))
        installation.save(flush: true)

        // position
        def cfo = new PersonPosition(name: "CFO").save(flush: true)
        def financeManager = new PersonPosition(name: "Finance Manager").save(flush: true)
        def implementer = new PersonPosition(name: "Implementer").save(flush: true)
        def programmer = new PersonPosition(name: "Programmer").save(flush: true)

        // people
        def financeSection = "Finance"
        def ITSection = "IT"
        new Person(firstName: "Evangelis", lastName: "T", employeeNumber: "000006", position: "CFO", level: "Staff", gender: Person.Genders.Female, dateOfBirth: Date.parse("dd/MM/yyyy", "01/01/1970"), personPosition: cfo, section: financeSection).save(flush: true)
        new Person(firstName: "Muhamad", lastName: "Yasin", employeeNumber: "000007", position: "Implementer", level: "Staff", gender: Person.Genders.Male, dateOfBirth: Date.parse("dd/MM/yyyy", "01/01/1970"), personPosition: implementer, section: financeSection).save(flush: true)
        new Person(firstName: "M", lastName: "Irmansyah", position: "Implementer", level: "Staff", gender: Person.Genders.Male, dateOfBirth: Date.parse("dd/MM/yyyy", "01/01/1970"), personPosition: implementer, section: financeSection).save(flush: true)
        new Person(firstName: "Julbintor", employeeNumber: "000010", position: "Implementer", level: "Staff", gender: Person.Genders.Male, dateOfBirth: Date.parse("dd/MM/yyyy", "01/01/1970"), personPosition: implementer, section: ITSection).save(flush: true)
        new Person(firstName: "Christoper", employeeNumber: "000010", position: "Implementer", level: "Staff", gender: Person.Genders.Male, dateOfBirth: Date.parse("dd/MM/yyyy", "01/01/1970"), personPosition: implementer, section: ITSection).save(flush: true)

        // add technical competency to implementers
        def implementers = Person.findAllByPersonPosition(implementer)
        for (Person person : implementers) {
            person.addToRequiredCompetencies(businessProcess)
            person.addToRequiredCompetencies(networking)
            person.addToRequiredCompetencies(installation)
            person.save(flush: true)
        }

        // evaluate
        def cal = Calendar.instance
        cal.set(Calendar.DATE, 1)
        def date = Date.parse(grailsApplication.config.ucds.format.date, cal.format(grailsApplication.config.ucds.format.date))
        def people = Person.all
        for (int j = 0; j < people.size(); j++) {
            ArrayList<Competency> requiredCompetencies = []
            def coreAndManagerialCompetencies = Competency.findAllByTypeInList([Competency.Types.Core, Competency.Types.Managerial])
            if (coreAndManagerialCompetencies) requiredCompetencies.addAll(coreAndManagerialCompetencies)
            def tecCompetencies = people.get(j).requiredCompetencies
            if (tecCompetencies) requiredCompetencies.addAll(tecCompetencies)
            for (int i = 0; i < requiredCompetencies.size(); i++) {
                int expected = requiredCompetencies.get(i).competencyLevels.size()
                int actual = expected - j % expected
                int gap = actual - expected
                new PersonCompetency(person: people.get(j), competency: requiredCompetencies.get(i), expectedScore: expected, actualScore: actual, gap: gap, period: date).save(flush: true)
            }
        }

        // training programs
        Date startDate = date;
        Date endDate = date;
        def trainerOrPIC = "TBD"
        def trainingPlan = new TrainingPlan(period: date, createdBy: 'admin', dateCreated: new Date(), costLimit: 1000000, timeLimit: 30).save(failOnError: true)
        PersonCompetency.findAllByPeriod(date).each { personCompetency ->
            def actualScore = personCompetency.actualScore
            def expectedScore = personCompetency.expectedScore
            personCompetency.competency.competencyLevels.each {
                if (it.level > actualScore && it.level <= expectedScore) {
                    it.trainingMethods.eachWithIndex { method, i ->
                        def cost = 100000 + i * 10000
                        startDate = endDate.next()
                        endDate = startDate.next()
                        def trainingProgram = new TrainingProgram(personCompetency: personCompetency, trainingMethod: method, competencyLevel: it.level, startDate: startDate, endDate: endDate, cost: cost, trainerOrPIC: trainerOrPIC).save()
                        personCompetency.addToTrainingPrograms(trainingProgram)
                        trainingPlan.addToTrainingPrograms(trainingProgram)
                    }
                }
            }
            personCompetency.save(failOnError: true)
            trainingPlan.save(failOnError: true)
        }

        // balance score cards
        def financial = new BalanceScoreCard(name: "Financial").save(flush: true)
        def environment = new BalanceScoreCard(name: "Environment").save(flush: true)
        def customerSatisfaction = new BalanceScoreCard(name: "Customer Satisfaction").save(flush: true)
        def employee = new BalanceScoreCard(name: "Employee").save(flush: true)
        def eduucationAndGrowth = new BalanceScoreCard(name: "Education and Growth").save(flush: true)
        def internalProcess = new BalanceScoreCard(name: "Internal Process").save(flush: true)
        def supplier = new BalanceScoreCard(name: "Supplier").save(flush: true)

        // kpi category
        new KeySuccessIndicatorCategory(name: "Satisfaction").save(flush: true)
        new KeySuccessIndicatorCategory(name: "Strategy").save(flush: true)
        new KeySuccessIndicatorCategory(name: "Process").save(flush: true)
        new KeySuccessIndicatorCategory(name: "Contribution").save(flush: true)
        new KeySuccessIndicatorCategory(name: "Capabilities").save(flush: true)

        // personal key success factor
        def balanceScoreCard = [:]
        for (BalanceScoreCard card : BalanceScoreCard.all) {
            balanceScoreCard[card.id.toString()] = "Yes"
        }

        for (Person person : people) {
            def keySuccessIndicators = [:]
            // 1
            for (KeySuccessIndicatorCategory cat : KeySuccessIndicatorCategory.all) {
                keySuccessIndicators[cat.id.toString()] = "Key Success Factor A" + " " + cat.name
            }
            person.addToKeySuccessFactors(new KeySuccessFactor(title: "Key Success Factor A", kri: "Key Success Factor A 80%", balanceScoreCard: (balanceScoreCard as JSON).toString(), keySuccessIndicatorCategory: (keySuccessIndicators as JSON).toString(), selectedKpi: ([:] as JSON).toString(), percentage: 100))
            // 2
            for (KeySuccessIndicatorCategory cat : KeySuccessIndicatorCategory.all) {
                keySuccessIndicators[cat.id] = "Key Success Factor B" + " " + cat.name
            }
            person.addToKeySuccessFactors(new KeySuccessFactor(title: "Key Success Factor B", kri: "Key Success Factor B 20%", balanceScoreCard: (balanceScoreCard as JSON).toString(), keySuccessIndicatorCategory: (keySuccessIndicators as JSON).toString(), selectedKpi: ([:] as JSON).toString(), percentage: 100))
            person.save()
        }

        // strategy objective
        def shareholderValueAdded = new StrategyObjective(name: "Shareholder Value Added", perspective: financial).save()
        def revenueGrowth = new StrategyObjective(name: "Revenue Growth", perspective: financial).save()
        def costReduction = new StrategyObjective(name: "Cost Reduction/Efficiency", perspective: financial).save()

        shareholderValueAdded.addToDependencies(new StrategyObjectiveDependency(strategyObjective: revenueGrowth, affectee: shareholderValueAdded, weight: 0.6))
        shareholderValueAdded.addToDependencies(new StrategyObjectiveDependency(strategyObjective: costReduction, affectee: shareholderValueAdded, weight: 0.4))
        shareholderValueAdded.save()

        def monitoringRevenueReport = new KeySuccessFactor(title: "Monitoring revenue generating process").save()
        monitoringRevenueReport.addToResponsiblePositions(cfo)
        monitoringRevenueReport.save()
        revenueGrowth.addToKeySuccessFactors(monitoringRevenueReport)

        def monitoringDailyRevenueReport = new KeySuccessFactor(title: "Monitoring daily revenue generating process", parent: monitoringRevenueReport).save()
        monitoringDailyRevenueReport.addToResponsiblePositions(implementer)
        monitoringDailyRevenueReport.save()
        revenueGrowth.addToKeySuccessFactors(monitoringDailyRevenueReport)
        revenueGrowth.save()

        def costReductionMonthly = new KeySuccessFactor(title: "Cost reduction monthly").save()
        costReductionMonthly.addToResponsiblePositions(cfo)
        costReductionMonthly.save()
        costReduction.addToKeySuccessFactors(costReductionMonthly)

        def costReductionDaily = new KeySuccessFactor(title: "Cost reduction daily", parent: costReductionMonthly).save()
        costReductionDaily.addToResponsiblePositions(implementer)
        costReductionDaily.save()
        costReduction.addToKeySuccessFactors(costReductionDaily)
        costReduction.save()

        // evaluate implementers kpi
        implementers.each { person ->
            person.getKeySuccessFactors()?.eachWithIndex { keySuccessFactor, i ->
                def weight = 25
                def actual = 70 + i * 10
                def target = 100
                def score = actual / target * 100
                def result = score * weight / 100
                def name = (keySuccessFactor.useKri) ? keySuccessFactor.kri : keySuccessFactor.title
                new Kpi(name: name, position: person.personPosition?.name, period: date, person: person, percentage: keySuccessFactor.percentage, weight: weight, target: target, actual: actual, score: score, result: result, keySuccessFactor: keySuccessFactor).save(failOnError: true)
            }
            // strategy kpi
            person.personPosition?.keySuccessFactors?.eachWithIndex { keySuccessFactor, i ->
                def weight = 25
                def actual = 70 + i * 10
                def target = 100
                def score = actual / target * 100
                def result = score * weight / 100
                boolean locked = false
                new Kpi(name: keySuccessFactor.title, position: person.personPosition?.name, period: date, person: person, weight: weight, target: target, actual: actual, score: score, result: result, locked: locked, keySuccessFactor: keySuccessFactor).save(flush: true)
            }
        }

        def cfos = Person.findAllByPersonPosition(cfo)
        cfos?.each { person ->
            person.getKeySuccessFactors()?.eachWithIndex { keySuccessFactor, i ->
                def weight = 25
                def actual = 70 + i * 10
                def target = 100
                def score = actual / target * 100
                def result = score * weight / 100
                def name = (keySuccessFactor.useKri) ? keySuccessFactor.kri : keySuccessFactor.title
                new Kpi(name: name, position: person.personPosition?.name, period: date, person: person, percentage: keySuccessFactor.percentage, weight: weight, target: target, actual: actual, score: score, result: result, keySuccessFactor: keySuccessFactor).save(failOnError: true)
            }
            person.personPosition?.keySuccessFactors?.eachWithIndex { keySuccessFactor, i ->
                def res = Kpi.executeQuery("select avg(k.score) from Kpi k where k.keySuccessFactor.parent = :parent and period = :period", [parent: keySuccessFactor, period: date])
                def weight = 25
                def actual = (res.size() > 0) ? res.get(0) as Double : 0.0
                def target = (actual > 0) ? 100.0 : 0.0
                def score = actual / target * 100
                def result = score * weight / 100
                boolean locked = (actual > 0)
                new Kpi(name: keySuccessFactor.title, position: person.personPosition?.name, period: date, person: person, weight: weight, target: target, actual: actual, score: score, result: result, locked: locked, keySuccessFactor: keySuccessFactor).save(failOnError: true)
            }
        }

        // holiday
        def today = new Date()
        int year = today[Calendar.YEAR]
        new Holiday(month: 1, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 2, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 3, year: year, number: 0).save(failOnError: true)
        new Holiday(month: 4, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 5, year: year, number: 2).save(failOnError: true)
        new Holiday(month: 6, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 7, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 8, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 9, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 10, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 11, year: year, number: 0).save(failOnError: true)
        new Holiday(month: 12, year: year, number: 1).save(failOnError: true)

        // competency characteristic

        // Knowledge
        new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Knowledge, name: "Define").save()
        new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Knowledge, name: "Describe").save()
        new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Knowledge, name: "Identify").save()
        new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Knowledge, name: "Register").save()
        new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Knowledge, name: "Match").save()
        new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Knowledge, name: "Mention").save()
        new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Knowledge, name: "Declare").save()
        new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Knowledge, name: "Reproduce").save()
        new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Knowledge, name: "Additional").save()

        /*
    // Comprehension
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Defense").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Distinguish").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Guess").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Explain").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Expand").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Conclude").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Generalize").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Give example").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Rewrite").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Estimate").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Comperehension, name: "Additional").save()

    // Application
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Change").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Count").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Demonstrate").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Found").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Manipulate").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Modify").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Operate").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Predict").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Prepare").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Produce").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Connect").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Show").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Solve").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Use").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Application, name: "Additional").save()

    // Analysis
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Detail").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Create diagram").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Distinguish").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Identify").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Illustrate").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Conclude").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Show").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Connect").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Choose").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Separate").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Divide").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Analysis, name: "Additional").save()

    // Synthesis
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Categorize").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Combine").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Compose").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Create").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Desain").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Explain").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Modify").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Organize").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Arrange").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Plan").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Reorganize").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Reconstruct").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Reorganize").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Revise").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Rewrite").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Write").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Tell").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Synthesis, name: "Additional").save()

    // Evaluate
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Categorize").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Combine").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Compose").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Create").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Desain").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Explain").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Modify").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Organize").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Arrange").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Plan").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Reorganize").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Reconstruct").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Reorganize").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Revise").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Rewrite").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Write").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Tell").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Cognitive.Evaluation, name: "Additional").save()

    // todo add more cognitive

    // Perception
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Affective.Receiving, name: "Ask").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Affective.Receiving, name: "Choose").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Affective.Receiving, name: "Describe").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Affective.Receiving, name: "Follow").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Affective.Receiving, name: "Give").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Affective.Receiving, name: "Identify").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Affective.Receiving, name: "Mention").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Affective.Receiving, name: "Show").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Affective.Receiving, name: "Answer").save()
    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Affective.Receiving, name: "Additional").save()

    // todo add more affective

    new CompetencyCharacteristic(subType: CompetencyCharacteristic.SubTypes.Psychomotor.Mechanism, name: "Repair").save()

    // todo add more psychomotor
    */
    }

}
