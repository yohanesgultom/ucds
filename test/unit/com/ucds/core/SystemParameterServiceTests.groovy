package com.ucds.core

import com.ucds.core.util.GridUtilService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor

@TestFor(SystemParameterService)
@Mock([SystemParameter, GridUtilService])
class SystemParameterServiceTests {

    void testGetList() {
        def rowList = [new SystemParameter(code: 'value', inactive: 'value', value: 'value')]
        def serviceStub = mockFor(GridUtilService)
        def params = [page: 1]
        serviceStub.demand.getDomainList { Class c, Map map -> [rows: rowList, totalRecords: rowList.size(), page: 1, totalPage: 1] }
        service.gridUtilService = serviceStub.createMock()
        service.getList(params)
    }

    void testAdd() {
        def params = [code: 'value', inactive: 'value', value: 'value']
        service.add(params)
        assert SystemParameter.count() == 1
    }

    void testEdit() {
        mockDomain(SystemParameter, [[code: 'value', inactive: 'value', value: 'value']])
        def params = [code: 'value', inactive: 'value', value: 'value']
        service.edit(params)
        assert SystemParameter.count() == 1
        assert SystemParameter.findByKey('value').value == 'value'
    }

    void testDelete() {
        mockDomain(SystemParameter, [[code: 'value', inactive: 'value', value: 'value']])
        def params = [code: 'value', inactive: 'value', value: 'value']
        service.delete(params)
        assert SystemParameter.count() == 0
    }

}
