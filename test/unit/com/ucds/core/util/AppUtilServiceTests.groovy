package com.ucds.core.util

import com.ucds.core.SystemParameter
import com.ucds.core.business.Holiday
import grails.test.mixin.*
import org.joda.time.DateTimeConstants

@TestFor(AppUtilService)
@Mock([Holiday, SystemParameter])
class AppUtilServiceTests {

    def "test getTotalWorkingDays"() {
        given:
        def leaveParam = new SystemParameter(code: "MONTHLY_LEAVE", value: "1").save(failOnError: true)
        def jan = new Holiday(month: 1, year: 2015, number: 1).save(failOnError: true)
        def feb = new Holiday(month: 2, year: 2015, number: 2).save(failOnError: true)
        int leave = Integer.parseInt(leaveParam.value)

        expect:
        assert service.getTotalWorkingDays(jan.month, jan.year) == (22 - jan.number - leave)
        assert service.getTotalWorkingDays(feb.month, feb.year) == (20 - feb.number - leave)
    }

    def "test getTotalDayInMonth"() {
        expect:
        assert service.getTotalDayInMonth(DateTimeConstants.SUNDAY, 1, 2015) == 4
        assert service.getTotalDayInMonth(DateTimeConstants.SATURDAY, 1, 2015) == 5
    }

    def "test getTotalEffectiveMinutes"() {
        given:
        new SystemParameter(code: "MONTHLY_LEAVE", value: "1").save(failOnError: true)
        new SystemParameter(code: "DAILY_WORKING_HOURS", value: "8").save(failOnError: true)
        int year = 2015
        new Holiday(month: 1, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 2, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 3, year: year, number: 0).save(failOnError: true)
        new Holiday(month: 4, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 5, year: year, number: 2).save(failOnError: true)
        new Holiday(month: 6, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 7, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 8, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 9, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 10, year: year, number: 1).save(failOnError: true)
        new Holiday(month: 11, year: year, number: 0).save(failOnError: true)
        new Holiday(month: 12, year: year, number: 1).save(failOnError: true)

        expect:
        assert service.getTotalEffectiveMinutes(year) == 114240
    }
}
