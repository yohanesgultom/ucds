package com.ucds.core.business


import grails.test.mixin.*
import groovy.json.JsonOutput
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(WorkContractTypeController)
@Mock([WorkContractType])
class WorkContractTypeControllerTests {

    void "test getLOV"() {
        given:
        def w1 = new WorkContractType(name: "Normal", annualLeaveDays: 12, averageDailyWorkHours: 8).save(failOnError: true)
        def w2 = new WorkContractType(name: "Hard", annualLeaveDays: 6, averageDailyWorkHours: 12).save(failOnError: true)
        def ws = [[id:w1.id,name:w1.name],[id:w2.id,name:w2.name]]
        when:
        controller.getLOV()
        then:
        assert response.text == JsonOutput.toJson(ws)
    }

}
