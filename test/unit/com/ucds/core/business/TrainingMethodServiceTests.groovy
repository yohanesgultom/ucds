package com.ucds.core.business



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TrainingMethodService)
class TrainingMethodServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
