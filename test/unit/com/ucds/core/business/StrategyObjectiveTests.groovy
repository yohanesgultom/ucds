package com.ucds.core.business


import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(StrategyObjective)
class StrategyObjectiveTests {

    void testSomething() {
        fail "Implement me"
    }
}
