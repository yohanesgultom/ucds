<%=packageName ? "package ${packageName}" : ''%>
<% def count = 0; def properties = domainClass.getPersistentProperties(); %>
<% def instanceName = propertyName.replace('Instance',''); def idProp_ = org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder.evaluateMapping(domainClass) ? org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder.evaluateMapping(domainClass).getIdentity().getName() : domainClass.getIdentifier().getName(); def idProp = idProp_.getAt(0).toUpperCase() + idProp_.substring(1);%>
class ${className}Service {

    def gridUtilService

    def getList(def params){
        def ret = null

        try {
            def res = gridUtilService.getDomainList(${className}, params)
            def rows = []
            def counter = 0
            ((List<${className}>)(res?.rows))?.each{
                rows << [
                        id: it.${idProp_},
                        cell:[
                            <% for (property in properties) { if (!property.isAssociation() && !property.isBasicCollectionType()) { %>
                            it.${property.name}<% if (count < properties.length - 1) { %>,<% } %> <% } %> <% count++; } %>
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception{
        def ${instanceName} = new ${className}()
        <% for (property in properties) { if (!property.isAssociation() && !property.isBasicCollectionType()) { %>
        ${instanceName}.${property.name} = params.${property.name} <% } } %>

        ${instanceName}.save()
        if (${instanceName}.hasErrors()) {
            throw new Exception("\${${instanceName}.errors}")
        }
    }

    def edit(params){
        def ${instanceName} = ${className}.findBy${idProp}(params.${idProp_})
        if (${instanceName}) {
            <% for (property in properties) { if (!property.isAssociation() && !property.isBasicCollectionType()) { %>
            ${instanceName}.${property.name} = params.${property.name} <% } } %>

            ${instanceName}.save()
            if (${instanceName}.hasErrors()) {
                throw new Exception("\${${instanceName}.errors}")
            }
        }else{
            throw new Exception("${className} not found")
        }
    }

    def delete(params){
        def ${instanceName} = ${className}.findBy${idProp}(params.${idProp_})
        if (${instanceName}) {
            ${instanceName}.delete()
        }else{
            throw new Exception("${className} not found")
        }
    }

}