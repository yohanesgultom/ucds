${"<"}%@ page contentType="text/html;charset=UTF-8" %${">"}
<% def instanceName = propertyName.replace('Instance',''); def idProp_ = org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder.evaluateMapping(domainClass) ? org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder.evaluateMapping(domainClass).getIdentity().getName() : domainClass.getIdentifier().getName(); %>
<html>
<head>
    <title>${className}</title>
    <meta name="layout" content="embedded"/>
</head>
<body>

<table id="${className}-Grid"></table>
<div id="${className}-Pager"></div>

<script type="text/javascript">

    function init${className}Grid(){
        $('#${className}-Grid').jqGrid({
            url: '\${request.getContextPath()}/${instanceName}/getList',
            editurl: '\${request.getContextPath()}/${instanceName}/edit',
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            colNames:[
                <% def count = 0; def properties = domainClass.getPersistentProperties(); for (property in properties) { if (!property.isAssociation() && !property.isBasicCollectionType()) { %>
                '<g:message code="app.${instanceName}.${property.getName()}.label" default="${property.getName().charAt(0).toUpperCase()}${property.getName().substring(1)}"/>'<% if (count < properties.length - 1) { %>,<% } %> <% } %> <% count++; } %>

            ],
            colModel :[
                <% count = 0; for (property in properties) { if (!property.isAssociation() && !property.isBasicCollectionType()) { %>
                {name:'${property.getName()}', index:'${property.getName()}',editable:true,edittype:'text',searchoptions:{sopt:['eq','ne','cn','nc']}} <% if (count < properties.length - 1) { %> , <% } %> <% } %> <% count++; } %>

            ],
            height: 'auto',
            pager: '#${className}-Pager',
            rowNum:10,
            rowList:[10,20,30],
            viewrecords: true,
            sortable: true,
            gridview: true,
            forceFit: true,
            loadui: 'block',
            rownumbers: true,
            caption: '${className}',
            onSelectRow: function(rowid){
                var rd = $("#${className}-Grid").jqGrid('getRowData', rowid);
                //TODO
            },
            gridComplete: function(){

            },
            sortname: '${domainClass.getIdentifier()?.name}',
            sortorder: 'asc'
        });

        $('#${className}-Grid').jqGrid(
                'navGrid','#${className}-Pager',
                {edit:true,add:true,del:true,view:true}, // options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterEdit: true
                }, // edit options
                {
                    afterSubmit: afterSubmitForm,
                    closeAfterAdd: true
                }, // add options
                {
                    afterSubmit: afterSubmitForm,
                    onclickSubmit: function(params, posdata){
                        var id = $("#${className}-Grid").jqGrid('getGridParam','selrow');
                        var rd = $("#${className}-Grid").jqGrid('getRowData', id);
                        return {${idProp_} : rd.${idProp_}};
    }
    }, // del options
    {}, // search options
    {} // view options
    );
    }

    function afterSubmitForm(response, postdata){
        var res = $.parseJSON(response.responseText);
        alert(res.message);
        return [true,""];
    }

    $(function(){
        init${className}Grid();
    });

</script>
</body>
</html>